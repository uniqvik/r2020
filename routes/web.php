<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Auth::routes(['register' => false, 'reset' => false]);
//demo
Route::get('demo','DemoController@index');
Route::get('demodashboard','DemoController@demodashboard')->name('demodashboard');
Route::get('companyprofile','DemoController@companyprofile')->name('companyprofile');
Route::get('myprofile','DemoController@myprofile')->name('myprofile');
Route::get('users','DemoController@users')->name('users');
Route::get('demoproperty','DemoController@property')->name('demoproperty');
//
Route::group(['middleware' => 'auth'], function () {

    Route::namespace('Admin')->group(function () {
        Route::resource('admin/roles', 'AdminRolesController');
        Route::resource('admin/users', 'AdminUsersController');




    Route::resource('admin/associations', 'AdminAssociationsController');
        Route::get('riapermissions/{id}', ['as'=> 'roles.riapermissions','uses' => 'RiaPermissionController@index']);
        Route::put('riapermissions/{id}', ['as'=> 'roles.riapermissions.update','uses' => 'RiaPermissionController@update']);

        Route::resource('admin/location/country', 'AdminCountriesController');
        Route::resource('admin/location/zone', 'AdminZoneController');
        Route::resource('admin/location/city', 'AdminCityController');
        Route::resource('admin/location/community', 'AdminCommunityController');
        Route::resource('admin/location/subcommunity', 'AdminSubCommunityController');
        Route::resource('admin/email/campaign', 'AdminCampaignsController');
        Route::resource('admin/email/template', 'AdminEmailTemplateController');
        Route::resource('products', 'AdminProductsController');
        Route::resource('inclusions', 'AdminInclusionsController');
        Route::resource('cmscategories', 'AdminCmsCategories');
        Route::resource('cms', 'AdminCms');
        Route::resource('seo', 'AdminSeoManagement');
        Route::resource('admin/propertyfeild', 'AdminPropertyFeild');
        Route::resource('admin/propertyfeildlist', 'AdminPropertyFeildList');
        Route::resource('admin/setting', 'AdminSettings');
        Route::resource('property', 'AdminProperty');
        Route::resource('project', 'AdminProject');
        //Package Management
        Route::get('agentpackage', 'AdminPackagesController@index')->name('agentpackage');
        Route::get('companypackage', 'AdminPackagesController@companyPackage')->name('companypackage');
        Route::get('developerpackage', 'AdminPackagesController@developerPackage')->name('developerpackage');
        Route::get('corporateaffiliatepackage', 'AdminPackagesController@corporateAffiliatePackage')->name('corporateaffiliatepackage');
        Route::post('updatepackage', 'AdminPackagesController@updatepackage')->name('updatepackage');
        //get location for dropdown
        Route::get('admin/location/getCountries', 'AdminCountriesController@getCountries')->name('getCountries');
        Route::post('admin/location/getZonesbyId', 'AdminZoneController@getZonesbyId')->name('getZonesbyId');
        Route::post('admin/location/getCitiesbyId', 'AdminCityController@getCitiesbyId')->name('getCitiesbyId');
        Route::post('admin/location/getCommunitybyId', 'AdminCommunityController@getCommunitybyId')->name('getCommunitybyId');
        Route::post('cmscategories/storeCategoryFromAjax', 'AdminCmsCategories@storeCategoryFromAjax')->name('storeCategoryFromAjax');
        //statusChange routes
        Route::get('roles/statusChange/{id}', ['as'=> 'roles.statusChange','uses' => 'AdminRolesController@statusChange']);
        Route::get('associations/statusChange/{id}', ['as'=> 'associations.statusChange','uses' => 'AdminAssociationsController@statusChange']);
        Route::get('location/country/statusChange/{id}', ['as'=> 'country.statusChange','uses' => 'AdminCountriesController@statusChange']);
        Route::get('location/zone/statusChange/{id}', ['as'=> 'zone.statusChange','uses' => 'AdminZoneController@statusChange']);
        Route::get('location/city/statusChange/{id}', ['as'=> 'city.statusChange','uses' => 'AdminCityController@statusChange']);
        Route::get('location/community/statusChange/{id}', ['as'=> 'community.statusChange','uses' => 'AdminCommunityController@statusChange']);
        Route::get('location/subcommunity/statusChange/{id}', ['as'=> 'subcommunity.statusChange','uses' => 'AdminSubCommunityController@statusChange']);
        Route::get('products/statusChange/{id}', ['as'=> 'products.statusChange','uses' => 'AdminProductsController@statusChange']);
        Route::get('inclusions/statusChange/{id}', ['as'=> 'inclusions.statusChange','uses' => 'AdminInclusionsController@statusChange']);
        Route::get('users/statusChange/{id}', ['as'=> 'users.statusChange','uses' => 'AdminUsersController@statusChange']);
        Route::get('email/campaign/statusChange/{id}', ['as'=> 'campaign.statusChange','uses' => 'AdminCampaignsController@statusChange']);
        Route::get('email/campaign/template/{id}', ['as'=> 'template.statusChange','uses' => 'AdminEmailtemplateController@statusChange']);
        Route::get('seo/seo/{id}', ['as'=> 'seo.statusChange','uses' => 'AdminSeoManagement@statusChange']);
        Route::get('propertyfeild/propertyfeild/{id}', ['as'=> 'propertyfeild.statusChange','uses' => 'AdminPropertyFeild@statusChange']);
        Route::get('propertyfeildlist/propertyfeildlist/{id}', ['as'=> 'propertyfeildlist.statusChange','uses' => 'AdminPropertyFeildList@statusChange']);

        Route::get('cms/cmscategory/{id}', ['as'=> 'cmscategory.statusChange','uses' => 'AdminCmsCategories@statusChange']);
        Route::get('cms/cms/{id}', ['as'=> 'cms.statusChange','uses' => 'AdminCms@statusChange']);
        Route::get('setting/setting/{id}', ['as'=> 'setting.statusChange','uses' => 'AdminSettings@statusChange']);
        Route::get('property/property/{id}', ['as'=> 'property.statusChange','uses' => 'AdminProperty@statusChange']);
        //Route::get('project/project/{id}', ['as'=> 'project.statusChange','uses' => 'AdminProject@statusChange']);

        Route::get('products/timePricesDelete/{id}', ['as'=> 'products.timePricesDelete','uses' => 'AdminProductsController@timePricesDelete']);

        Route::post('users/checkParentRole', ['as'=> 'users.checkParentRole','uses' => 'AdminUsersController@checkParentRole']);
    });
    Route::group(['namespace' => 'Admin','as'=>'admin'], function () {
        Route::get('/dashboard', 'Dashboard@index')->name('dashboard');
    });
});
