<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InclusionProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('inclusion_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inclusion_id')->unsigned()->nullable() ;
            $table->integer('product_id')->unsigned()->nullable();
            $table->string('value')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('order')->default(0);
            /*$table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();*/
            $table->engine = 'InnoDB';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('inclusion_product');
    }

}
