<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('subscription_id')->unsigned();
            $table->string('packageType');
            $table->string('timePeriod');
            $table->integer('noComapnyAgents')->nullable();
            $table->string('promocode')->nullable();
            $table->string('orderNumber')->nullable();
            $table->string('productAmount')->nullable();
            $table->string('vatAmount')->nullable();
            $table->string('paymentStatus');
            $table->string('paymentMethod')->nullable();
            $table->date('paymentDate');
            $table->date('startDate');
            $table->date('endDate');
            $table->text('remarks')->nullable();
            $table->string('history_order')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_histories');
    }
}
