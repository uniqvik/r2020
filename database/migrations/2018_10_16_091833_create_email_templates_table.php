<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('replyTo')->nullable()->default('info@realopedia.com');
            $table->string('mail_from')->nullable()->default('notification@realopedia.com');
            $table->string('mailSubject');
            $table->text('description')->nullable();
            $table->string('type')->default('mail_body');
            $table->string('header')->nullable();
            $table->string('footer')->nullable();
            $table->integer('campaign')->unsigned();
            $table->integer('is_active')->default(1);
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_templates');
    }
}
