<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->unsigned();
            $table->integer('parent_id');
            $table->string('company_name');
            $table->string('contact_name');
            $table->string('email');
            $table->string('username');
            $table->string('password');
            $table->string('license_number')->nullable();
            $table->date('license_expiry')->nullable();
            $table->integer('country')->unsigned()->nullable();
            $table->integer('zone')->unsigned()->nullable();
            $table->integer('city')->unsigned()->nullable();
            $table->integer('community')->unsigned()->nullable();
            $table->integer('subcommunity')->unsigned()->nullable();
            $table->string('mobile')->nullable();
            $table->string('telephone')->nullable();
            $table->string('email_alt')->nullable();
            $table->string('user_image', 300)->nullable();
            $table->text('language')->nullable();
            $table->text('specialities')->nullable();
            $table->string('occupation', 300)->nullable();
            $table->string('gender')->nullable();
            $table->text('social_media_links')->nullable();
            $table->string('website', 300)->nullable();
            $table->integer('group_flag')->unsigned()->nullable();
            $table->string('xml_unique_id', 10)->nullable();
            $table->string('refered_by')->nullable();
            $table->integer('pap_project_id')->nullable();
            $table->timestamp('realopedia_created_at')->nullable();
            $table->integer('rep_id')->nullable();
            $table->boolean('is_active')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
