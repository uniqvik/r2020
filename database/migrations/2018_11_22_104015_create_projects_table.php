<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title', 300)->nullable();
            $table->boolean('is_roadshow')->default(0);
            $table->enum('project_type', ['Commercial', 'Residential'])->default('Residential');
            $table->integer('reservation')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('zone_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('community_id')->nullable();
            $table->integer('sub_community_id')->nullable();
            $table->string('rera_permit', 300)->nullable();
            $table->string('latitude',50)->nullable();
            $table->string('longitude',50)->nullable();
            $table->bigInteger('price_range_from')->nullable();
            $table->bigInteger('price_range_to')->nullable();
            $table->string('currency',5)->nullable();
            $table->integer('project_category')->unsigned();
            $table->bigInteger('price_per_sqft')->nullable();
            $table->bigInteger('price_per_sqmt')->nullable();
            $table->text('availability_status')->nullable();
            $table->date('expected_completion')->nullable();
            $table->string('website', 300)->nullable();
            $table->string('bed_range_from', 50)->nullable();
            $table->string('bed_range_to', 50)->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
