<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 300)->nullable();
            $table->integer('xml_id')->default('0');
            $table->integer('project_id')->default('0');
            $table->string('web_reference', 300)->nullable();
            $table->string('reference_mls', 300)->nullable();
            $table->enum('property_type', ['Commercial', 'Residential'])->default('Residential');
            $table->enum('frequency', ['Per Year', 'Per Month'])->default('Per Year');
            $table->enum('ad_type', ['Rent', 'Sale'])->default('Sale');
            $table->text('matrix_unique_id')->nullable();
            $table->string('rent_period', 300)->nullable();
            $table->integer('unit_type')->nullable();
            $table->float('plot_size_sqft', 15, 4)->nullable();
            $table->float('plot_size_sqmt', 15, 4)->nullable();
            $table->float('builtup_area_sqft', 15, 4)->nullable();
            $table->float('builtup_area_sqmt', 15, 4)->nullable();
            $table->string('bedrooms', 50)->nullable();
            $table->string('bathrooms', 50)->nullable();
            $table->integer('parking_no')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('zone_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('community_id')->nullable();
            $table->integer('sub_community_id')->nullable();
            $table->string('rera_permit', 300)->nullable();
            $table->date('permit_startdate')->nullable();
            $table->date('permit_enddate')->nullable();
            $table->integer('permitstatus')->nullable();
            $table->string('currency',10)->nullable();
            $table->bigInteger('local_price')->nullable();
            $table->bigInteger('usd_price')->nullable();
            $table->string('m_flag',50)->nullable();
            $table->enum('status', ['1', '3','Sold','Rent'])->default();
            $table->enum('is_featured', ['0', '1'])->default('0');
            $table->integer('assign_property')->default('0');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
