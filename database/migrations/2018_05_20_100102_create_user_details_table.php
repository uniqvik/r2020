<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('verification_code')->nullable();
            $table->text('address')->nullable();
            $table->text('address_alt')->nullable();
            $table->text('about_me')->nullable();
            $table->text('friends')->nullable();
            $table->text('rejected')->nullable();
            $table->text('invitation_sent')->nullable();
            $table->text('invitation_received')->nullable();
            $table->string('userdetail_banner', 300)->nullable();
            $table->string('license_upload', 300)->nullable();
            $table->text('watchlist')->nullable();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
