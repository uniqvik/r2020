<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesAddReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('restrict');
            $table->foreign('group_flag')->references('id')->on('associations')->onDelete('restrict');
            $table->foreign('country')->references('id')->on('countries')->onDelete('restrict');
            $table->foreign('zone')->references('id')->on('zones')->onDelete('restrict');
            $table->foreign('city')->references('id')->on('cities')->onDelete('restrict');
            $table->foreign('community')->references('id')->on('communities')->onDelete('restrict');
            $table->foreign('subcommunity')->references('id')->on('sub_communities')->onDelete('restrict');
        });
        Schema::table('user_details', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('permissions', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
        Schema::table('ria_permissions', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
        Schema::table('login_histories', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('subscription_histories', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
        Schema::table('zones', function (Blueprint $table) {
            $table->foreign('country')->references('id')->on('countries')->onDelete('restrict');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->foreign('country')->references('id')->on('countries')->onDelete('restrict');
            $table->foreign('zone')->references('id')->on('zones')->onDelete('restrict');
        });
        Schema::table('communities', function (Blueprint $table) {
            $table->foreign('country')->references('id')->on('countries')->onDelete('restrict');
            $table->foreign('zone_id')->references('id')->on('zones')->onDelete('restrict');
        });
        Schema::table('sub_communities', function (Blueprint $table) {
            $table->foreign('country')->references('id')->on('countries')->onDelete('restrict');
            $table->foreign('community_id')->references('id')->on('communities')->onDelete('restrict');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('restrict');
        });
        Schema::table('product_time_prices', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
        Schema::table('inclusion_product', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('inclusion_id')->references('id')->on('inclusions')->onDelete('cascade');
        });
        Schema::table('email_templates', function (Blueprint $table) {
            $table->foreign('campaign')->references('id')->on('campaigns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
            $table->dropForeign(['group_flag']);
            $table->dropForeign(['country']);
            $table->dropForeign(['zone']);
            $table->dropForeign(['city']);
            $table->dropForeign(['community']);
            $table->dropForeign(['subcommunity']);
        });
        Schema::table('user_details', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
        });
        Schema::table('ria_permissions', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
        });
        Schema::table('login_histories', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('subscription_histories', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['subscription_id']);
        });
        Schema::table('zones', function (Blueprint $table) {
            $table->dropForeign(['country']);
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropForeign(['country']);
            $table->dropForeign(['zone']);
        });
        Schema::table('communities', function (Blueprint $table) {
            $table->dropForeign(['country']);
            $table->dropForeign(['zone_id']);
        });
        Schema::table('sub_communities', function (Blueprint $table) {
            $table->dropForeign(['country']);
            $table->dropForeign(['community_id']);
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
        });
        Schema::table('product_time_prices', function (Blueprint $table) {
            $table->dropForeign(['product_id']);
        });
        Schema::table('inclusion_product', function (Blueprint $table) {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['inclusion_id']);
        });
        Schema::table('email_templates', function (Blueprint $table) {
            $table->dropForeign(['campaign']);
        });

    }
}
