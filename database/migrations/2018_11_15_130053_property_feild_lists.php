<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PropertyFeildLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_feild_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',200)->nullable();
            $table->integer('property_feild_id')->unsigned();
            $table->string('property_type',50)->nullable();
            $table->integer('is_active')->default(1);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_feild_lists');
    }
}
