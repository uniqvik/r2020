<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned();
            $table->text('description')->nullable();
            $table->text('short_des')->nullable();
            $table->text('address')->nullable();
            $table->string('zipcode',10)->nullable();
            $table->enum('furnished', ['Yes', 'No','Partial'])->default('No');
            $table->enum('tenentated', ['Yes', 'No'])->default('No');
            $table->date('tenant_exp_date')->nullable();
            $table->text('views')->nullable();
            $table->string('unit_numer',50)->nullable();
            $table->string('latitude',50)->nullable();
            $table->string('longitude',50)->nullable();
            $table->text('facilities')->nullable();
            $table->string('videos',300)->nullable();
            $table->string('view360',300)->nullable();
            $table->text('property_status')->nullable();
            $table->text('community')->nullable();
            $table->text('garage_type')->nullable();
            $table->text('flooring')->nullable();
            $table->text('exterior_features')->nullable();
            $table->text('interior_features')->nullable();
            $table->text('parking_desc')->nullable();
            $table->text('floor_plan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_descriptions');
    }
}
