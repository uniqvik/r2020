<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiaPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ria_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->text('access')->nullable();
            $table->text('list')->nullable();
            $table->text('view')->nullable();
            $table->text('add')->nullable();
            $table->text('edit')->nullable();
            $table->text('delete')->nullable();
            $table->text('search')->nullable();
            $table->text('status')->nullable();
            $table->text('export')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ria_permissions');
    }
}
