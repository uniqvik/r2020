<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->text('description')->nullable();
            $table->text('short_des')->nullable();
            $table->text('address')->nullable();
            $table->string('video_url', 300)->nullable();
            $table->string('virtual_video_url', 300)->nullable();
            $table->text('key_features')->nullable();
            $table->text('floor_plans')->nullable();
            $table->text('mr_flyers')->nullable();
            $table->text('mr_websitebanners')->nullable();
            $table->text('mr_brouchure')->nullable();
            $table->text('mr_researchreports')->nullable();
            $table->text('pay_milestones')->nullable();
            $table->text('pay_percentage')->nullable();
            $table->text('pay_amounts')->nullable();
            $table->text('payment_chart')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_descriptions');
    }
}
