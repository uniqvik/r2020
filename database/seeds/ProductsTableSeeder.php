<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE products AUTO_INCREMENT = 1');
        $todayDate = date('Y-m-d H:i:s');
        DB::table('products')->insert([
            ['productName' => 'Classic','type'=>'subscription','role_id'=>6,'slug'=>'classic_company','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Classic','type'=>'subscription','role_id'=>7,'slug'=>'classic_agent','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Classic','type'=>'subscription','role_id'=>8,'slug'=>'classic_developer','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Classic','type'=>'subscription','role_id'=>9,'slug'=>'classic_corporateaffiliate','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Premium','type'=>'subscription','role_id'=>7,'slug'=>'premium_agent','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Premium+','type'=>'subscription','role_id'=>7,'slug'=>'premium+_agent','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Global Corporate Packages','type'=>'subscription','role_id'=>6,'slug'=>'globalcorporatepackages_company','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Premium','type'=>'subscription','role_id'=>8,'slug'=>'premium_developer','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Premium','type'=>'subscription','role_id'=>9,'slug'=>'premium_corporateaffiliate','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Featured Property','type'=>'addons','role_id'=>6,'slug'=>'addons_featuredproperty_company','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['productName' => 'Featured Property','type'=>'addons','role_id'=>7,'slug'=>'addons_featuredproperty_agent','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            /*['productName' => 'Featured Property','type'=>'addons','role_id'=>8,'slug'=>'addons_featuredproperty_developer','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],*/
            ['productName' => 'Projects','type'=>'addons','role_id'=>8,'slug'=>'addons_projects','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1]
        ]);
    }
}
