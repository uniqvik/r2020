<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $todayDate = date('Y-m-d H:i:s');

        $getusers = DB::table('users')
                         ->select('id','email')
                         ->where('role_id', '=', 8)
                         ->get();
        
        foreach($getusers as $user){
            $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/project/getProject&email='.$user->email);    
            $results = json_decode($json);
            if($results){
                foreach ($results as $result) {
                    $country = null;$zone = null;$city = null;
                    if($result->country_id){
                        $country_id = DB::table('countries')
                                 ->select('id')
                                 ->where('name', '=', $result->country_id)
                                 ->get();
                        $country = $country_id[0]->id;
                    }

                    if($result->zone_id){
                        $zone_id = DB::table('zones')
                                 ->select('id')
                                 ->where('name', '=', $result->zone_id)
                                 ->get();
                                 if(count($zone_id) > 0){
                        $zone = $zone_id[0]->id;}
                    }
                    
                    if($result->city_id){
                        $city_id = DB::table('cities')
                                 ->select('id')
                                 ->where('name', '=', $result->city_id)
                                 ->get();
                                 if(count($city_id) > 0){
                        $city = $city_id[0]->id;}
                    }
                    $unit = DB::table('property_feild_lists')
                                 ->select('id')
                                 ->where('title', '=', $result->project_category)
                                 ->get();

                                 if(count($unit) > 0){
                        $unit_type = $unit[0]->id;}
                    
                    
                    DB::table('projects')->insert([
                    [
                        'user_id'=>$user->id,
                        'title'=>$result->title, 
                        'is_roadshow'=>$result->is_roadshow,
                        'project_type'=>$result->project_type, 
                        'reservation'=>$result->reservation, 
                        'country_id'=>$country,
                        'zone_id'=>$zone,
                        'city_id'=>$city,
                        'community_id'=>null, 
                        'sub_community_id'=>null,
                        'rera_permit'=>$result->rera_permit, 
                        'latitude'=>$result->latitude, 
                        'longitude'=>$result->longitude, 
                        'price_range_from'=>$result->price_range_from, 
                        'price_range_to'=>$result->price_range_to, 
                        'currency'=>$result->currency, 
                        'project_category'=>$unit_type, 
                        'price_per_sqft'=>$result->price_per_sqft, 
                        'price_per_sqmt'=>$result->price_per_sqmt, 
                        'availability_status'=>$result->availability_status, 
                        'expected_completion'=>$result->expected_completion, 
                        'website'=>$result->website, 
                        'bed_range_from'=>$result->bed_range_from, 
                        'bed_range_to'=>$result->bed_range_to, 
                        'is_active'=>$result->is_active, 
                        'created_at'=>$result->created_at, 
                        'updated_at'=>$result->updated_at, 
                        ],
                    ]);

                $id = DB::getPdo()->lastInsertId();
                $mr_brouchure = '';
                if($result->mr_brouchure){
                    $mr_brouchure = serialize($result->mr_brouchure);
                }
                DB::table('project_descriptions')->insert([
                    [
                        'project_id'=>$id, 
                        'description'=>$result->description, 
                        'short_des'=>'', 
                        'address'=>$result->address, 
                        'video_url'=>$result->video_url, 
                        'virtual_video_url'=>$result->virtual_video_url, 
                        'key_features'=>$result->key_features, 
                        'floor_plans'=>$result->floor_plans, 
                        'mr_flyers'=>$result->mr_flyers, 
                        'mr_websitebanners'=>$result->mr_websitebanners, 
                        'mr_brouchure'=>$mr_brouchure, 
                        'mr_researchreports'=>$result->mr_researchreports, 
                        'pay_milestones'=>$result->pay_milestones, 
                        'pay_percentage'=>$result->pay_percentage, 
                        'pay_amounts'=>$result->pay_amounts,
                        'payment_chart'=>$result->payment_chart
                        ],
                    ]);
                if($result->header_image){
                        DB::table('project_photos')->insert([
                        [
                            'project_id'=>$id, 
                            'type'=>'header_image', 
                            'link'=>$result->header_image,
                            ],
                        ]);
                }
                if($result->images){
                    foreach ($result->images as $image) {
                            DB::table('project_photos')->insert([
                            [
                                'project_id'=>$id, 
                                'type'=>'image', 
                                'link'=>$image->image,
                                ],
                            ]);
                    }
                }
                echo $result->title;  
                }
            }
        }
    }
}
