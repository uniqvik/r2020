<?php

use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $todayDate = date('Y-m-d H:i:s');

        $getusers = DB::table('users')
                         ->select('id','email')
                         ->get();
        
        foreach($getusers as $user){
            $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/property/Getproperty&email='.$user->email);    
            $results = json_decode($json);
            if($results){
                foreach ($results as $result) {
                    $country = null;$zone = null;$city = null;
                    if($result->country_id){
                        $country_id = DB::table('countries')
                                 ->select('id')
                                 ->where('name', '=', $result->country_id)
                                 ->get();
                        $country = $country_id[0]->id;
                    }

                    if($result->zone_id){
                        $zone_id = DB::table('zones')
                                 ->select('id')
                                 ->where('name', '=', $result->zone_id)
                                 ->get();
                                 if(count($zone_id) > 0){
                        $zone = $zone_id[0]->id;}
                    }
                    
                    if($result->city_id){
                        $city_id = DB::table('cities')
                                 ->select('id')
                                 ->where('name', '=', $result->city_id)
                                 ->get();
                                 if(count($city_id) > 0){
                        $city = $city_id[0]->id;}
                    }
                    $unit = DB::table('property_feild_lists')
                                 ->select('id')
                                 ->where('title', '=', $result->unit_type)
                                 ->get();

                                 if(count($unit) > 0){
                        $unit_type = $unit[0]->id;}

                    DB::table('properties')->insert([
                    [
                        'title'=>$result->title, 
                        'xml_id'=>0,'project_id'=>0,
                        'web_reference'=>$result->web_reference, 
                        'reference_mls'=>$result->reference_mls, 
                        'property_type'=>$result->property_type, 
                        'frequency'=>$result->frequency, 
                        'ad_type'=>$result->ad_type, 
                        'matrix_unique_id'=>$result->matrix_unique_id, 
                        'rent_period'=>$result->rent_period, 
                        'unit_type'=>$unit_type, 
                        'plot_size_sqft'=>$result->plot_size_sqft, 
                        'plot_size_sqmt'=>$result->plot_size_sqmt, 
                        'builtup_area_sqft'=>$result->builtup_area_sqft, 
                        'builtup_area_sqmt'=>$result->builtup_area_sqmt, 
                        'bedrooms'=>$result->bedrooms, 
                        'bathrooms'=>$result->bathrooms, 
                        'parking_no'=>$result->parking_no, 
                        'country_id'=>$country,
                        'zone_id'=>$zone,
                        'city_id'=>$city,
                        'community_id'=>null, 
                        'sub_community_id'=>null, 
                        'rera_permit'=>$result->rera_permit, 
                        'permit_startdate'=>$result->permit_startdate, 
                        'permit_enddate'=>$result->permit_enddate, 
                        'permitstatus'=>$result->permitstatus, 
                        'currency'=>$result->currency, 
                        'local_price'=>$result->local_price, 
                        'usd_price'=>$result->usd_price, 
                        'm_flag'=>$result->m_flag, 
                        'status'=>$result->status, 
                        'is_featured'=>$result->is_featured, 
                        'assign_property'=>0, 
                        'created_by'=>$user->id, 
                        'updated_by'=>$user->id, 
                        'created_at'=>$result->created_at, 
                        'updated_at'=>$result->updated_at, 
                        ],
                    ]);

                $id = DB::getPdo()->lastInsertId();
                DB::table('property_descriptions')->insert([
                    [
                        'property_id'=>$id, 
                        'description'=>$result->description, 
                        'short_des'=>'', 
                        'address'=>$result->address, 
                        'zipcode'=>$result->zipcode, 
                        'furnished'=>$result->furnished, 
                        'tenentated'=>$result->tenentated, 
                        'tenant_exp_date'=>$result->tenant_exp_date, 
                        'views'=>$result->views , 
                        'unit_numer'=>$result->unit_numer, 
                        'latitude'=>$result->latitude, 
                        'longitude'=>$result->longitude, 
                        'facilities'=>$result->facilities, 
                        'videos'=>$result->videos, 
                        'view360'=>$result->view360, 
                        'property_status'=>$result->property_status,
                        'community'=>$result->community, 
                        'garage_type'=>$result->garage_type, 
                        'flooring'=>$result->flooring, 
                        'exterior_features'=>$result->exterior_features, 
                        'interior_features'=>$result->interior_features, 
                        'parking_desc'=>$result->parking_desc, 
                        'floor_plan'=>$result->floor_plan, 
                        'created_at'=>$result->created_at, 
                        'updated_at'=>$result->updated_at, 
                        ],
                    ]);
                if($result->images){
                    $images = explode(',', $result->images);
                    foreach ($images as $image) {
                        DB::table('property_photos')->insert([
                        [
                            'property_id'=>$id, 
                            'type'=>'image', 
                            'image_link'=>$image,
                            'is_active' =>1,
                            'created_at'=>$result->created_at, 
                            'updated_at'=>$result->updated_at, 
                            ],
                        ]);
                    }
                }
                echo $result->title;  
                }
            }
        }
    }
}
