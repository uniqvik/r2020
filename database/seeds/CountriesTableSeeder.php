<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE countries AUTO_INCREMENT = 1');
        
        $todayDate = date('Y-m-d H:i:s');
        $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/countryapi/Getcountry');
        $results = json_decode($json);
        
        foreach($results as $result){
        DB::table('countries')->insert([
            ['name' => $result->name,
             'iso_code_2' => $result->iso_code_2,'iso_code_3' => $result->iso_code_3,
             'currency_code'=> $result->currency_code,'latitude'=>$result->latitude,
             'longitude'=>$result->longitude,'is_active'=>$result->status,
             'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ]);
        }
    }
}
