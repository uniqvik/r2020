<?php

use Illuminate\Database\Seeder;

class InclusionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE inclusions AUTO_INCREMENT = 1');
        $todayDate = date('Y-m-d H:i:s');
        $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/inclusions/GetInclusions');
        $results = json_decode($json);
        foreach($results as $result){
            DB::table('inclusions')->insert([
            ['name' =>  $result->vName,
             /*'slug'=> $result->vSlug,*/
             'is_active'=> $result->iStatus,
             'created_at' => $todayDate,
             'updated_at' => $todayDate,
             'created_by'=>1,'updated_by'=>1],
            ]);
        }
//        DB::table('inclusions')->insert([
//         ['name' => 'Build your personal profile, add a picture, experience, website & contact details','slug'=>'build_profile_agent','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Add pictures & videos to enhance your listings','slug'=>'pic_vid_listing','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Featured property listings','slug'=>'featured_listing','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Automatic upload of unlimited property listings (via XML/RETS)','slug'=>'xml_listing','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Access to a fully integrated lead management system from your Realopedia dashboard','slug'=>'leads','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Access Market Intelligence through the Knowledge Hub & Media Centre','slug'=>'mediacentre','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Join the Networking Hub to Connect & Build a Global Network','slug'=>'networkhub','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Messaging Chat System to potential clients','slug'=>'messageplatform','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Access & sell developers projects – be part of the Developers Preferred Agent Program (DPAP)','slug'=>'dpap_agent_company','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Access the Realopedia Facebook Group','slug'=>'facebookgroup','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Build your company profile, add logo, expertise, website & contact details','slug'=>'build_profile_company','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Add Agents under Broker Company (contact us)','slug'=>'agents_under_company','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Featured Property Listings Per Agent','slug'=>'featured_listing_peragent_under_company','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Upload an e-brochure, 2D & 3D floor plans, project video & picture gallery & 3D Virtual Tour to enhance your projects','slug'=>'project_details','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Add projects & inventory','slug'=>'add_project_inventory','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => 'Access agents globally through the Developers Preferred Agent Program (DPAP) to represent and sell your projects','slug'=>'dpap_devloper','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//         ['name' => '1 Full Page on the Realopedia eMagazine','slug'=>'eMagazine','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1]
//        ]);
    }
}
