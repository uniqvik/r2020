<?php

use Illuminate\Database\Seeder;

class RiaPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE ria_permissions AUTO_INCREMENT = 1');

        $todayDate = date('Y-m-d H:i:s');
        DB::table('ria_permissions')->insert([
            ['role_id' => '1','access'=> 'role,association,user,location,product,inclusion,package,email',
             'list'=> 'role,association,user,location,product,inclusion,package,email' ,
             'view'=> 'role,association,user,location,product,inclusion,package,email' ,
             'search'=> 'role,association,user,location,product,inclusion,package,email' ,
             'add'=> 'role,association,user,location,product,inclusion,package,email' ,
             'edit'=> 'role,association,user,location,product,inclusion,package,email' ,
             'delete'=> 'role,association,user,location,product,inclusion,package,email',
             'status'=> 'role,association,user,location,product,inclusion,package,email',
             'export'=> 'role,association,user,location,product,inclusion,package,email',
             'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['role_id' => '2','access'=> 'dashboard,user,location',
             'list'=> 'user,location' ,
             'search'=> 'user,location' ,
             'view'=> 'user,location' ,
             'add'=> 'user,location' ,
             'edit'=> 'user,location' ,
             'delete'=> 'user',
             'status'=> 'user,location',
             'export'=> 'user,location',
             'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['role_id' => '3','access'=> 'dashboard,user',
             'list'=> 'user' ,
             'search'=> 'user' ,
             'view'=> 'user' ,
             'add'=> '' ,
             'edit'=> '' ,
             'delete'=> '',
             'status'=> 'user',
             'export'=> 'user',
             'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['role_id' => '4','access'=> 'dashboard,user',
            'list'=> '' ,
            'search'=> '' ,
            'view'=> '' ,
            'add'=> '' ,
            'edit'=> '' ,
            'delete'=> '',
            'status'=> '',
            'export'=> '',
            'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['role_id' => '5','access'=> 'dashboard,user',
            'list'=> 'user' ,
            'search'=> 'user' ,
            'view'=> 'user' ,
            'add'=> '' ,
            'edit'=> '' ,
            'delete'=> '',
            'status'=> '',
            'export'=> 'user',
            'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],

        ]);

    }
}
