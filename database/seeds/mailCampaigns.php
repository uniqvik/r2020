<?php

use Illuminate\Database\Seeder;

class mailCampaigns extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('ALTER TABLE campaigns AUTO_INCREMENT = 1');

        $todayDate = date('Y-m-d H:i:s');
        $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/countryapi/Getcampaigns');
        $results = json_decode($json);

        foreach($results as $result){
        DB::table('campaigns')->insert([
            ['title' => $result->campaign_title,
             'description' => $result->campaign_desc,'is_active' => $result->status,
             'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ]);
        }
    }
}
