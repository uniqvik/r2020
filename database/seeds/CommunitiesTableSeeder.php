<?php

use Illuminate\Database\Seeder;

class CommunitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE communities AUTO_INCREMENT = 1');
        
        $todayDate = date('Y-m-d H:i:s');
        $zones = DB::table('zones')->where('code', 'ARE')->limit(100)->get();

        foreach ($zones as $zone) {
            
        
        $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/countryapi/Getcommunity&name='.urlencode($zone->name));

        $results = json_decode($json);
            if($results){
                foreach($results as $result){

                DB::table('communities')->insert([
                    ['name' => $result->community_name,
                     'country'=>$zone->country,
                     'zone_id' => $zone->id,
                     'is_active'=>$result->status,
                     'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
                    ]);
                }
                echo $zone->name.' -Done';
            }

        }
    }
}
