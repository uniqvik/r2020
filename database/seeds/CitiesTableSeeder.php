<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE cities AUTO_INCREMENT = 1');

        $todayDate = date('Y-m-d H:i:s');
        $zones = DB::table('zones')->limit(10)->get();

        foreach ($zones as $zone) {


            $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/countryapi/Getcity&code='.$zone->code .'&name='.urlencode($zone->name));
            
            $results = json_decode($json);
            if ($results) {
                foreach ($results as $result) {


                    DB::table('cities')->insert([
                        ['name' => $result->name,
                            'code' => $result->code, 'zone' => $zone->id,'country' => $zone->country,
                            'latitude' => $result->latitude,
                            'longitude' => $result->longitude, 'is_active' => $result->status,
                            'created_at' => $todayDate, 'updated_at' => $todayDate, 'created_by' => 1, 'updated_by' => 1],
                    ]);

                }
            }
        }
    }
}
