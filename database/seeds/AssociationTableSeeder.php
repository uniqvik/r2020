<?php

use Illuminate\Database\Seeder;

class AssociationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE associations AUTO_INCREMENT = 1');

        $todayDate = date('Y-m-d H:i:s');
        DB::table('associations')->insert([
            ['title' => 'Realopedia Test Accounts','code' => 'RTA','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'Realopedia Internal Accounts','code' => 'RIA','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'RETS','code' => 'RETS','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'CREA','code' => 'CREA','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'Panama','code' => 'PA','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'Masterkey','code' => 'M','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'India-NAR','code' => 'NAR-IND','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'Self Registered','code' => 'SR','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'DREI','code' => 'DREI','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ['title' => 'XML','code' => 'XML','created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
        ]);
    }
}
