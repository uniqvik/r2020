<?php

use Illuminate\Database\Seeder;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE zones AUTO_INCREMENT = 1');
        
        $todayDate = date('Y-m-d H:i:s');
        $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/countryapi/Getzone');
        $results = json_decode($json);
        
        foreach($results as $result){
            $country_id = DB::table('countries')
                     ->select('id')
                     ->where('iso_code_3', '=', $result->code)
                     ->get();

        DB::table('zones')->insert([
            ['name' => $result->name,
             'code' => $result->code,'country' => $country_id[0]->id,
             'latitude'=>$result->latitude,
             'longitude'=>$result->longitude,'is_active'=>$result->status,
             'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ]);
        }
    }
}
