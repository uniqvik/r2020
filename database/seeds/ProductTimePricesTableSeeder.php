<?php

use Illuminate\Database\Seeder;

class ProductTimePricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE product_time_prices AUTO_INCREMENT = 1');
        $todayDate = date('Y-m-d H:i:s');
        DB::table('product_time_prices')->insert([
            /* agent premium subscriptions*/
            ['product_id' => '5','timePeriod'=>'1','price'=>'9.99','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '5','timePeriod'=>'3','price'=>'29.99','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '5','timePeriod'=>'6','price'=>'59.99','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '5','timePeriod'=>'12','price'=>'99','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            /*agent premium+ subscription*/
            ['product_id' => '6','timePeriod'=>'12','price'=>'199','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            /*developer premium subscriptions*/
            ['product_id' => '8','timePeriod'=>'6','price'=>'3000','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '8','timePeriod'=>'12','price'=>'5000','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            /*CA premium subscriptions*/
            ['product_id' => '9','timePeriod'=>'12','price'=>'199','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            /*company addons-featured property*/
            ['product_id' => '10','timePeriod'=>'1','price'=>'20','currency'=>'USD','frequency'=>'1','created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '10','timePeriod'=>'1','price'=>'75','currency'=>'USD','frequency'=>'5','created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '10','timePeriod'=>'1','price'=>'150','currency'=>'USD','frequency'=>'15','created_at' => $todayDate, 'updated_at' => $todayDate],
            /*agent addons-featured property*/
            ['product_id' => '11','timePeriod'=>'1','price'=>'20','currency'=>'USD','frequency'=>'1','created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '11','timePeriod'=>'1','price'=>'75','currency'=>'USD','frequency'=>'5','created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '11','timePeriod'=>'1','price'=>'150','currency'=>'USD','frequency'=>'15','created_at' => $todayDate, 'updated_at' => $todayDate],
            /*developer addons-featured property*/
            /*['product_id' => 12,'timePeriod'=>'1','price'=>'20','currency'=>'USD','frequency'=>'1','created_at' => $todayDate, 'updated_at' => $todayDate],*/
            /*developer addons-project*/
            ['product_id' => '12','timePeriod'=>'6','price'=>'2000','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '12','timePeriod'=>'12','price'=>'4000','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            /*classic subscriptions*/
            ['product_id' => '1','timePeriod'=>'400','price'=>'0','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '2','timePeriod'=>'400','price'=>'0','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '3','timePeriod'=>'400','price'=>'0','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            ['product_id' => '4','timePeriod'=>'400','price'=>'0','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate],
            /*company global corporate subscription*/
            ['product_id' => '7','timePeriod'=>'12','price'=>'0','currency'=>'USD','frequency'=>NULL,'created_at' => $todayDate, 'updated_at' => $todayDate]
        ]);
    }
}
