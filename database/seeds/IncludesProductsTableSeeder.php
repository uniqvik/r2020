<?php

use Illuminate\Database\Seeder;

class IncludesProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE inclusion_product AUTO_INCREMENT = 1');
        $todayDate = date('Y-m-d H:i:s');
        $inclusions = DB::table('inclusions')->get();

        foreach($inclusions as $inclusion){
            $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/inclusions/GetInclusionsVal&name='.base64_encode($inclusion->name));
             // echo $inclusion->name.' '.base64_encode($inclusion->name).'<br/>';
            $results = json_decode($json);
            foreach($results as $res){
                if($res->eUserType =='service_provider'){$type = 'corporate affiliates';}else{$type = $res->eUserType;}
                if($res->vPackageType == 'Premium +'){$res->vPackageType = 'Premium+';}
                $product = DB::table('products')->select('products.id')
                    ->join('roles', 'roles.id', '=', 'products.role_id')
                    ->where('products.productName',$res->vPackageType)
                    ->where('roles.title',$type)->get();
                $pid  = $product->toArray();

                if(!$product->isEmpty()){
                 DB::table('inclusion_product')->insert([
                ['inclusion_id' => $inclusion->id,'product_id'=>$pid[0]->id,
                'value'=>$res->vSubscriptionInclusionValue,'order'=>1],
                 ]);
                }
            }
        }

//
//        DB::table('include_product')->insert([
//            /*agent includes*/
//            /*classic*/
//            ['include_id' => 1,'product_id'=>2,'value'=>'yes','order'=>1,'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//            /*premium*/
//            ['include_id' => 1,'product_id'=>5,'value'=>'yes','order'=>1,'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//            /*premium+*/
//            ['include_id' => 1,'product_id'=>6,'value'=>'yes','order'=>1,'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//
//            /*company includes*/
//            /*classic*/
//            ['include_id' => 11,'product_id'=>1,'value'=>'yes','order'=>1,'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//            /*global package*/
//            /*developer includes*/
//            /*classic*/
//            ['include_id' => 11,'product_id'=>3,'value'=>'yes','order'=>1,'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//            /*premium*/
//            /*corporate affiliate includes*/
//            /*classic*/
//            ['include_id' => 11,'product_id'=>4,'value'=>'yes','order'=>1,'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
//            /*premium*/
//        ]);
    }
}
