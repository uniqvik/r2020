<?php

use Illuminate\Database\Seeder;

class UserCompany extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $todayDate = date('Y-m-d H:i:s');
        $json = file_get_contents('https://www.realopedia.com/index.php?route=restructure/users/Getcompany');
        $results = json_decode($json);

        foreach($results as $result){
            $check_user = DB::table('users')
                         ->select('id')
                         ->where('email', '=', $result->email)
                         ->get();
            
            if(count($check_user) == 0){
            
            $country = null;$zone = null;$city = null;
            if($result->country){
                $country_id = DB::table('countries')
                         ->select('id')
                         ->where('name', '=', $result->country)
                         ->get();
                $country = $country_id[0]->id;
            }

            if($result->zone){
                $zone_id = DB::table('zones')
                         ->select('id')
                         ->where('name', '=', $result->zone)
                         ->get();
                         if(count($zone_id) > 0){
                $zone = $zone_id[0]->id;}
            }
            
            if($result->city){
                $city_id = DB::table('cities')
                         ->select('id')
                         ->where('name', '=', $result->city)
                         ->get();
                         if(count($city_id) > 0){
                $city = $city_id[0]->id;}
            }
            
            if($result->group_flag){
                $getgroup_flag = DB::table('associations')
                         ->select('id')
                         ->where('code', '=', $result->group_flag)
                         ->get();

                         if(count($getgroup_flag) > 0){ 
                $group_flag = $getgroup_flag[0]->id;}else{$group_flag = '8';}

            }else{
                $group_flag = '8';
            }
            $package = $result->packageType;
            switch ($package) {
                case "Classic Plus":
                $package = 'Classic';
                break;
            }

            $packageType = DB::table('products')
            ->select('id')
            ->where('role_id', '=', '6')
            ->where('productName', 'like', '%'.$package.'%')
            ->get();
            
            $time = 12;
            $timePeriods = array(0=>'Select',1=>'1 Month',2=>'2 Months',3=>'3 Months',4=>'4 Months',5=>'5 Months',6=>'6 Months',7=>'7 Months',8=>'8 Months',9=>'9 Months',10=>'10 Months',11=>'11 Months',
            12=>'1 Year',24=>'2 Years',400=>'Lifetime');
            $time = array_search($result->timePeriod,$timePeriods);

            DB::table('users')->insert([
            [
                'parent_id'=>0,
                'role_id'=>6,
                'company_name'=>$result->company_name,
                'contact_name'=>$result->contact_name,
                'email'=>$result->email,
                'username'=>$result->username,
                'password'=>bcrypt($result->password),
                'license_number'=>$result->license_number,
                'license_expiry'=>$result->license_expiry,
                'country'=>$country,
                'zone'=>$zone,
                'city'=>$city,
                'mobile'=>$result->mobile,
                'telephone'=>$result->telephone,
                'email_alt'=>$result->email_alt,
                'user_image'=>$result->user_image,
                'language'=>$result->language,
                'specialities'=>$result->specialities,
                'occupation'=>$result->occupation,
                'gender'=>$result->gender,
                'social_media_links'=>$result->social_media_links,
                'website'=>$result->website,
                'group_flag'=>$group_flag,
                'refered_by'=>$result->refered_by,
                'realopedia_created_at'=>$result->realopedia_created_at,
                'rep_id'=>$result->rep_id,
                'is_active'=>$result->is_active,                
             'created_at' => $todayDate,'updated_at' => $todayDate,'created_by'=>1,'updated_by'=>1],
            ]);

        $id = DB::getPdo()->lastInsertId();
        DB::table('user_details')->insert([
            ['user_id'=>$id,'address'=>$result->address,'about_me'=>$result->about_me,'userdetail_banner'=>$result->userdetail_banner,'license_upload'=>$result->license_upload],
        ]);

        DB::table('subscriptions')->insert([
            ['user_id'=>$id,'packageType'=>$packageType[0]->id,'timePeriod'=>$time,'noComapnyAgents'=>$result->noComapnyAgents,
            'promocode'=>$result->promocode,'startDate'=>$result->startDate,'endDate'=>$result->endDate,'created_at'=>$todayDate,'updated_at'=>$todayDate],
        ]);
        $subscription_id = DB::getPdo()->lastInsertId();
        $gethistory = file_get_contents('https://www.realopedia.com/index.php?route=restructure/users/getSubscriptionHistory&user_id='.$result->user_id);
        $subHistory = json_decode($gethistory);

        if($subHistory){
            foreach($subHistory as $history){
                DB::table('subscription_histories')->insert([
                    ['user_id'=>$id,'subscription_id'=>$subscription_id,'packageType'=>$history->packageType,'timePeriod'=>$history->timePeriod,
                    'noComapnyAgents'=>$history->noComapnyAgents,'promocode'=>$history->promocode,'orderNumber'=>$history->orderNumber,
                    'productAmount'=>$history->productAmount,'vatAmount'=>$history->vatAmount ,'paymentStatus'=>$history->paymentStatus,
                    'paymentMethod'=>$history->paymentMethod,'paymentDate'=>$history->paymentDate,'remarks'=>$history->remarks,
                    'startDate'=>$history->startDate,'endDate'=>$history->endDate,'history_order'=>$history->history_order,
                    'created_at'=>$todayDate,'updated_at'=>$todayDate],
                ]);
            }
        }
        }
        echo $result->email.'<br>'; 
    }
    }
}
