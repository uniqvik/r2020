<?php

use Illuminate\Database\Seeder;

class DefaultUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('ALTER TABLE users AUTO_INCREMENT = 1');
        DB::statement('ALTER TABLE user_details AUTO_INCREMENT = 1');
        $todayDate = date('Y-m-d H:i:s');
        //Superadmin
        $id = DB::table('users')->insertGetId(
            ['role_id' => 1, 'parent_id' => 0, 'company_name'=>'Realopedia SuperAdmin',
                'contact_name'=>'Realopedia SuperAdmin', 'email'=>'developer_test@realopedia.com',
                'username'=>'Rsuperadmin', 'password'=>bcrypt('123$%^'), 'group_flag' => 2, 'created_at'=> $todayDate,
                'updated_at'=> $todayDate, 'realopedia_created_at'=> $todayDate, 'created_by' => 1, 'updated_by' => 1]
        );

        DB::table('user_details')->insert(
            ['user_id' => $id, 'address' => 'Dubai,UAE', 'about_me'=>'This account managed by developers']
        );

        //Admin
        $idA = DB::table('users')->insertGetId(
            ['role_id' => 2, 'parent_id' => 0, 'company_name'=>'Realopedia Admin',
                'contact_name'=>'Realopedia Admin', 'email'=>'itadmin_test@realopedia.com',
                'username'=>'Radmin', 'password'=>bcrypt('123$%^'), 'group_flag' => 2, 'created_at'=> $todayDate,
                'updated_at'=> $todayDate, 'realopedia_created_at'=> $todayDate, 'created_by' => 1, 'updated_by' => 1]
        );

        DB::table('user_details')->insert(
            ['user_id' => $idA, 'address' => 'Dubai,UAE', 'about_me'=>'This account managed by Admin']
        );
        //Support
        $idS = DB::table('users')->insertGetId(
            ['role_id' => 3, 'parent_id' => 0, 'company_name'=>'Realopedia Support',
                'contact_name'=>'Realopedia Support', 'email'=>'support_test@realopedia.com',
                'username'=>'Rsupport', 'password'=>bcrypt('123$%^'), 'group_flag' => 2, 'created_at'=> $todayDate,
                'updated_at'=> $todayDate, 'realopedia_created_at'=> $todayDate, 'created_by' => 1, 'updated_by' => 1]
        );

        DB::table('user_details')->insert(
            ['user_id' => $idS, 'address' => 'Dubai,UAE', 'about_me'=>'This account managed by Support']
        );
        //Accounts
        $idAc = DB::table('users')->insertGetId(
            ['role_id' => 4, 'parent_id' => 0, 'company_name'=>'Realopedia Accounts',
                'contact_name'=>'Realopedia Accounts', 'email'=>'accounts_test@realopedia.com',
                'username'=>'Raccounts', 'password'=>bcrypt('123$%^'), 'group_flag' => 2, 'created_at'=> $todayDate,
                'updated_at'=> $todayDate, 'realopedia_created_at'=> $todayDate, 'created_by' => 1, 'updated_by' => 1]
        );

        DB::table('user_details')->insert(
            ['user_id' => $idAc, 'address' => 'Dubai,UAE', 'about_me'=>'This account managed by Accounts']
        );

        //Marketing
        $idM = DB::table('users')->insertGetId(
            ['role_id' => 5, 'parent_id' => 0, 'company_name'=>'Realopedia Marketing',
                'contact_name'=>'Realopedia Marketing', 'email'=>'marketing_test@realopedia.com',
                'username'=>'Rmarketing', 'password'=>bcrypt('123$%^'), 'group_flag' => 2, 'created_at'=> $todayDate,
                'updated_at'=> $todayDate, 'realopedia_created_at'=> $todayDate, 'created_by' => 1, 'updated_by' => 1]
        );

        DB::table('user_details')->insert(
            ['user_id' => $idM, 'address' => 'Dubai,UAE', 'about_me'=>'This account managed by Marketing']
        );
    }
}
