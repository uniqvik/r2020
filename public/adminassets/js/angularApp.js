/**
 * Custom AngularJS App File
 * for populating and creating
 * form fields dynamically
 **/
var app = angular.module("dynamicFieldsPlugin", [])

.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
})

app.controller("dynamicFields", function($scope,$http) {

    $scope.choices = [[{id: 'timePeriod1', name: 'timePeriod'},{id:'price1',name:'price'},{id:'currency1',name:'currency'},{id:'frequency1',name:'frequency'},{id:'status1',name:'status'}]];

    $scope.addNewChoice = function() {
        var newItemNo = $scope.choices.length+1;
        //$scope.choices.push({'id' : 'choice' + newItemNo, 'name' : 'choice' + newItemNo});
        $scope.choices.push([{id: 'timePeriod'+ newItemNo, name: 'timePeriod'},
                            {id:'price'+ newItemNo,name:'price'},
                            {id:'currency'+ newItemNo,name:'currency'},
                            {id:'frequency'+ newItemNo,name:'frequency'},
                            {id:'status'+ newItemNo,name:'status'}
                            ]);
    };

    $scope.removeFrontChoice = function(val) {
       /* var newItemNo = $scope.choices.length-1;
        if (newItemNo !== 0) {
            $scope.choices.pop();
        }*/
        if (val !== 0) {
            $scope.choices.splice(val, 1);
        }
    };

   /* $scope.removeBackChoice = function($url) {

        $http({
            method: 'GET',
            url: $url
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    };*/


    $scope.showAddChoice = function(choice) {
        return choice.id === $scope.choices[$scope.choices.length-1].id;
    };

});
