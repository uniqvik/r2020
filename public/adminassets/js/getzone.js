var app = angular.module('location', [], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});
// app.config(['$locationProvider', function($locationProvider){
//   $locationProvider.html5Mode({
//     enabled: true,
//     requireBase: false
//     })
//   }])
app.controller('MainCtrl', function($scope,$http) {
  $scope.loadCountry = function(){
    $http({
        method: 'GET',
        url: 'getCountries'
    }).then(function successCallback(response) {
        $scope.countries = response.data;
        //$scope.country = $location.search().country;
    }, function errorCallback(response) {

    });
  }
  $scope.loadState = function(){
    $http({
      method: 'POST',
      data: { 'id' : $scope.country },
      url: 'getZonesbyId'
    }).then(function successCallback(response) {
        $scope.states = response.data;
        //$scope.state = $location.search().zone;
    }, function errorCallback(response) {

    });
  }

  $scope.loadCommunity = function(){
    $http({
      method: 'POST',
      data: { 'id' : $scope.country },
      url: 'getCommunitybyId'
    }).then(function successCallback(response) {
        $scope.communities = response.data;
        //$scope.state = $location.search().zone;
    }, function errorCallback(response) {

    });
  }
});
