<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    //
    protected $fillable = ['title','description'];

    public function emailTemps(){
        return $this->hasMany('App\EmailTemplate','campaign');
    }
}
