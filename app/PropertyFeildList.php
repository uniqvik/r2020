<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeildList extends Model
{
    protected $fillable = ['title','property_feild_id','property_type','is_active'];

    public function FeildName(){
        return $this->belongsTo('App\PropertyFeild','property_feild_id');
    }
}
