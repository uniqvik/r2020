<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Association extends Model
{
    //
    protected $fillable = ['title','description','code','created_by','updated_by'];

    public function users(){
        return $this->hasMany('App\User','group_flag');
    }
}
