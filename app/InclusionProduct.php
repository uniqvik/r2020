<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InclusionProduct extends Model
{
    protected $table = 'inclusion_product';
    protected $fillable = ['id','inclusion_id','product_id','value','order'];

    public $timestamps = false;

    public function inclusionDetail(){
        return $this->belongsTo('App\Inclusion','inclusion_id');
    }
}
