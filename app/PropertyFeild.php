<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeild extends Model
{
    protected $fillable = ['title','property_type','is_active'];

    public function FeildItems(){
        return $this->hasMany('App\PropertyFeildList','property_feild_id');
    }
}
