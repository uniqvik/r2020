<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsCategories extends Model
{
    protected $fillable = ['title','description','image','is_active','created_by','updated_by'];

    public function cmsItems(){
        return $this->hasMany('App\Cms','category_id');
    }
}
