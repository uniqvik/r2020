<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    //
    protected $fillable = ['replyTo','mail_from','mailSubject','campaign','description','type','header','footer'];

    public function campaigns(){
        return $this->belongsTo('App\Campaign','campaign');
    }

    public function Header(){
        return $this->belongsTo('App\EmailTemplate','header');
    }

    public function Footer(){
        return $this->belongsTo('App\EmailTemplate','footer');
    }
}
