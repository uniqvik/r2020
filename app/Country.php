<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = ['name','iso_code_2','iso_code_3','currency_code','latitude','longitude','description','image','created_by','updated_by'];

    public function users(){
        return $this->hasMany('App\User','country');
    }

}
