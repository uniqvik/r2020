<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class riaPermission extends Model
{
    //
    protected $fillable = ['role_id','access','list','view','add','edit','delete','search','status','export',];

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }
}
