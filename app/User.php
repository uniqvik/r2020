<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = ['id'];

    // protected $fillable = [
    //     'name', 'email', 'password','country','zone','city','community','subcommunity'
    // ];
    protected $dates = ['realopedia_created_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }
    public function parentRole()
    {
        return $this->belongsTo('App\user', 'parent_id');
    }
    public function userDetail()
    {
        return $this->hasOne('App\UserDetail', 'user_id');
    }
    public function association()
    {
        return $this->belongsTo('App\Association', 'group_flag');
    }

    public function countryDet(){
        return $this->belongsTo('App\Country','country');
    }

    public function state(){
        return $this->belongsTo('App\Zone','zone');
    }

    public function cityDet(){
        return $this->belongsTo('App\City','city');
    }

    public function communityDet(){
        return $this->belongsTo('App\Community','community');
    }

    public function subCommunity(){
        return $this->belongsTo('App\SubCommunity','subcommunity');
    }


    public function hasAccess($module,$modType)
    {
        $userRole = $this->role;
        if($module == 'permission' && $userRole->id == 1){return true;}
        if(strpos($module,',')){$accessModule = explode(',',$module);}
        else{$accessModule = array($module);}
        $userPermission = json_decode($userRole->riapermission,true);
        $userPermission = $userPermission[$modType];
        $userAcccess = explode(',',$userPermission);
        if(array_intersect($accessModule,$userAcccess)){return true;}
        return false;
    }
}
