<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

   /* public function users()
    {
        return $this->belongsToMany('App\User');
    }*/

    protected $fillable = ['title','type','parent_role','description','created_by','updated_by'];

   public function users(){
       return $this->hasMany('App\User','role_id')/*->where('is_active', 1)*/;
   }

    public function riapermission(){
        return $this->hasOne('App\RiaPermission','role_id');
    }

    public function products(){
        return $this->hasMany('App\Product','role_id')/*->where('is_active', 1)*/;
    }
}
