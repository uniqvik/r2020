<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    //
    protected $fillable = ['name','code','latitude','longitude','description','image','created_by','updated_by','country'];

    public function users(){
        return $this->hasMany('App\User','zone');
    }

    public function countryDet(){
        return $this->belongsTo('App\Country','country');
    }
}
