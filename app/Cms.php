<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $fillable = ['title','content','summary','category_id','image','is_active','created_by','updated_by'];

    public function cmsCat(){
    	return $this->belongsTo('App\CmsCategories','category_id');
    }
}
