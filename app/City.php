<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $fillable = ['name','iso_code_3','latitude','longitude','country','zone','description','image','created_by','updated_by'];

    public function users(){
        return $this->hasMany('App\User','city');
    }

    public function state(){
        return $this->belongsTo('App\Zone','zone');
    }

    public function countryDet(){
        return $this->belongsTo('App\Country','country');
    }
}
