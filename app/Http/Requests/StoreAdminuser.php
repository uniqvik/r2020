<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminuser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_name' => 'required',
            'email' => 'required|unique:users,email|unique:users,email_alt',
            'email_alt' => 'nullable|unique:users,email|unique:users,email_alt',
            'username' => 'required|unique:users',
            'password' => 'required',
            'group_flag'=> 'required',
            'role_id' => 'required',
            'country' => 'required',
            'zone' => 'required'
        ];
    }
}
