<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DemoController extends Controller
{

    public function index()
    {
        return view('admin.demo.log');
    }
    public function demodashboard()
    {
        return view('admin.demo.index');
    }

    public function companyprofile(){
        return view('admin.demo.comp');
    }

    public function myprofile(){
        return view('admin.demo.my');
    }
    public function users(){
        return view('admin.demo.users');
    }
    public function property(){
        return view('admin.demo.prop');
    }
}
