<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function publicroles()
    {
        $roles = array();
        $DBroles = DB::table('roles')->select('id','title')->where([['type', '=', 'website'],['is_active', '=', '1'],])->get()->toArray();
        foreach($DBroles as $DBrole){
            $roles[$DBrole->id] = ucwords($DBrole->title);
        }
        return $roles;
    }

    public function allRoles()
    {
        $roles = array();
        $DBroles = DB::table('roles')->select('id','title')->where([['is_active', '=', '1'],['title','!=','superadmin']])->get()->toArray();
        foreach($DBroles as $DBrole){
            $roles[$DBrole->id] = ucwords($DBrole->title);
        }
        return $roles;
    }

    public function userGroups()
    {
        $roles = array();
        $DBroles = DB::table('associations')->select('id','title')->where('is_active', '=', '1')->get()->toArray();
        foreach($DBroles as $DBrole){
            $roles[$DBrole->id] = ucwords($DBrole->title);
        }
        return $roles;
    }

    public function timeperiods()
    {
        $timePeriods = array(0=>'Select',1=>'1 month',2=>'2 months',3=>'3 months',4=>'4 months',5=>'5 months',6=>'6 months',7=>'7 months',8=>'8 months',9=>'9 months',10=>'10 months',11=>'11 months',
                            12=>'1 year',24=>'2 years',400=>'Lifetime');
        return $timePeriods;
    }

    public function status()
    {
        $statusArr = array(1=>'Active',/*2=>'Pending Approval',*/3=>'On Hold',/*4=>'Disabled',*/0=>'Inactive');
        return $statusArr;
    }
}
