<?php

namespace App\Http\Controllers\Admin;

use App\PropertyFeild;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminPropertyFeild extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'propertyfeild')) {
            $pageLimit = 20;

            if ($request) {
                $title = $request->title;
                $status = $request->status;
                $results = PropertyFeild::query();
                if($title){
                    $results =  $results->where('title', 'like','%'.$title.'%');
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $results =  $results->where('is_active',$status);
                }
                $results = $results->paginate($pageLimit)->appends(request()->query());
            } else {
                $results = PropertyFeild::paginate($pageLimit);
            }

            return view('admin.propertyfeild.propertyfeild.index', compact('results'));
        }
        if (Gate::denies('accessPermission', 'propertyfeild')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('addPermission', 'propertyfeild')) {
            $propertyfeild = new PropertyFeild;
            return view('admin.propertyfeild.propertyfeild.form',compact('propertyfeild'));
        }
        if (Gate::denies('addPermission', 'propertyfeild')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/propertyfeild');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        PropertyFeild::create($input);
        Session::flash('success_msg', 'Property Feild created!');
        return redirect('admin/propertyfeild');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'propertyfeild')) {
            $category = PropertyFeild::find($id);
            if ($category) {
                return view('admin.propertyfeild.propertyfeild.view',compact('category'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'propertyfeild')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('viewPermission', 'propertyfeild')) {
            $propertyfeild = PropertyFeild::find($id);
            if ($propertyfeild) {
                return view('admin.propertyfeild.propertyfeild.form',compact('propertyfeild'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'propertyfeild')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $PropertyFeild = PropertyFeild::find($id);
        if($PropertyFeild) {
            $update = $request->all();
            $PropertyFeild->update($update);
            Session::flash('success_msg','Property Feild updated! ');
        }
        else{Session::flash('fail_msg','Property Feild update failed! ');}
        return redirect('admin/propertyfeild');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'propertyfeild')) {
            $PropertyFeild = PropertyFeild::find($id);
            if ($PropertyFeild) {
                if ($PropertyFeild->FeildItems->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! Property Feild options available for this Property Feild.');
                } else {
                    $PropertyFeild->delete();
                    Session::flash('success_msg', 'Property Feild deleted! ');
                }
            } else {
                Session::flash('fail_msg', 'Property Feild not available! ');
            }
            return redirect('admin/propertyfeild');
        }
        if (Gate::denies('deletePermission', 'propertyfeild')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/propertyfeild');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'propertyfeild')) {
            $PropertyFeild = PropertyFeild::find($id);
            if ($PropertyFeild) {
                if ($PropertyFeild->is_active) {
                    $PropertyFeild->is_active = 0;
                    $PropertyFeild->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $PropertyFeild->is_active = 1;
                    $PropertyFeild->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/propertyfeild');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/propertyfeild');
            }
        }

        if (Gate::denies('statusPermission', 'propertyfeild')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/propertyfeild');
        }
    }
}
