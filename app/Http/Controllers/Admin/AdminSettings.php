<?php

namespace App\Http\Controllers\Admin;


use App\Settings;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminSettings extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'setting')) {
            $pageLimit = 20;
            
            if ($request) {
                $title = $request->title;
                $type = $request->type;
                $status = $request->status;
                $settings = Settings::query(); 
                if($title){
                    $settings =  $settings->where('title', 'like','%'.$title.'%');
                }
                if($type){
                    $settings =  $settings->where('type',$type);
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $settings =  $settings->where('is_active',$status);
                }
                $settings = $settings->paginate($pageLimit)->appends(request()->query());
            } else {
                $settings = Settings::paginate($pageLimit);
            }
            
            return view('admin.setting.index', compact('settings')); 
        }
        if (Gate::denies('accessPermission', 'setting')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('addPermission', 'setting')) { 
            $setting = new Settings;
            return view('admin.setting.form',compact('setting'));
        }
        if (Gate::denies('addPermission', 'setting')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/setting');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        Settings::create($input);
        Session::flash('success_msg', 'Setting created!');
        return redirect('admin/setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'setting')) {
            $setting = Settings::find($id);
            if ($setting) {
                return view('admin.setting.view',compact('setting'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'setting')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/setting');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('editPermission', 'setting')) {
            
            $setting = Settings::find($id);
            if ($setting) {
                return view('admin.setting.form',compact('setting'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'setting')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/setting');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting = Settings::find($id);
        if($setting) {
            $update = $request->all();
            
            $setting->update($update);
            Session::flash('success_msg','Setting updated! ');
        }
        else{Session::flash('fail_msg','Setting update failed! ');}
        return redirect('admin/setting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'setting')) {
            $setting = Settings::find($id);
            if ($setting) {
               
                    $setting->delete();
                    Session::flash('success_msg', 'Setting deleted! ');
                
            } else {
                Session::flash('fail_msg', 'Setting not available! ');
            }
            return redirect('admin/setting');
        }
        if (Gate::denies('deletePermission', 'setting')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/cms');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'setting')) {
            $setting = Settings::find($id);
            if ($setting) {
                if ($setting->is_active) {
                    $setting->is_active = 0;
                    
                    $setting->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $setting->is_active = 1;
                
                    $setting->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/setting');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/setting');
            }
        }

        if (Gate::denies('statusPermission', 'setting')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/setting');
        }
    }
}