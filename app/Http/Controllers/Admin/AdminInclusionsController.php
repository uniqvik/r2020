<?php

namespace App\Http\Controllers\Admin;

use App\Inclusion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminInclusionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if(Gate::allows('accessPermission', 'inclusion')) {
            $pageLimit = 20;
            $qryVal = $request->name;
            if ($qryVal) {
                $inclusions = Inclusion::where('name', 'like', $qryVal)->paginate($pageLimit);
            } else {
                $inclusions = Inclusion::paginate($pageLimit);
            }
            return view('admin.inclusions.index', compact('inclusions'));
        }
        if (Gate::denies('accessPermission', 'inclusion')) {
            return redirect('admin/errors/404');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Gate::allows('addPermission', 'inclusion')) {
            $subscriptionProducts = array();
            $publicRoles = $this->publicroles();
            $product_inclusion = array();
            foreach ($publicRoles as $key=>$val){
                $roleProducts = DB::table('products')->select('id','productName')->where([['role_id', '=', $key],['is_active', '=', '1'],['type','=','subscription']])->get()->toArray();
                $valArray = array();
                foreach ($roleProducts as $rp){
                    $valArray[$rp->id] = $rp->productName;
                    $subscriptionProducts[$val]= $valArray;
                }
            }
            //$productInclusions = ['Agent'=>array(2=>'Classic',5=>'Premium',6=>'Premium+'),'Company'=>[1=>'Classic',7=>'Global Corporate Packages'],'Developer'=>[3=>'Classic',8=>'Premium'],'Corporate Affiliate'=>[4=>'Classic',9=>'Premium']];
            return view('admin.inclusions.form',compact('subscriptionProducts','product_inclusion'));
        }
        if (Gate::denies('addPermission', 'inclusion')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/inclusions');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input['name']=$request->input('name');
        $input['slug']=$request->input('slug');
        //$input['productinclusions']=$request->input('productinclusions');
        $input['created_by'] = auth()->user()->id;
        $input['updated_by'] = auth()->user()->id;
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');
        $incID = Inclusion::insertGetId($input);
        if($incID){
            $inclusion = Inclusion::find($incID);
            if($request->input('prodInclu')){$inclusion->products()->sync($request->input('prodInclu'));}

        }
        Session::flash('success_msg', 'Inclusion created ! ');
        return redirect('/admin/inclusions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Gate::allows('editPermission', 'inclusion')) {
            $inclusion = Inclusion::find($id);
            $product_inclusion = array();
            foreach ($inclusion->products as $inc) {
                $product_inclusion[] = $inc->pivot->product_id;
            }
            $subscriptionProducts = array();
            $publicRoles = $this->publicroles();
            foreach ($publicRoles as $key=>$val) {
                $roleProducts = DB::table('products')->select('id', 'productName')->where([['role_id', '=', $key], ['is_active', '=', '1'], ['type', '=', 'subscription']])->get()->toArray();
                $valArray = array();
                foreach ($roleProducts as $rp) {
                    $valArray[$rp->id] = $rp->productName;
                    $subscriptionProducts[$val] = $valArray;
                }
            }
            return view('admin.inclusions.view',compact(['inclusion','subscriptionProducts','product_inclusion']));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'inclusion')) {
            $inclusion = Inclusion::find($id);
            $product_inclusion = array();
            foreach ($inclusion->products as $inc) {
                $product_inclusion[] = $inc->pivot->product_id;
            }
            $subscriptionProducts = array();
            $publicRoles = $this->publicroles();
            foreach ($publicRoles as $key=>$val) {
                $roleProducts = DB::table('products')->select('id', 'productName')->where([['role_id', '=', $key], ['is_active', '=', '1'], ['type', '=', 'subscription']])->get()->toArray();
                $valArray = array();
                foreach ($roleProducts as $rp) {
                    $valArray[$rp->id] = $rp->productName;
                    $subscriptionProducts[$val] = $valArray;
                }
            }
            return view('admin.inclusions.form',compact(['inclusion','subscriptionProducts','product_inclusion']));
        }
        if (Gate::denies('editPermission', 'inclusion')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/inclusions');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $inclusion = Inclusion::find($id);
        if($inclusion) {
            $update = $request->all();
            $update['updated_by'] = auth()->user()->id;
            $update['updated_at'] = date('Y-m-d H:i:s');
            $inclusion->update($update);
            $inclusion->products()->sync($request->prodInclu);
            Session::flash('success_msg', 'Inclusion updated ! ');
            return redirect('/admin/inclusions');
        }
        else{
            Session::flash('fail_msg', 'Update Failed ! ');
            return redirect('/admin/inclusions');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'inclusion')) {
            $inclusion = Inclusion::find($id);
            if($inclusion){
                $inclusion->delete();
                Session::flash('success_msg','Inclusion deleted! ');

            }
            else{ Session::flash('fail_msg','Inclusion not available! ');}
            return redirect('/admin/inclusions');
        }
        if (Gate::denies('deletePermission', 'inclusion')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/inclusions');
        }
    }


    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'inclusion')) {
            $inclusion = Inclusion::find($id);
            if ($inclusion){
                if ($inclusion->is_active) {
                    $inclusion->is_active = 0;
                    $inclusion->updated_by = auth()->user()->id;
                    $inclusion->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $inclusion->is_active = 1;
                    $inclusion->updated_by = auth()->user()->id;
                    $inclusion->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/inclusions');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('/admin/inclusions');
            }
        }
        if (Gate::denies('statusPermission', 'inclusion')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/inclusions');
        }
    }
}
