<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SubCommunity;
use App\Community;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;

class AdminSubCommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if (Gate::allows('accessPermission', 'location')) {
            $pageLimit = 20;

            if ($request) {
                $name = $request->name;
                $country = $request->country;
                $community = $request->community;
                $status = $request->status;
                $subcommunities = SubCommunity::query();
                if($country){
                    $subcommunities =  $subcommunities->where('country',$country);
                }
                if($community){
                    $subcommunities =  $subcommunities->where('community_id',$community);
                }
                if($name){
                    $subcommunities =  $subcommunities->where('name', 'like','%'.$name.'%');
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $subcommunities =  $subcommunities->where('is_active',$status);
                }
                $subcommunities = $subcommunities->paginate($pageLimit)->appends(request()->query());

                //$subcommunities = SubCommunity::where('name', 'like', $qryVal)->paginate($pageLimit);
            } else {
                $subcommunities = SubCommunity::paginate($pageLimit);
            }
            return view('admin.location.subcommunity.index', compact('subcommunities'));
        }
        if (Gate::denies('accessPermission', 'location')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Gate::allows('addPermission', 'location')) {
            $communities = Community::pluck('name', 'id')->toArray();
            $subcommunity = new SubCommunity;
            return view('admin.location.subcommunity.form',compact('communities','subcommunity'));
        }
        if (Gate::denies('addPermission', 'location')) {
            return view('admin.location.subcommunity.form');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        SubCommunity::create($request->all());
        Session::flash('success_msg', 'subcommunity created ! ');
        return redirect('/admin/location/subcommunity');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'location')) {
            $subcommunity = SubCommunity::find($id);
            if ($subcommunity) {
                return view('admin.location.subcommunity.view', compact('subcommunity'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/subcommunity');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'location')) {
            $subcommunity = SubCommunity::find($id);
            $communities = Community::pluck('name', 'id')->toArray();;
            if ($subcommunity) {
                return view('admin.location.subcommunity.form', compact('subcommunity','communities'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/subcommunity');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $subcommunity = SubCommunity::find($id);
        if ($subcommunity) {
            $subcommunity->update($request->all());
            Session::flash('success_msg', 'subcommunity updated! ');
        } else {
            Session::flash('fail_msg', 'subcommunity update failed! ');
        }
        return redirect('/admin/location/subcommunity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'location')) {
            $subcommunity = SubCommunity::find($id);
            if ($subcommunity) {
                if ($subcommunity->users->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! User Accounts available for this community.');
                } else {
                    $subcommunity->delete();
                    Session::flash('success_msg', 'subcommunity deleted!');
                }
            } else {
                Session::flash('fail_msg', 'subcommunity not available! ');
            }
            return redirect('/admin/location/subcommunity');
        }
        if (Gate::denies('deletePermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/subcommunity');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'location')) {
            $subcommunity = SubCommunity::find($id);
            if ($subcommunity) {
                if ($subcommunity->is_active) {
                    $subcommunity->is_active = 0;
                    $subcommunity->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $subcommunity->is_active = 1;
                    $subcommunity->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/location/subcommunity');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ' );
                return redirect('/admin/location/subcommunity');
            }
        }
        if (Gate::denies('statusPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/subcommunity');
        }


    }
}
