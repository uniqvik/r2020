<?php

namespace App\Http\Controllers\Admin;

use App\Seo;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminSeoManagement extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'seo')) {
            $pageLimit = 20;
             
            if ($request) {
                $page = $request->page;
                $keyword = $request->keyword;
                $description = $request->description;
                $status = $request->status;
                $seos = Seo::query();
                if($page){
                    $seos =  $seos->where('page', 'like','%'.$page.'%');
                }
                if($keyword){
                    $seos =  $seos->where('meta_keywords', 'like','%'.$keyword.'%');
                }
                if($description){
                    $seos =  $seos->where('meta_description', 'like','%'.$description.'%');
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $seos =  $seos->where('is_active',$status);
                }
                $seos = $seos->paginate($pageLimit)->appends(request()->query());
            } else {
                $seos = Seo::paginate($pageLimit);
            }
           
            return view('admin.seo.index', compact('seos'));
        }
        if (Gate::denies('accessPermission', 'seo')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $columns = \DB::connection()->getSchemaBuilder()->getColumnListing("users");
        // dd($columns);
        if (Gate::allows('addPermission', 'seo')) {
            return view('admin.seo.form');
        }
        if (Gate::denies('addPermission', 'seo')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/seo');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Seo::create($input);
        Session::flash('success_msg', 'SEO created!');
        return redirect('admin/seo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'seo')) {
            $seo = Seo::find($id);
            if ($seo) {
                return view('admin.seo.view',compact('seo'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'seo')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/seo');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('viewPermission', 'seo')) {
            $seo = Seo::find($id);
            if ($seo) {
                return view('admin.seo.form',compact('seo'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'seo')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/seo');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seo = Seo::find($id);
        if($seo) {
            $update = $request->all();
            
            //$update['updated_by'] = auth()->user()->id ;
            $seo->update($update);
            Session::flash('success_msg','SEO updated! ');
        }
        else{Session::flash('fail_msg','SEO update failed! ');}
        return redirect('admin/seo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'seo')) {
            $seo = Seo::find($id);
            if ($seo) {
                    $seo->delete();
                    Session::flash('success_msg', 'SEO deleted! ');
            } else {
                Session::flash('fail_msg', 'SEO not available! ');
            }
            return redirect('admin/seo');
        }
        if (Gate::denies('deletePermission', 'seo')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/seo');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'seo')) {
            $seo = Seo::find($id);
            if ($seo) {
                if ($seo->is_active) {
                    $seo->is_active = 0;
                    $seo->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $seo->is_active = 1;
                    $seo->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/seo');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/seo');
            }
        }

        if (Gate::denies('statusPermission', 'seo')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/seo');
        }
    }
}
