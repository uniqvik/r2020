<?php

namespace App\Http\Controllers\Admin;

use App\CmsCategories;
use App\Cms;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminCms extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'cms')) {
            $pageLimit = 20;
            $categories = CmsCategories::all();
            if ($request) {
                $title = $request->title;
                $status = $request->status;
                $category_id = $request->category_id;
                $cmslist = Cms::query(); 
                if($title){
                    $cmslist =  $cmslist->where('title', 'like','%'.$title.'%');
                }
                if($category_id){
                    $cmslist =  $cmslist->where('category_id',$category_id);
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $cmslist =  $cmslist->where('is_active',$status);
                }
                $cmslist = $cmslist->paginate($pageLimit)->appends(request()->query());
            } else {
                $cmslist = Cms::paginate($pageLimit);
            }
            
            return view('admin.cms.cmslist.index', compact('categories','cmslist'));
        }
        if (Gate::denies('accessPermission', 'cms')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('addPermission', 'cms')) {
            
            $categories = CmsCategories::pluck('title', 'id')->toArray();
            return view('admin.cms.cmslist.form',compact('categories'));
        }
        if (Gate::denies('addPermission', 'cms')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        if ($request->hasFile('image')) {
            $this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
            $image = $request->file('image');
            $name = str_slug($request->title).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/cms');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $input['image'] = '/images/cms/'.$name;
        }
        $input['created_by'] = auth()->user()->id;
        $input['updated_by'] = auth()->user()->id;
        Cms::create($input);
        Session::flash('success_msg', 'Cms created!');
        return redirect('admin/cms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'cms')) {
            $cms = Cms::find($id);
            if ($cms) {
                return view('admin.cms.cmslist.view',compact('cms'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'cms')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/cms');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('editPermission', 'cms')) {
            $categories = CmsCategories::pluck('title', 'id')->toArray();
            $cms = Cms::find($id);
            if ($cms) {
                return view('admin.cms.cmslist.form',compact('categories','cms'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'cms')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/cms');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cms = Cms::find($id);
        if($cms) {
            $update = $request->all();
            if ($request->hasFile('image')) {
                $this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
                $image = $request->file('image');
                $name = str_slug($request->title).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/cms');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $update['image'] = '/images/cms/'.$name;
            }
            $update['updated_by'] = auth()->user()->id ;
            $cms->update($update);
            Session::flash('success_msg','CMS updated! ');
        }
        else{Session::flash('fail_msg','CMS update failed! ');}
        return redirect('admin/cms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'cms')) {
            $cms = Cms::find($id);
            if ($cms) {
               
                    $cms->delete();
                    Session::flash('success_msg', 'CMS deleted! ');
                
            } else {
                Session::flash('fail_msg', 'CMS not available! ');
            }
            return redirect('admin/cms');
        }
        if (Gate::denies('deletePermission', 'cms')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/cms');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'cms')) {
            $cms = Cms::find($id);
            if ($cms) {
                if ($cms->is_active) {
                    $cms->is_active = 0;
                    $cms->updated_by = auth()->user()->id;
                    $cms->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $cms->is_active = 1;
                    $cms->updated_by = auth()->user()->id;
                    $cms->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/cms');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/cms');
            }
        }

        if (Gate::denies('statusPermission', 'cms')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/cms');
        }
    }
}
