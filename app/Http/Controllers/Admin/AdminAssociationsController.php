<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;
use App\Association;

class AdminAssociationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //
        if (Gate::allows('accessPermission', 'association')) {
            $pageLimit = 20;
            $qryVal = $request->title;
            if ($qryVal) {
                $associations = Association::where('title', 'like', $qryVal)->paginate($pageLimit);
            } else {
                $associations = Association::paginate($pageLimit);
            }
            return view('admin.associations.index', compact('associations'));
        }
        if (Gate::denies('accessPermission', 'association')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Gate::allows('addPermission', 'association')) {
            $association = new Association;
            return view('admin.associations.form' ,compact('association'));
        }
        if (Gate::denies('addPermission', 'association')) {
            Session::flash('fail_msg', 'No permission! Contact administrator! ');
            return redirect('/admin/associations');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $input['created_by'] = auth()->user()->id;
        $input['updated_by'] = auth()->user()->id;
        Association::create($input);
        Session::flash('success_msg', 'Association created!');
        return redirect('/admin/associations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'association')) {
            $association = Association::find($id);
            if ($association) {
                return view('admin.associations.form', compact('association'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'association')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/associations');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $association = Association::find($id);
        $update = $request->all();
        $update['updated_by'] = auth()->user()->id ;
        if ($association) {
            $association->update($update);
            Session::flash('success_msg', 'Association updated! ');
        } else {
            Session::flash('fail_msg', 'Association update failed! ');
        }
        return redirect('/admin/associations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'association')) {
            $association = Association::find($id);
            if ($association) {
                if ($association->users->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! User Accounts available for this association.');
                } else {
                    $association->delete();
                    Session::flash('success_msg', 'Association deleted! ');
                }
            } else {
                Session::flash('fail_msg', 'Association not available! ');
            }
            return redirect('/admin/associations');
        }
        if (Gate::denies('deletePermission', 'association')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/associations');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'association')) {
            $association = Association::find($id);
            if ($association) {
                if ($association->is_active) {
                    $association->is_active = 0;
                    $association->updated_by = auth()->user()->id;
                    $association->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $association->is_active = 1;
                    $association->updated_by = auth()->user()->id;
                    $association->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/associations');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('/admin/associations');
            }
        }

        if (Gate::denies('statusPermission', 'association')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/associations');
        }
    }
}
