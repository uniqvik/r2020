<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Community;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;

class AdminCommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if (Gate::allows('accessPermission', 'location')) {
            $pageLimit = 20;

            if ($request) {
                $communities = Community::query();
                $name = $request->name;
                $country = $request->country;
                $zone = $request->zone;
                $status = $request->status;
                if($name){
                    $communities =  $communities->where('name', 'like','%'.$name.'%');
                }
                if($country){
                    $communities =  $communities->where('country',$country);
                }
                if($zone){
                    $communities =  $communities->where('zone_id',$zone);
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $communities =  $communities->where('is_active',$status);
                }
                $communities = $communities->paginate($pageLimit)->appends(request()->query());
            } else {
                $communities = Community::paginate($pageLimit);
            }
            return view('admin.location.community.index', compact('communities'));
        }
        if (Gate::denies('accessPermission', 'location')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        if (Gate::allows('addPermission', 'location')) {
            $zones = Zone::pluck('name', 'id')->toArray();
            $community = new Community;
            return view('admin.location.community.form',compact('zones','community'));
        }
        if (Gate::denies('addPermission', 'location')) {
            return view('admin.location.community.form');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Community::create($request->all());
        Session::flash('success_msg', 'Community created ! ');
        return redirect('/admin/location/community');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'location')) {
            $community = Community::find($id);
            if ($community) {
                return view('admin.location.community.view', compact('community'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/community');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'location')) {
            $community = Community::find($id);
            if ($community) {
                $zones = Zone::pluck('name', 'id')->toArray();
                return view('admin.location.community.form', compact('community','zones'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/community');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $community = Community::find($id);
        if ($community) {
            $community->update($request->all());
            Session::flash('success_msg', 'Community updated! ');
        } else {
            Session::flash('fail_msg', 'Community update failed! ');
        }
        return redirect('/admin/location/community');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'location')) {
            $community = Community::find($id);
            if ($community) {
                if ($community->users->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! User Accounts available for this community.');
                } else {
                    $community->delete();
                    Session::flash('success_msg', 'Community deleted!');
                }
            } else {
                Session::flash('fail_msg', 'Community not available! ');
            }
            return redirect('/admin/location/community');
        }
        if (Gate::denies('deletePermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/community');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'location')) {
            $community = Community::find($id);
            if ($community) {
                if ($community->is_active) {
                    $community->is_active = 0;
                    $community->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $community->is_active = 1;
                    $community->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/location/community');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ' );
                return redirect('/admin/location/community');
            }
        }
        if (Gate::denies('statusPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/community');
        }


    }
    public function getCommunitybyId(Request $request)
    {
        if($request->id)
        {
            $community = Community::query();

            $community =  $community->where('country',$request->id);

            return $community->get();
        }
    }
}
