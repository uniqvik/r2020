<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\City;
use App\Zone;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;

class AdminCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');

    }


    public function index(Request $request)
    {
        //
        if (Gate::allows('accessPermission', 'location')) {
            $pageLimit = 20;
            if ($request) {
                $name = $request->name;
                $country = $request->country;
                $zone = $request->zone;
                $status = $request->status;
                $cities = City::query();
                if($name){
                    $cities =  $cities->where('name', 'like','%'.$name.'%');
                }
                if($country){
                    $cities =  $cities->where('country',$country);
                }
                if($zone){
                    $cities =  $cities->where('zone',$zone);
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $cities =  $cities->where('is_active',$status);
                }
                $cities = $cities->paginate($pageLimit)->appends(request()->query());
            } else {
                $cities = City::paginate($pageLimit);

            }
            return view('admin.location.city.index', compact('cities'));
        }
        if (Gate::denies('accessPermission', 'location')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        if (Gate::allows('addPermission', 'location')) {
            $zones= Zone::pluck('name', 'id')->toArray();
            $city = new City;
            return view('admin.location.city.form',compact('zones','city'));
        }
        if (Gate::denies('addPermission', 'location')) {
            return view('admin.location.city.form');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        City::create($request->all());
        Session::flash('success_msg', 'City created ! ');
        return redirect('/admin/location/city');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'location')) {
            $city = City::find($id);
            if ($city) {
                return view('admin.location.city.view', compact('city'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/city');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $zones= Zone::pluck('name', 'id')->toArray();
        if (Gate::allows('editPermission', 'location')) {
            $city = City::find($id);
            if ($city) {
                return view('admin.location.city.form', compact('city','zones'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/city');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $city = City::find($id);
        if ($city) {
            $city->update($request->all());
            Session::flash('success_msg', 'City updated! ');
        } else {
            Session::flash('fail_msg', 'City update failed! ');
        }
        return redirect('/admin/location/city');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'location')) {
            $city = City::find($id);
            if ($city) {
                if ($city->users->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! User Accounts available for this city.');
                } else {
                    $city->delete();
                    Session::flash('success_msg', 'City deleted!');
                }
            } else {
                Session::flash('fail_msg', 'City not available! ');
            }
            return redirect('/admin/location/city');
        }
        if (Gate::denies('deletePermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/city');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'location')) {
            $city = City::find($id);
            if ($city) {
                if ($city->is_active) {
                    $city->is_active = 0;
                    $city->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $city->is_active = 1;
                    $city->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/location/city');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ' );
                return redirect('/admin/location/city');
            }
        }
        if (Gate::denies('statusPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/city');
        }


    }

    public function getCitiesbyId(Request $request)
    {
        if($request->id)
        {
            $cities = City::query();
            $cities =  $cities->where('zone',$request->id);
            return $cities->get();
        }
    }
}
