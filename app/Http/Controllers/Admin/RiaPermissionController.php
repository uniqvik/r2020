<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\RiaPermission;

class RiaPermissionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index($id)
    {
        //
        if(Gate::allows('accessPermission', 'permission')) {
        $permission = RiaPermission::where('role_id', '=', $id)->first();
        if($permission) {
            return view('admin.permissions.index',compact('permission'));
        }
        else{Session::flash('fail_msg','Permissions not available! ');
            return redirect('/admin/roles');
        }
        }
        if (Gate::denies('accessPermission', 'permission')) {
            return redirect('admin/errors/404');
        }

    }

    public function update(Request $request){
        if(!$request->id) {Session::flash('fail_msg','Permissions not available! ');
            return redirect('/admin/roles');}
        $accessStr = $listStr = $searchStr = $viewStr = $addStr = $editStr = $deleteStr = $statusStr = $exportStr = '';
        $access = $request->access; $accessStr = ($access) ? implode($access,',') : '';
        $list = $request->list; $listStr =  ($list) ? implode($list,',') : '';
        $search = $request->search; $searchStr = ($search) ? implode($search,',') : '';
        $view = $request->view; $viewStr = ($view) ? implode($view,',') : '';
        $add = $request->add;  $addStr =  ($add) ? implode($add,',') : '';
        $edit = $request->edit; $editStr =  ($edit) ? implode($edit,',') : '';
        $delete = $request->delete; $deleteStr =  ($delete) ? implode($delete,',') : '';
        $status = $request->status; $statusStr =  ($status) ? implode($status,',') : '';
        $export = $request->export; $exportStr = ($export) ? implode($export,',') : '';

        if($request->id) {
            RiaPermission::where('role_id', $request->id)->update(['access' => $accessStr, 'list' => $listStr, 'search' => $searchStr, 'view' => $viewStr, 'add' => $addStr, 'edit' => $editStr,
                'delete' => $deleteStr, 'status' => $statusStr, 'export' => $exportStr,'updated_by' => auth()->user()->id]);
            Session::flash('success_msg', 'Permission updated! ');
            return redirect('/admin/roles');
        }
        else{Session::flash('fail_msg','Permissions not available! ');
            return redirect('/admin/roles');
        }

    }
}
