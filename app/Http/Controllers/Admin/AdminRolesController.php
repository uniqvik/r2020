<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use App\Role;


use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;


class AdminRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if(Gate::allows('accessPermission', 'role')) {
            $pageLimit = 20;
            $qryVal = $request->title;
            if ($qryVal) {
                $roles = Role::where('title', 'like', $qryVal)->paginate($pageLimit);
            } else {
                $roles = Role::paginate($pageLimit);
            }
            return view('admin.roles.index', compact('roles'));
        }
        if (Gate::denies('accessPermission', 'role')) {
            return redirect('admin/errors/404');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Gate::allows('addPermission', 'role')) {
            $publicroles = array(0=>'Select')+$this->publicroles();
            $role = new Role;
             return view('admin.roles.form', compact('publicroles','role'));
        }
        if (Gate::denies('addPermission', 'role')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/roles');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $todayDate = date('Y-m-d H:i:s');
        if($request->type == 'RIA') {
            $id = Role::insertGetId(['title'=>$request->title,'type'=>$request->type,'description'=>$request->description,'created_at'=>$todayDate,'parent_role'=>$request->parent_role,
                'updated_at'=>$todayDate,'created_by' => auth()->user()->id, 'updated_by' => auth()->user()->id]);
            DB::table('ria_permissions')->insert(['role_id' => $id, 'access' => 'dashboard', 'list' => '', 'view' => '',
                'search' => '', 'add' => '', 'edit' => '', 'delete' => '', 'status' => '', 'export' => '',
                'created_at' => $todayDate, 'updated_at' => $todayDate,'created_by' => auth()->user()->id, 'updated_by' => auth()->user()->id]);
            Session::flash('success_msg', 'Role created ! ');
        }
        else{
            $id = Role::insertGetId(['title'=>$request->title,'type'=>$request->type,'description'=>$request->description,'created_at'=>$todayDate,'updated_at'=>$todayDate,'parent_role'=>$request->parent_role,
                'created_by' => auth()->user()->id, 'updated_by' => auth()->user()->id]);
            Session::flash('success_msg', 'Role created ! ');}
        return redirect('/admin/roles');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'role')) {
            $role = Role::find($id);
            if ($role) {
                $publicroles = array(0=>'Select')+$this->publicroles();
                //dd($publicroles);
                return view('admin.roles.form', compact('role','publicroles'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'role')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/roles');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $role = Role::find($id);
        if($role) {
            $update = $request->all();
            $update['updated_by'] = auth()->user()->id ;
            $role->update($update);
            Session::flash('success_msg','Role updated! ');
        }
        else{Session::flash('fail_msg','Role update failed! ');}
        return redirect('/admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'role')) {
        $role = Role::find($id);
        if($role){
            if($role->users->isNotEmpty()){
                Session::flash('fail_msg','Delete Failed! User Accounts available for this role.');
            }
            else {
                $role->delete();
                Session::flash('success_msg','Role deleted! ');
            }
        }
        else{ Session::flash('fail_msg','Role not available! ');}
        return redirect('/admin/roles');
        }
        if (Gate::denies('deletePermission', 'role')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/roles');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'role')) {
            $role = Role::find($id);
            if ($role) {
                if ($role->is_active) {
                    $role->is_active = 0;
                    $role->updated_by = auth()->user()->id;
                    $role->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $role->is_active = 1;
                    $role->updated_by = auth()->user()->id;
                    $role->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/roles');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('/admin/roles');
            }
        }
        if (Gate::denies('statusPermission', 'role')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/roles');
        }

    }

}
