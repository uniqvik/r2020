<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Property;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminProperty extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'property')) {
            $pageLimit = 20;
            
            $unit = $request->unit;

            if ($request){
                $properties = Property::query();

                $country = $request->country;
                $zone = $request->zone;
                $title = $request->title;
                $reference_mls = $request->reference_mls;
                $property_type = $request->property_type;
                $category = $request->category;
                if($country){
                    $properties =  $properties->where('country_id',$country);
                }
                if($zone){
                    $properties =  $properties->where('zone_id',$zone);
                }
                if($title){
                    $properties =  $properties->where('title', 'like','%'.$title.'%');
                }
                if($reference_mls){
                    $properties =  $properties->where('reference_mls', 'like','%'.$reference_mls.'%');
                }
                if($property_type){
                    $properties =  $properties->where('property_type',$property_type);
                }
                if($category){
                    $properties =  $properties->where('ad_type',$category);
                }
                $properties = $properties->paginate($pageLimit)->appends(request()->query());
            }
            return view('admin.property.index',compact('properties')); 
        }

        if (Gate::denies('accessPermission', 'property')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function statusChange($id)
    {}
}
