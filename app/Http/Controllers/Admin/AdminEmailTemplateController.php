<?php

namespace App\Http\Controllers\Admin;

use App\EmailTemplate;
use App\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminEmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if(Gate::allows('accessPermission', 'email')) {
            $pageLimit = 20;
            $campaigns = Campaign::all();

            if ($request) {
                $mailSubject = $request->mailSubject;
                $type = $request->type;
                $campaign = $request->campaign;
                $status = $request->status;

                $emailTemps = EmailTemplate::query();
                if($mailSubject){
                    $emailTemps =  $emailTemps->where('mailSubject', 'like','%'.$mailSubject.'%');
                }
                if($type){
                    $emailTemps =  $emailTemps->where('type', $type);
                }
                if($campaign){
                    $emailTemps =  $emailTemps->where('campaign', $campaign);
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $emailTemps =  $emailTemps->where('is_active',$status);
                }
                $emailTemps = $emailTemps->paginate($pageLimit)->appends(request()->query());
            } else {
                $emailTemps = EmailTemplate::paginate($pageLimit);
            }
            return view('admin.emails.template.index', compact('emailTemps','campaigns'));
        }
        if (Gate::denies('accessPermission', 'email')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('addPermission', 'email')) {
            $template = new EmailTemplate;
            $campaigns = Campaign::all();
            $header = EmailTemplate::where('type', 'header')->get();
            $footer = EmailTemplate::where('type', 'footer')->get();
            return view('admin.emails.template.form',compact('template','campaigns','header','footer'));
        }
        if (Gate::denies('addPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/template');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //dd($input);
        if($input['type'] == 'mail_body')
        {
            $input['campaign'] = $input['campaign'];
        }else{$input['campaign'] = '1';}
        $input['created_by'] = auth()->user()->id;
        $input['updated_by'] = auth()->user()->id;
        EmailTemplate::create($input);
        Session::flash('success_msg', 'Email Template created!');
        return redirect('admin/email/template');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'email')) {

            $template = Emailtemplate::find($id);
            if ($template) {
                return view('admin.emails.template.view',compact('template'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/template');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('editPermission', 'email')) {

            $template = Emailtemplate::find($id);
            if ($template) {
                $campaigns = Campaign::all();
                $header = EmailTemplate::where('type', 'header')->get();
                $footer = EmailTemplate::where('type', 'footer')->get();
                return view('admin.emails.template.form',compact('template','campaigns','header','footer'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/template');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $template = Emailtemplate::find($id);
        if($template) {
            $update = $request->all();
            $update['updated_by'] = auth()->user()->id ;
            $template->update($update);
            Session::flash('success_msg','Email Template updated! ');
        }
        else{Session::flash('fail_msg','Email Template update failed! ');}
        return redirect('admin/email/template');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'email')) {
            $template = Emailtemplate::find($id);
            if ($template) {
                $template->delete();
                Session::flash('success_msg', 'Email Template deleted! ');
            } else {
                Session::flash('fail_msg', 'Email Template not available! ');
            }
            return redirect('admin/email/template');
        }
        if (Gate::denies('deletePermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/template');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'email')) {
            $template = Emailtemplate::find($id);
            if ($template) {
                if ($template->is_active) {
                    $template->is_active = 0;
                    $template->updated_by = auth()->user()->id;
                    $template->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $template->is_active = 1;
                    $template->updated_by = auth()->user()->id;
                    $template->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/email/template');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/email/template');
            }
        }

        if (Gate::denies('statusPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/template');
        }
    }
}
