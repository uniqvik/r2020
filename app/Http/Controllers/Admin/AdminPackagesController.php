<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use App\Product;
use App\Inclusion;
use App\InclusionProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminPackagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'package'))
        {
            $products = Product::query();
            $products = $products->where('type', 'subscription');
            $products = $products->where('role_id', '7');
            $products = $products->get();
            
            if($request->package)
            {
                $productId = $request->package;
            }else{$productId = $products[0]->id;}

            
            foreach($products as $product)
            {
                $productIds[] = $product->id;               
            }

            $getInclusions = Product::with('inclusions')
            ->where('id', $productId)
            ->orderBy('id')
            ->get();
            $inclusions = $getInclusions[0];
            
            return view('admin.package.agent',compact('products','inclusions'));
        }
        if (Gate::denies('accessPermission', 'package')) {
            return redirect('admin/errors/404');
        }
    }

    public function companyPackage(Request $request)
    {
        if(Gate::allows('accessPermission', 'package'))
        {
            $products = Product::query();
            $products = $products->where('type', 'subscription');
            $products = $products->where('role_id', '6');
            $products = $products->get();
            
            if($request->package)
            {
                $productId = $request->package;
            }else{$productId = $products[0]->id;}

            
            foreach($products as $product)
            {
                $productIds[] = $product->id;               
            }

            $getInclusions = Product::with('inclusions')
            ->where('id', $productId)
            ->orderBy('id')
            ->get();
            $inclusions = $getInclusions[0];
            
            return view('admin.package.company',compact('products','inclusions'));
        }
        if (Gate::denies('accessPermission', 'package')) {
            return redirect('admin/errors/404');
        }
    }

    public function developerPackage(Request $request)
    {
        if(Gate::allows('accessPermission', 'package'))
        {
            $products = Product::query();
            $products = $products->where('type', 'subscription');
            $products = $products->where('role_id', '8');
            $products = $products->get();
            
            if($request->package)
            {
                $productId = $request->package;
            }else{$productId = $products[0]->id;}

            
            foreach($products as $product)
            {
                $productIds[] = $product->id;               
            }

            $getInclusions = Product::with('inclusions')
            ->where('id', $productId)
            ->orderBy('id')
            ->get();
            $inclusions = $getInclusions[0];
            
            return view('admin.package.developer',compact('products','inclusions'));
        }
        if (Gate::denies('accessPermission', 'package')) {
            return redirect('admin/errors/404');
        }
    }

    public function corporateAffiliatePackage(Request $request)
    {
        if(Gate::allows('accessPermission', 'package'))
        {
            $products = Product::query();
            $products = $products->where('type', 'subscription');
            $products = $products->where('role_id', '9');
            $products = $products->get();
            
            if($request->package)
            {
                $productId = $request->package;
            }else{$productId = $products[0]->id;}

            
            foreach($products as $product)
            {
                $productIds[] = $product->id;               
            }

            $getInclusions = Product::with('inclusions')
            ->where('id', $productId)
            ->orderBy('id')
            ->get();
            $inclusions = $getInclusions[0];
            
            return view('admin.package.corporateaffiliate',compact('products','inclusions'));
        }
        if (Gate::denies('accessPermission', 'package')) {
            return redirect('admin/errors/404');
        }
    }

    public function updatePackage(Request $request)
    {
        foreach($request->value as $key=>$val)
        {
            $inclusionproduct = InclusionProduct::find($key);
            if($inclusionproduct){
                $update['value'] = $val;
                $update['order'] = $request->order[$key];
                $inclusionproduct->update($update);
            }
        }
        Session::flash('success_msg','Package updated! ');
        return redirect('/admin/'.$request->redirect);
    }
}
