<?php

namespace App\Http\Controllers\Admin;

use App\Campaign;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminCampaignsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if(Gate::allows('accessPermission', 'email')) {
            $pageLimit = 20;

            if ($request) {
                $title = $request->title;
                $description = $request->description;
                $status = $request->status;
                $campaigns = Campaign::query();
                if($title){
                    $campaigns =  $campaigns->where('title', 'like','%'.$title.'%');
                }
                if($description){
                    $campaigns =  $campaigns->where('description', 'like','%'.$description.'%');
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $campaigns =  $campaigns->where('is_active',$status);
                }
                $campaigns = $campaigns->paginate($pageLimit)->appends(request()->query());
            } else {
                $campaigns = Campaign::paginate($pageLimit);
            }

            return view('admin.emails.campaign.index', compact('campaigns'));
        }
        if (Gate::denies('accessPermission', 'email')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Gate::allows('addPermission', 'email')) {
            $campaign = new Campaign;
            return view('admin.emails.campaign.form',compact('campaign'));
        }
        if (Gate::denies('addPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $input['created_by'] = auth()->user()->id;
        $input['updated_by'] = auth()->user()->id;
        Campaign::create($input);
        Session::flash('success_msg', 'Campaign created!');
        return redirect('admin/email/campaign');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'email')) {
            $campaign = Campaign::find($id);
            if ($campaign) {
                return view('admin.emails.campaign.view',compact('campaign'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'email')) {
            $campaign = Campaign::find($id);
            if ($campaign) {
                return view('admin.emails.campaign.form',compact('campaign'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campaign = Campaign::find($id);
        if($campaign) {
            $update = $request->all();
            $update['updated_by'] = auth()->user()->id ;
            $campaign->update($update);
            Session::flash('success_msg','Campaign updated! ');
        }
        else{Session::flash('fail_msg','Campaign update failed! ');}
        return redirect('admin/email/campaign');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'email')) {
            $campaign = Campaign::find($id);
            if ($campaign) {
                if ($campaign->emailTemps->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! Email tempates available for this campaign.');
                } else {
                    $campaign->delete();
                    Session::flash('success_msg', 'Campaign deleted! ');
                }
            } else {
                Session::flash('fail_msg', 'Campaign not available! ');
            }
            return redirect('admin/email/campaign');
        }
        if (Gate::denies('deletePermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'email')) {
            $campaign = Campaign::find($id);
            if ($campaign) {
                if ($campaign->is_active) {
                    $campaign->is_active = 0;
                    $campaign->updated_by = auth()->user()->id;
                    $campaign->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $campaign->is_active = 1;
                    $campaign->updated_by = auth()->user()->id;
                    $campaign->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/email/campaign');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/email/campaign');
            }
        }

        if (Gate::denies('statusPermission', 'email')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }
}
