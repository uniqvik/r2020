<?php

namespace App\Http\Controllers\Admin;

use App\PropertyFeildList;
use App\PropertyFeild;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminPropertyFeildList extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'propertyfeildlist')) {
            $pageLimit = 20;
            $property_feilds = PropertyFeild::all();
            if ($request) {
                $title = $request->title;
                $property_feild = $request->property_feild;
                $status = $request->status;
                $results = PropertyFeildList::query();
                if($title){
                    $results =  $results->where('title', 'like','%'.$title.'%');
                }
                if($property_feild){
                    $results =  $results->where('property_feild_id',$property_feild);
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $results =  $results->where('is_active',$status);
                }
                $results = $results->paginate($pageLimit)->appends(request()->query());
            } else {
                $results = PropertyFeildList::paginate($pageLimit);
            }

            return view('admin.propertyfeild.propertyfeildlist.index', compact('results','property_feilds'));
        }
        if (Gate::denies('accessPermission', 'PropertyFeildList')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('addPermission', 'propertyfeildlist')) {
            $property_feilds = PropertyFeild::all();
            $propertyfeildlist  = new PropertyFeildList;
            return view('admin.propertyfeild.propertyfeildlist.form',compact('property_feilds','propertyfeildlist'));
        }
        if (Gate::denies('addPermission', 'propertyfeildlist')) {
            Session::flash('fail_msg', 'No permission! Contact administrator');
            return redirect('admin/PropertyFeildList');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        PropertyFeildList::create($input);
        Session::flash('success_msg', 'Property Feild Option created!');
        return redirect('admin/propertyfeildlist');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'propertyfeildlist')) {
            $category = PropertyFeildList::find($id);
            if ($category) {
                return view('admin.propertyfeild.propertyfeildlist.view',compact('category'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'propertyfeildlist')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('viewPermission', 'propertyfeildlist')) {
            $propertyfeildlist = PropertyFeildList::find($id);
            $property_feilds = PropertyFeild::all();
            if ($propertyfeildlist) {
                return view('admin.propertyfeild.propertyfeildlist.form',compact('propertyfeildlist','property_feilds'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'propertyfeildlist')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $PropertyFeildList = PropertyFeildList::find($id);
        if($PropertyFeildList) {
            $update = $request->all();
            $PropertyFeildList->update($update);
            Session::flash('success_msg','Property Feild updated! ');
        }
        else{Session::flash('fail_msg','Property Feild update failed! ');}
        return redirect('admin/propertyfeildlist');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'propertyfeildlist')) {
            $PropertyFeildList = PropertyFeildList::find($id);
            if ($PropertyFeildList) {

                    $PropertyFeildList->delete();
                    Session::flash('success_msg', 'Property Feild option deleted! ');

            } else {
                Session::flash('fail_msg', 'Property Feild option not available! ');
            }
            return redirect('admin/propertyfeildlist');
        }
        if (Gate::denies('deletePermission', 'propertyfeildlist')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/propertyfeildlist');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'propertyfeildlist')) {
            $PropertyFeildList = PropertyFeildList::find($id);
            if ($PropertyFeildList) {
                if ($PropertyFeildList->is_active) {
                    $PropertyFeildList->is_active = 0;
                    $PropertyFeildList->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $PropertyFeildList->is_active = 1;
                    $PropertyFeildList->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/propertyfeildlist');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/propertyfeildlist');
            }
        }

        if (Gate::denies('statusPermission', 'propertyfeildlist')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/propertyfeildlist');
        }
    }
}
