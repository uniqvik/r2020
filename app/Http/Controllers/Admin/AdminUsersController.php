<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\UserDetail;
use App\Role;
use App\Association;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\StoreAdminuser;
use App\Http\Requests\UpdateAdminUser;
use Illuminate\Support\Str;
class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //

        if(Gate::allows('accessPermission', 'user')) {
            $users = User::all(); //print_r($users);
            $allRoles = $this->allRoles();
            $userGroups = $this->userGroups();
            $pageLimit = 20;
            if ($request){
                $name = $request->name;
                $country = $request->country;
                $zone = $request->zone;
                $email = $request->email;
                $status = $request->status;
                $role = $request->role;
                $associations = $request->associations;
                $users = User::query();
                if($country){
                    $users =  $users->where('country',$country);
                }
                if($zone){
                    $users =  $users->where('zone',$zone);
                }
                if($name){
                    $users =  $users->where('contact_name', 'like','%'.$name.'%');
                }
                if($email){
                    $users =  $users->where('email', 'like','%'.$email.'%');
                }
                if($role){
                    $users =  $users->where('role_id',$role);
                }
                if($associations){
                    $users =  $users->where('group_flag',$associations);
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $users =  $users->where('is_active',$status);
                }
                $users = $users->paginate($pageLimit)->appends(request()->query());
            }else{
                $users = User::paginate($pageLimit);
            }
            return view('admin.users.index',compact('users','allRoles','userGroups'));
        }
        if (Gate::denies('accessPermission', 'user')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('addPermission', 'user')) {
            $allRoles = $this->allRoles();
            $associations = Association::all();
            $user = new user;
            return view('admin.users.form',compact('allRoles','user','associations'));
        }
        if (Gate::denies('addPermission', 'user')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/users');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminuser $request)
    {
        $validated = $request->validated();
        if($request->license_expiry){$license_expiry = date('Y-m-d',strtotime($request->license_expiry));}else{$license_expiry=NULL;}
        $parent_id = 0;
        if(isset($request->parent_id) && $request->parent_id > 0){$parent_id = $request->parent_id;}
        $id = User::insertGetId([
            'email'=> $request->email,
            'password'=>bcrypt($request->password),
            'contact_name'=>$request->contact_name,
            'company_name'=>$request->company_name,
            'license_number'=>$request->license_number,
            'license_expiry'=>$license_expiry,
            'email_alt'=>$request->email_alt,
            'telephone'=>$request->telephone,
            'username'=>$request->username,
            'role_id'=>$request->role_id,
            'parent_id'=>$parent_id,
            'gender'=>$request->gender,
            'occupation'=>$request->occupation,
            'language'=>$request->language,
            'specialities'=>$request->specialities,
            'group_flag'=>$request->group_flag,
            'website'=>$request->website,
            'mobile'=>$request->mobile,
            'country'=>$request->country,
            'zone'=>$request->zone,
            'city'=>$request->city,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
            'realopedia_created_at'=>date('Y-m-d H:i:s'),
        ]);
       $userDetail =  UserDetail::insertGetId(['user_id' => $id,'address'=>$request->address,'about_me'=>$request->about_me,'address_alt'=>$request->address_alt]);

        if ($request->hasFile('license_upload')) {
                $image = $request->file('license_upload');
                $name = Str::slug($request->license_number).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/user/'.$id.'/');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $license_upload = '/images/user/'.$id.'/'.$name;
                 UserDetail::where('id',$userDetail)
                            ->update(['license_upload'=>$license_upload]);
        }
        if ($request->hasFile('user_image')) {
                $image = $request->file('user_image');
                $name = Str::slug($request->contact_name).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/user/'.$id.'/');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $user_image = '/images/user/'.$id.'/'.$name;
                User::where('id',$userDetail)
                            ->update(['user_image'=>$user_image]);
        }
        if ($request->hasFile('userdetail_banner')) {
                $image = $request->file('userdetail_banner');
                $name = Str::slug($request->contact_name).time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/user/'.$id.'/');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $userdetail_banner = '/images/user/'.$id.'/'.$name;
                UserDetail::where('id',$userDetail)
                            ->update(['userdetail_banner'=>$userdetail_banner]);
        }

        Session::flash('success_msg', 'User created !');
        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'user')) {
            $user = User::find($id);
            if ($user) {
                $roles = Role::all();
                $associations = Association::all();
                return view('admin.users.view',compact('roles','user','associations'));
            } else {
                return redirect('admin/errors/404');
            }


        }
        if (Gate::denies('viewPermission', 'user')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('editPermission', 'user')) {
            $user = User::find($id);
            if ($user) {
                $allRoles = $this->allRoles();
                $associations = Association::all();
                return view('admin.users.form',compact('allRoles','user','associations'));
            } else {
                return redirect('admin/errors/404');
            }


        }
        if (Gate::denies('editPermission', 'user')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/users');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminUser $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            if($request->license_expiry){$license_expiry = date('Y-m-d',strtotime($request->license_expiry));}else{$license_expiry=NULL;}
            $parent_id = 0;
            if(isset($request->parent_id) && $request->parent_id > 0){$parent_id = $request->parent_id;}

            $update['contact_name'] =$request->contact_name;
            $update['company_name'] =$request->company_name;
            $update['license_number'] =$request->license_number;
            $update['license_expiry'] =$license_expiry;
            $update['email_alt'] =$request->email_alt;
            $update['telephone'] =$request->telephone;
            $update['role_id'] =$request->role_id;
            $update['parent_id'] =$parent_id;
            $update['gender'] =$request->gender;
            $update['occupation'] =$request->occupation;
            $update['language'] =$request->language;
            $update['specialities'] =$request->specialities;
            $update['group_flag'] =$request->group_flag;
            $update['website'] =$request->website;
            $update['mobile'] =$request->mobile;
            $update['country'] =$request->country;
            $update['zone'] =$request->zone;
            $update['city'] =$request->city;


            if($request->password){
                $update['password'] =bcrypt($request->password);
            }
            if ($request->hasFile('user_image')) {
                $image = $request->file('user_image');
                $name = Str::slug($request->contact_name).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/user/'.$id.'/');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $user_image = '/images/user/'.$id.'/'.$name;
                $update['user_image'] =$user_image;
            }
            //$update['updated_at'] = date('Y-m-d H:i:s');
            $user->update($update);

            $userdetail = UserDetail::query();
            $userdetail->where('user_id',$id);
            if($userdetail)
            {
                $updatedetail['address'] = $request->address;
                $updatedetail['about_me'] = $request->about_me;
                $updatedetail['address_alt'] = $request->address_alt;
                if ($request->hasFile('license_upload')) {
                    $image = $request->file('license_upload');
                    $name = Str::slug($request->license_number).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/images/user/'.$id.'/');
                    $imagePath = $destinationPath. "/".  $name;
                    $image->move($destinationPath, $name);
                    $license_upload = '/images/user/'.$id.'/'.$name;
                    $updatedetail['license_upload'] = $license_upload;
                }
                if ($request->hasFile('userdetail_banner')) {
                    $image = $request->file('userdetail_banner');
                    $name = Str::slug($request->contact_name).time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/images/user/'.$id.'/');
                    $imagePath = $destinationPath. "/".  $name;
                    $image->move($destinationPath, $name);
                    $userdetail_banner = '/images/user/'.$id.'/'.$name;
                    $updatedetail['userdetail_banner'] = $userdetail_banner;
                }
                $userdetail->update($updatedetail);
            }
            Session::flash('success_msg', 'User updated! ');
        } else {
            Session::flash('fail_msg', 'User update failed! ');
        }
        return redirect('/admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'user')) {
            $user = User::find($id);
            if ($user) {

                $user->delete();
                Session::flash('success_msg', 'User deleted!');

            } else {
                Session::flash('fail_msg', 'User not available! ');
            }
            return redirect('/admin/users');
        }
        if (Gate::denies('deletePermission', 'user')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/users');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'user')) {
            $user = User::find($id);
            if ($user) {
                if ($user->is_active) {
                    $user->is_active = 0;
                    $user->save();
                    Session::flash('success_msg', 'User updated! ');
                } else {
                    $user->is_active = 1;
                    $user->save();
                    Session::flash('success_msg', 'User updated! ');
                }
                return redirect('/admin/users');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ' );
                return redirect('/admin/users');
            }
        }
        if (Gate::denies('statusPermission', 'user')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/users');
        }
    }

    public function checkParentRole(Request $request){
        $role = Role::find($request->id);
        if($role && $role->parent_role > 0){
            $users = User::where('role_id',$role->parent_role)
                    ->where('parent_id','0')
                    ->where('is_active','1');
            return $users->get();
        }
    }
}
