<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Zone;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;

class AdminZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if (Gate::allows('accessPermission', 'location')) {
            $countries = Country::get();
            $pageLimit = 20;
            if ($request) {
                $name = $request->name;
                $country = $request->country;
                $status = $request->status;
                $zones = Zone::query();
                if($country){
                    $zones =  $zones->where('country',$country);
                }
                if($name){
                    $zones =  $zones->where('name', 'like','%'.$name.'%');
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $zones =  $zones->where('is_active',$status);
                }
                $zones = $zones->paginate($pageLimit)->appends(request()->query());
            } else {
                $zones = Zone::paginate($pageLimit);
            }
            return view('admin.location.zone.index', compact('zones','countries'));
        }
        if (Gate::denies('accessPermission', 'location')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for `cre`ating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //
        if (Gate::allows('addPermission', 'location')) {
            $countries = Country::get();
            $zone = new Zone;
            return view('admin.location.zone.form',compact('countries','zone'));
        }
        if (Gate::denies('addPermission', 'location')) {
            return view('admin.location.zone.form');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $todayDate = date('Y-m-d H:i:s');
        $id = Zone::insertGetId(['name'=>$request->name,'code'=>$request->code,'country'=>$request->country,'latitude'=>$request->latitude,'longitude'=>$request->longitude,
            'description'=>$request->description,'created_at'=>$todayDate,'updated_at'=>$todayDate,'created_by' => auth()->user()->id, 'updated_by' => auth()->user()->id]);
        if ($request->hasFile('image')) {
            $this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
            $image = $request->file('image');
            $name = str_slug($request->name).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/location/zone');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $zone = Zone::find($id);
            $zone->image = '/images/location/zone/'.$name;
            $zone->save();
        }
        Session::flash('success_msg', 'Zone created ! ');
        return redirect('/admin/location/zone');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'location')) {
            $countries = Country::get();
            $zone = Zone::find($id);
            if ($zone) {
                return view('admin.location.zone.view', compact('zone','countries'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/zone');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('editPermission', 'location')) {
            $countries = Country::get();
            $zone = Zone::find($id);
            if ($zone) {
                return view('admin.location.zone.form', compact('zone','countries'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('editPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/zone');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $zone = Zone::find($id);
        if ($zone) {
            $zone->update($request->all());
            if ($request->hasFile('image')) {
                //$this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
                echo $image = $request->file('image');exit;
                $name = str_slug($request->name).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/location/zone');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $zone->image = '/images/location/zone/'.$name;
                $zone->save();
            }
            Session::flash('success_msg', 'Zone updated! ');
        } else {
            Session::flash('fail_msg', 'Zone update failed! ');
        }
        return redirect('/admin/location/zone');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'location')) {
            $zone = Zone::find($id);
            if ($zone) {
                if ($zone->users->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! User Accounts available for this zone.');
                } else {
                    $zone->delete();
                    Session::flash('success_msg', 'Zone deleted!');
                }
            } else {
                Session::flash('fail_msg', 'Zone not available! ');
            }
            return redirect('/admin/location/zone');
        }
        if (Gate::denies('deletePermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/zone');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'location')) {
            $zone = Zone::find($id);
            if ($zone) {
                if ($zone->is_active) {
                    $zone->is_active = 0;
                    $zone->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $zone->is_active = 1;
                    $zone->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/location/zone');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ' );
                return redirect('/admin/location/zone');
            }
        }
        if (Gate::denies('statusPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/zone');
        }


    }

    public function getZonesbyId(Request $request)
    {

        if($request->id)
        {
            $zones = Zone::query();

            $zones =  $zones->where('country',$request->id);

            return $zones->get();
        }


    }
}
