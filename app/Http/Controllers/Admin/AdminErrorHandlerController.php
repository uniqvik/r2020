<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminErrorHandlerController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function errorCode404()
    {
        return view('admin.errors.404');
    }


    public function errorCode405()
    {
        return view('admin.errors.405');
    }
}
