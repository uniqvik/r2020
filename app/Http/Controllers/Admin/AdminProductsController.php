<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductTimePrice;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AdminProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        //
        if(Gate::allows('accessPermission', 'product')) {
            $pageLimit = 20;
            $qryVal = $request->name;
            if ($qryVal) {
                $products = Product::where('productName', 'like', $qryVal)->paginate($pageLimit);
            } else {
                $products = Product::paginate($pageLimit);
            }
            return view('admin.products.index', compact('products'));
        }
        if (Gate::denies('accessPermission', 'product')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Gate::allows('addPermission', 'product')) {
            $publicroles = $this->publicroles();
            $timeperiods = $this->timePeriods();
            $status = $this->status();
            $productTimePrice = '';
            return view('admin.products.form',compact(['publicroles','timeperiods','status','productTimePrice']));
        }
        if (Gate::denies('addPermission', 'product')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/products');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //save to products table
        $todayDate = date('Y-m-d H:i:s');
        $id = Product::insertGetId(['productName'=>$request->productName,'type'=>$request->type,'role_id'=>$request->role_id,'slug'=>$request->slug,
                                    'created_at'=>$todayDate,'updated_at'=>$todayDate,'created_by' => auth()->user()->id, 'updated_by' => auth()->user()->id]);

        //timeperiod - prices saved in product_time_prices table
        $TPcount = count($request->timePeriod);
        for($count = 0; $count < $TPcount ; $count++){ // for multiple time options for products
            $timeArr = array(); //reset array
            if($request->timePeriod[$count] != 0) {
                $timeArr['timePeriod'] = $request->timePeriod[$count]; //timeprice fields
                $timeArr['product_id'] = $id; // include product id
                $timeArr['price'] = $request->price[$count];
                $timeArr['currency'] = $request->currency[$count];
                $timeArr['frequency'] = $request->frequency[$count];
                $timeArr['is_active'] = $request->status[$count];
                $timeArr['created_at'] = date('Y-m-d H:i:s');
                $timeArr['updated_at'] = date('Y-m-d H:i:s');
                ProductTimePrice::insert($timeArr);
            }
        }
        Session::flash('success_msg', 'Product updated! ');
        return redirect('/admin/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Gate::allows('editPermission', 'product')) {
            $publicroles = $this->publicroles();
            $timeperiods = $this->timePeriods();
            $status = $this->status();
            $product = Product::find($id);
            $productTimePrice = ProductTimePrice::where('product_id', '=', $id)->get();
            return view('admin.products.view',compact(['publicroles','timeperiods','status','product','productTimePrice']));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'product')) {
            $publicroles = $this->publicroles();
            $timeperiods = $this->timePeriods();
            $status = $this->status();
            $product = Product::find($id);
            $productTimePrice = ProductTimePrice::where('product_id', '=', $id)->get();
            return view('admin.products.form',compact(['publicroles','timeperiods','status','product','productTimePrice']));
        }
        if (Gate::denies('editPermission', 'product')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/products');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //save to products table
        $product = Product::find($id);
        if($product) {
            $update = $request->all();
            $update['updated_by'] = auth()->user()->id;
            $product->update($update);
            //timeperiod - prices saved in product_time_prices table
            $productTimePrice = ProductTimePrice::where('product_id', '=', $id);
            $TPcount = count($request->timePeriod);
            $productTimePrice->delete();
            for($count = 0; $count < $TPcount ; $count++){ // for multiple time options for products
                $timeArr = array(); //reset array
                if($request->timePeriod[$count] != 0) {
                    $timeArr['timePeriod'] = $request->timePeriod[$count]; //timeprice fields
                    $timeArr['product_id'] = $id; // include product id
                    $timeArr['price'] = $request->price[$count];
                    $timeArr['currency'] = $request->currency[$count];
                    $timeArr['frequency'] = $request->frequency[$count];
                    $timeArr['is_active'] = $request->status[$count];
                    $timeArr['created_at'] = date('Y-m-d H:i:s');
                    $timeArr['updated_at'] = date('Y-m-d H:i:s');
                    $productTimePrice->insert($timeArr); // update 1st timeprice value
                }
            }
            Session::flash('success_msg', 'Product updated! ');
            return redirect('/admin/products');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'product')) {
            $product = Product::find($id);
            if($product){
                $product->delete();
                Session::flash('success_msg','Product deleted! ');

            }
            else{ Session::flash('fail_msg','Product not available! ');}
            return redirect('/admin/products');
        }
        if (Gate::denies('deletePermission', 'product')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/products');
        }

    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'product')) {
            $product = Product::find($id);
            if ($product) {
                if ($product->is_active) {
                    $product->is_active = 0;
                    $product->updated_by = auth()->user()->id;
                    $product->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $product->is_active = 1;
                    $product->updated_by = auth()->user()->id;
                    $product->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('/admin/products');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('/admin/products');
            }
        }
        if (Gate::denies('statusPermission', 'product')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/products');
        }
    }

    public function timePricesDelete($id){
        $productTimePrice = ProductTimePrice::find($id);
        if($productTimePrice) {
            $productTimePrice->delete();
            return redirect()->back();
        }
    }
}
