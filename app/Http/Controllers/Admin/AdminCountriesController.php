<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Country;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminCountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');


    }

    public function index(Request $request)
    {
        //
        if(Gate::allows('accessPermission', 'location')) {
        $pageLimit = 20;

        if($request) {
            $name = $request->name;
            $status = $request->status;
            $sortBy = ($request->sort ? $request->sort : 'id');
            $order = ($request->order ? $request->order : 'asc');
            $countries = Country::query();
            if($name){
                $countries =  $countries->where('name', 'like','%'.$name.'%');
            }
            if($status){
                if($status == 2){$status = 0;}
                $countries =  $countries->where('is_active',$status);
            }
            $countries = $countries->orderBy($sortBy,$order);
            $countries = $countries->paginate($pageLimit)->appends(request()->query());
        }
        else{$countries = Country::paginate($pageLimit);}
        if($order == 'desc'){$order = 'asc';}else{$order = 'desc';}
        return view('admin.location.country.index',compact('countries','order'));
        }
        if (Gate::denies('accessPermission', 'location')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Gate::allows('addPermission', 'location')) {
            $country = new Country;

        return view('admin.location.country.form',compact('country'));
        }
        if (Gate::denies('addPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/country');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $todayDate = date('Y-m-d H:i:s');
        $id = Country::insertGetId(['name'=>$request->name,'iso_code_2'=>$request->iso_code_2,'iso_code_3'=>$request->iso_code_3,'currency_code'=>$request->currency_code,
            'latitude'=>$request->latitude,'longitude'=>$request->longitude,
            'description'=>$request->description,'created_at'=>$todayDate,'updated_at'=>$todayDate,'created_by' => auth()->user()->id, 'updated_by' => auth()->user()->id]);
        if ($request->hasFile('image')) {
            $this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
            $image = $request->file('image');
            $name = str_slug($request->name).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/location/country');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $country = Country::find($id);
            $country->image = '/images/location/country/'.$name;
            $country->save();
        }
       Session::flash('success_msg', 'Country created ! ');
       return redirect('/admin/location/country');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Gate::allows('viewPermission', 'location')) {
            $country = Country::find($id);
            if($country){return view('admin.location.country.view',compact('country'));}
            else {return redirect('admin/errors/404');}
            }
            if (Gate::denies('viewPermission', 'location')) {
                Session::flash('fail_msg', 'No permission! Contact administrator ');
                return redirect('/admin/location/country');
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (Gate::allows('editPermission', 'location')) {
        $country = Country::find($id);
        if($country){return view('admin.location.country.form',compact('country'));}
        else {return redirect('admin/errors/404');}
        }
        if (Gate::denies('editPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/country');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $country = Country::find($id);
        if($country) {
            $update = $request->all();
            $update['updated_by'] = auth()->user()->id ;
            $country->update($update);
            if ($request->hasFile('image')) {
                //$this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
                $image = $request->file('image');
                $name = str_slug($request->name).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/location/country');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $country->image = '/images/location/country/'.$name;
                $country->save();
            }
            Session::flash('success_msg','Country updated! ');
        }
        else{Session::flash('fail_msg','Country update failed! ');}
        return redirect('/admin/location/country');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gate::allows('deletePermission', 'location')) {
        $country = Country::find($id);
        if($country){
            if($country->users->isNotEmpty()){
                Session::flash('fail_msg','Delete Failed! User Accounts available for this country.');
            }
            else {
                $country->delete();
                Session::flash('success_msg','Country deleted!');
            }
        }
        else{ Session::flash('fail_msg','Country not available! ');}
        return redirect('/admin/location/country');
        }
        if (Gate::denies('deletePermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/country');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'location')) {
        $country = Country::find($id);
        if($country){
            if($country->is_active){$country->is_active = 0;
                $country->updated_by = auth()->user()->id;
                $country->save();
                Session::flash('success_msg','Status updated! ');
            }
            else{$country->is_active = 1;
                $country->updated_by = auth()->user()->id;
                $country->save();
            Session::flash('success_msg','Status updated! ');}
            return redirect('/admin/location/country');
        }
        else {
            Session::flash('fail_msg','Status update failed ! ');
            return redirect('/admin/location/country');
        }
        }

        if (Gate::denies('statusPermission', 'location')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('/admin/location/country');
        }
    }

    public function getCountries()
    {
        return Country::all();
    }

}
