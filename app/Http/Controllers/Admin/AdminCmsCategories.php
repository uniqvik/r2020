<?php

namespace App\Http\Controllers\Admin;

use App\CmsCategories;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminCmsCategories extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if(Gate::allows('accessPermission', 'cms_category')) {
            $pageLimit = 20;
             
            if ($request) {
                $title = $request->title;
                $status = $request->status;
                $categories = CmsCategories::query();
                if($title){
                    $categories =  $categories->where('title', 'like','%'.$title.'%');
                }
                if($status){
                    if($status == 2){$status = 0;}
                    $categories =  $categories->where('is_active',$status);
                }
                $categories = $categories->paginate($pageLimit)->appends(request()->query());
            } else {
                $categories = CmsCategories::paginate($pageLimit);
            }
           
            return view('admin.cms.category.index', compact('categories'));
        }
        if (Gate::denies('accessPermission', 'cms_category')) {
            return redirect('admin/errors/404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('addPermission', 'cms_category')) {
            return view('admin.cms.category.form');
        }
        if (Gate::denies('addPermission', 'cms_category')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        if ($request->hasFile('image')) {
            $this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
            $image = $request->file('image');
            $name = str_slug($request->title).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/cms');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $input['image'] = '/images/cms/'.$name;
        }
        $input['created_by'] = auth()->user()->id;
        $input['updated_by'] = auth()->user()->id;
        CmsCategories::create($input);
        Session::flash('success_msg', 'Category created!');
        return redirect('admin/cmscategories');
    }
    public function storeCategoryFromAjax(Request $request)
    {   
        if($request->title){
            $time = date('Y-m-d H:i:s');
            $input['title'] = $request->title;
            $input['created_at'] = $time;
            $input['updated_at'] = $time;
            $input['created_by'] = auth()->user()->id;
            $input['updated_by'] = auth()->user()->id;
            $add = CmsCategories::insertGetId($input);
            $response = array(
                'status' => 'success',
                'title' => $request->title,
                'id' => $add,
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        return response()->json($response); 
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('viewPermission', 'cms_category')) {
            $category = CmsCategories::find($id);
            if ($category) {
                return view('admin.cms.category.view',compact('category'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'cms_category')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('viewPermission', 'cms_category')) {
            $category = CmsCategories::find($id);
            if ($category) {
                return view('admin.cms.category.form',compact('category'));
            } else {
                return redirect('admin/errors/404');
            }
        }
        if (Gate::denies('viewPermission', 'cms_category')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/email/campaign');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = CmsCategories::find($id);
        if($category) {
            $update = $request->all();
            if ($request->hasFile('image')) {
                $this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif|max:2048']);
                $image = $request->file('image');
                $name = str_slug($request->title).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/cms');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $update['image'] = '/images/cms/'.$name;
            }
            $update['updated_by'] = auth()->user()->id ;
            $category->update($update);
            Session::flash('success_msg','Category updated! ');
        }
        else{Session::flash('fail_msg','Category update failed! ');}
        return redirect('admin/cmscategories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('deletePermission', 'cms_category')) {
            $category = CmsCategories::find($id);
            if ($category) {
                if ($category->cmsItems->isNotEmpty()) {
                    Session::flash('fail_msg', 'Delete Failed! CMS available for this category.');
                } else { 
                    $category->delete();
                    Session::flash('success_msg', 'Campaign deleted! ');
                }
            } else {
                Session::flash('fail_msg', 'Campaign not available! ');
            }
            return redirect('admin/cmscategories');
        }
        if (Gate::denies('deletePermission', 'cms_category')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/cmscategories');
        }
    }

    public function statusChange($id)
    {
        if (Gate::allows('statusPermission', 'cms_category')) {
            $category = CmsCategories::find($id);
            if ($category) {
                if ($category->is_active) {
                    $category->is_active = 0;
                    $category->updated_by = auth()->user()->id;
                    $category->save();
                    Session::flash('success_msg', 'Status updated! ');
                } else {
                    $category->is_active = 1;
                    $category->updated_by = auth()->user()->id;
                    $category->save();
                    Session::flash('success_msg', 'Status updated! ');
                }
                return redirect('admin/cmscategories');
            } else {
                Session::flash('fail_msg', 'Status update failed ! ');
                return redirect('admin/cmscategories');
            }
        }

        if (Gate::denies('statusPermission', 'cms_category')) {
            Session::flash('fail_msg', 'No permission! Contact administrator ');
            return redirect('admin/cmscategories');
        }
    }
}
