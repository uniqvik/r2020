<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCommunity extends Model
{
    protected $fillable = ['name','country','community_id','latitude','longitude','description','created_by','updated_by'];

    public function users(){
        return $this->hasMany('App\User','city');
    }

    public function Community(){
        return $this->belongsTo('App\Community', 'community_id');
    }

    public function state(){
        return $this->belongsTo('App\Zone','zone');
    }

    public function countryDet(){
        return $this->belongsTo('App\Country','country');
    }
}
