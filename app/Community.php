<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    protected $fillable = ['name','country','zone_id','latitude','longitude','description','created_by','updated_by'];

    public function users(){
        return $this->hasMany('App\User','city');
    }

    public function state(){
        return $this->belongsTo('App\Zone', 'zone_id');
    }

    public function countryDet(){
        return $this->belongsTo('App\Country','country');
    }
}
