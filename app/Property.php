<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $guarded = ['id'];


    public function Unit() 
    {
        return $this->belongsTo('App\PropertyFeildList', 'unit_type');
    }

    public function UserDet() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function countryDet(){
        return $this->belongsTo('App\Country','country_id');
    }

    public function state(){
        return $this->belongsTo('App\Zone','zone_id');
    }

    public function cityDet(){
        return $this->belongsTo('App\City','city_id');
    }

    public function communityDet(){
        return $this->belongsTo('App\Community','community_id');
    }

    public function subCommunity(){
        return $this->belongsTo('App\SubCommunity','sub_community_id');
    }

    public function PropertyDetail()
    {
        return $this->hasOne('App\PropertyDescription', 'property_id');
    }

    public function PropertyPhoto()
    {
        return $this->hasMany('App\PropertyPhoto', 'property_id');
    }
}
