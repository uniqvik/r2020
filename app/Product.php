<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = ['productName', 'type', 'role_id','slug'];

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id')/*->where('is_active', 1)*/;
    }

    public function inclusions(){
        return $this->belongsToMany('App\Inclusion', 'inclusion_product')->withPivot('value','id','order');
    }
}
