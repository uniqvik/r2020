@extends('layouts.admin')
@section('content')
    <script src="{{ asset('dist/js/angularApp.js') }}"></script>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Products
                <small>Manage Products</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('products.index')}}">Products</a></li>
                <li class="active"><a href="{{route('products.create')}}">Add/Edit</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" ng-app="dynamicFieldsPlugin">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add/Edit Products</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" ng-controller="dynamicFields">
                            @if(isset($product->id)) {{--edit form--}}
                            {!! Form::model($product,['method' => 'PATCH','action' => ['Admin\AdminProductsController@update',$product->id]
                            ,'name' => 'productsForm','id' => 'productsForm']) !!}
                            {{csrf_field()}}
                            @else {{--create form--}}
                            {!!  Form::open(['method' => 'POST','action' => 'Admin\AdminProductsController@store','name' => 'productsForm','id' => 'productsForm']) !!}
                            @endif
                            <div class="form-group">
                                {!! Form::label('productName','Product Name') !!}
                                {!! Form::text('productName',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                   {!! Form::label('type','Type') !!}
                                   {!! Form::select('type',['subscription' => 'Subscription','addons' => 'Addons','banners'=>'Banners'],null, ['class' => 'form-control',]) !!}
                            </div>
                             <div class="form-group">
                                   {!! Form::label('role','Role') !!}
                                   {!! Form::select('role_id',$publicroles,null, ['class' => 'form-control','data-validation' => 'required' ]) !!}
                             </div>
                            <div class="form-group">
                                {!! Form::label('slug','Slug') !!}
                                {!! Form::text('slug',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>

                                 <table   class="table table-striped" role="grid" name="timePeriods" id="timePeriods" width="70%" cellpadding="5" cellspacing="5">
                                     <thead><tr role="row"><th>Time Period</th><th>Price</th><th>Currency Code</th><th>Number/Agents/Count</th><th>Status</th><th>&nbsp;</th></tr></thead>
                                     <tbody>
                                     @if($productTimePrice)
                                     @foreach ($productTimePrice as $key => $tp)  {{--looping for values from db--}}
                                        <tr ng-style="tp_{{$tp->id}}">
                                            <td>{!! Form::select('timePeriod[]',$timeperiods,$tp->timePeriod, ['id'=>"[[arr.id]]",'class' => 'form-control']) !!}</td>
                                            <td>{!! Form::text('price[]',$tp->price,['id'=>"[[arr.id]]",'class'=>'form-control']) !!}</td>
                                            <td>{!! Form::text('currency[]',$tp->currency,['id'=>"[[arr.id]]",'class'=>'form-control']) !!}</td>
                                            <td>{!! Form::text('frequency[]',$tp->frequency,['id'=>"[[arr.id]]",'class'=>'form-control']) !!}</td>
                                            <td>{!! Form::select('status[]',$status,$tp->is_active, ['id'=>"[[arr.id]]",'class' => 'form-control']) !!}</td>
                                            <td></td>
                                            {{--<td><small ng-click="tp_{{$tp->id}}={display:'none'};removeBackChoice('{{route('products.timePricesDelete',$tp->id)}}')"
                                                       class="label pull-right bg-red"  style="cursor: pointer;padding:5px 10px; font-size: 16px">-</small></td>--}}
                                           {{-- <td><small ng-click="tp_{{$tp->id}}={display:'none'};removeBackChoice('{{$key}}',{{$productTimePrice}})"
                                                       class="label pull-right bg-red"  style="cursor: pointer;padding:5px 10px; font-size: 16px">-</small></td>--}}
                                            <td><a href="{{route('products.timePricesDelete',$tp->id)}}"><small
                                                       class="label pull-right bg-red"  style="cursor: pointer;padding:5px 10px; font-size: 16px">-</small></a></td>
                                        </tr>
                                     @endforeach
                                     @endif
                                     <tr data-ng-repeat="(key,choice) in choices"> {{--looping - front end control for new adding--}}
                                         <td data-ng-repeat="arr in choice">
                                             {!! Form::select('timePeriod[]',$timeperiods,null, ['id'=>"[[arr.id]]",'class' => 'form-control','ng-if' => "arr.name == 'timePeriod'"]) !!}
                                             {!! Form::text('price[]',null,['id'=>"[[arr.id]]",'class'=>'form-control','ng-if' => "arr.name == 'price'"]) !!}
                                             {!! Form::text('currency[]','USD',['id'=>"[[arr.id]]",'class'=>'form-control','ng-if' => "arr.name == 'currency'"]) !!}
                                             {!! Form::text('frequency[]',null,['id'=>"[[arr.id]]",'class'=>'form-control','ng-if' => "arr.name == 'frequency'"]) !!}
                                             {!! Form::select('status[]',$status,null, ['id'=>"[[arr.id]]",'class' => 'form-control','ng-if' => "arr.name == 'status'"]) !!}
                                         </td>

                                         <td><small ng-if="$last" ng-show="showAddChoice(choice)" ng-click="addNewChoice()"
                                                    class="label pull-right bg-green" style="cursor: pointer;padding:5px 10px; font-size: 16px">+</small></td>
                                         <td><small ng-click="removeFrontChoice(key)" class="label pull-right bg-red"  style="cursor: pointer;padding:5px 10px; font-size: 16px">-</small></td>
                                         </td>

                                     </tr>

                                     {{--<tr>
                                         <td>{!! Form::select('timePeriod',$timeperiods,null, ['class' => 'form-control']) !!}</td>
                                         <td>{!! Form::text('price',null,['class'=>'form-control']) !!}</td>
                                         <td>{!! Form::text('currency','USD',['class'=>'form-control']) !!}</td>
                                         <td>{!! Form::text('frequency',null,['class'=>'form-control']) !!}</td>
                                         <td>+</td>
                                     </tr>--}}
                                     </tbody>
                                 </table>

                            <div class="form-group">
                                {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                                <a href="{{route('products.index')}}" class="btn btn-default">Cancel</a>
                            </div>
                                {!! Form::close() !!}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
            });
        });

    </script>
@stop