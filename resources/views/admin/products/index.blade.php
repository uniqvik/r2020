@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Products
                <small>Manage Products</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Products</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">

                                {!!  Form::open(['method' => 'GET','action' => 'Admin\AdminProductsController@index' ]) !!}
                                <div class="col-md-3 form-group">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder' => 'Product Name']) !!}
                                </div>
                                <div class="col-md-3 form-group">
                                    {!! Form::text('type',null,['class'=>'form-control','placeholder' => 'Product Type']) !!}
                                </div>
                                <div class="col-md-3 form-group">
                                    {!! Form::text('role',null,['class'=>'form-control','placeholder' => 'Role']) !!}
                                </div>
                                {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                                {!! Form::reset('Reset', ['class' => 'btn btn-default']) !!}
                                {!! Form::close() !!}



                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" onclick="window.location='{{ route("products.create") }}'">+ Product</button>
                                </div>
                            </div>
                            @if(Session::has('success_msg'))
                                <div class="col-md-12">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> {{session('success_msg')}}</h4>
                                </div>
                                </div>
                            @endif
                            @if(Session::has('fail_msg'))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</h4>
                                </div>
                            @endif
                            <table id="associationsTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th >Action</th>
                                    <th>Product Name</th>
                                    <th>Type</th>
                                    <th>Role</th>
                                    <th>Slug</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($products)
                                    @if($products->currentpage() == 1)
                                        @php($ctr = 1)
                                    @else
                                        @php($ctr = ($products->currentpage()-1)*$products->perpage()+1)

                                    @endif
                                    @foreach($products as $product)
                                        <tr>
                                            <td >{{$ctr}}</td>
                                            <td>
                                                <span style="float: left"><a href="{{route('products.edit',$product->id)}}" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i>Edit</a></span>
                                                <span style="float: left"><a href="{{route('products.show',$product->id)}}" data-toggle="tooltip" title="View"><i class="fa fa-fw fa-eye"></i>View</a></span>
                                                <span style="float: left">
                                                <form action="{{route('products.destroy',$product->id) }}" method="post" id="deleteForm{{$product->id}}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {!! csrf_field() !!}
                                                    <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$product->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                        <i class="fa fa-fw  fa-trash"></i>Delete</a>

                                                </form>
                                                </span>
                                                @if($product->is_active)
                                                    <span><a href="{{route('products.statusChange',$product->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                            <span style="color: yellowgreen"><i class="fa fa-fw fa-check"></i></span>Active</a></span>
                                                @else
                                                    <span><a href="{{route('products.statusChange',$product->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                            <span style="color: red"><i class="fa fa-fw fa-times"></i></span>Inactive</a></span>
                                                @endif
                                            </td>
                                            <td>{{$product->productName}} </td>
                                            <td>{{$product->type}} </td>
                                            <td>{{$product->role->title}} </td>
                                            <td>{{$product->slug}} </td>
                                            <td>{{$product->created_at->format('Y-m-d')}}</td>
                                            <td>{{$product->updated_at->format('Y-m-d')}}</td>

                                        </tr>
                                        @php($ctr++)
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th >Action</th>
                                    <th>Product Name</th>
                                    <th>Type</th>
                                    <th>Role</th>
                                    <th>Slug</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </tfoot>
                            </table>

                            {{--pagination--}}

                            @include('admin/partials/pagination',['results'=>$products])
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop