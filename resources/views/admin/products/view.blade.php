@extends('layouts.admin')
@section('content')
    <script src="{{ asset('dist/js/angularApp.js') }}"></script>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Products
                <small>Manage Products</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('products.index')}}">Products</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px">
                    <div class="form-group">
                        <label>Product Name :</label><span>{{$product->productName}}</span>
                    </div>
                    <div class="form-group">
                        <label>Type :</label><span>{{$product->type}}</span>
                    </div>
                    <div class="form-group">
                        <label>Role :</label><span>{{$publicroles[$product->role_id]}}</span>
                    </div>
                    <div class="form-group">
                        <label>Slug :</label><span>{{$product->slug}}</span>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr role="row">
                                <td>Time Period</td>
                                <td>Price</td>
                                <td>Currency Code</td>
                                <td>Number/Agents/Count</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                        @if($productTimePrice)
                                     @foreach ($productTimePrice as $key => $tp)  {{--looping for values from db--}}
                                        <tr ng-style="tp_{{$tp->id}}">
                                            <td>
                                            {{$timeperiods[$tp->timePeriod]}}    
                                            </td>
                                            <td>{{$tp->price}}</td>
                                            <td>{{$tp->currency}}</td>
                                            <td>{{$tp->frequency}}</td>
                                            <td>
                                                {{$status[$tp->is_active]}}       
                                            </td>
                                        </tr>
                                     @endforeach
                                     @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop