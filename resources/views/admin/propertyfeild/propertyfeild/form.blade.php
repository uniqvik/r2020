@extends('layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">   Property Feilds
                        <small>Manage Property Feilds</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('propertyfeild.index')}}">Property Feilds</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if(isset($propertyfeild->id)) {{--edit form--}}
                                <form method="POST"
                                action="{{route('propertyfeild.update',$propertyfeild->id)}}"
                                name="associationsForm"
                                id="associationsForm"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else
                                <form method="POST"
                                            action="{{route('propertyfeild.store')}}"
                                            name="associationsForm"
                                            id="associationsForm"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" value="{{$propertyfeild->title}}" class="form-control" data-validation="required">

                                </div>
                                <div class="form-group">

                                    <label for="name">Property Type</label>
                                    <div class="form-group">
                                        <div class="radio">
                                        @php
                                        if(isset($propertyfeild->id)){$property_type = $propertyfeild->property_type;}else{$property_type = 'no';}
                                        @endphp
                                            <label style="margin-right:5px">
                                                <input type="radio" value="yes" name="property_type" data-validation="required" @if($property_type == 'yes') checked="" @endif>Yes
                                            </label>
                                            <label>
                                                <input type="radio" value="no" name="property_type" data-validation="required" @if($property_type == 'no') checked="" @endif>No
                                            </label>
                                        </div>
                                        </div>
                                </div>


                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('propertyfeild.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
