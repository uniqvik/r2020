@extends('layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">   Property Feild List
                        <small>Manage Property Feild List</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('propertyfeildlist.index')}}">Property Feild List</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if(isset($propertyfeildlist->id)) {{--edit form--}}
                                <form method="POST"
                                action="{{route('propertyfeildlist.update',$propertyfeildlist->id)}}"
                                name="associationsForm"
                                id="associationsForm"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else
                                <form method="POST"
                                            action="{{route('propertyfeildlist.store')}}"
                                            name="associationsForm"
                                            id="associationsForm"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" value="{{$propertyfeildlist->title}}" class="form-control" data-validation="required">

                                </div>
                                <div class="form-group">

                                    <label for="name">Property Feild</label>
                                    @php
                                    if(isset($propertyfeildlist->id)){
                                        $property_feild_id = $propertyfeildlist->property_feild_id; $property_type = $propertyfeildlist->property_type;
                                    }else{$property_feild_id = '';$property_type = '';}
                                    @endphp
                                        <select name="property_feild_id" id="property_feild_id" class="form-control" data-validation="required">
                                                <option value=''>Select</option>
                                            @foreach($property_feilds as $property_feild)
                                               <option value="{{$property_feild->id}}" class="{{$property_feild->property_type}}" @if($property_feild_id == $property_feild->id) selected @endif>{{$property_feild->title}}</option>
                                            @endforeach
                                        </select>
                                </div>

                                {{-- <style>#type{display: none}</style>
                            <div class="form-group " style="padding-left:0" id="type">
                                    <label for="name">Property Type</label>
                                    <div class="form-group">
                                            <div class="radio">
                                                                                    <label style="margin-right:5px">
                                                    <input type="radio" value="Residential" name="property_type" @if($property_type == 'Residential')checked=""@endif>Residential
                                                </label>
                                                <label>
                                                    <input type="radio" value="Commercial" name="property_type" @if($property_type == 'Commercial')checked=""@endif>Commercial
                                                </label>
                                            </div>
                                            </div>
                            </div> --}}
                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('propertyfeildlist.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
