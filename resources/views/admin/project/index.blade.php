@extends('layouts.admin')
@section('content')
    <script> 
        var app = angular.module('location', [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });
        // app.config(['$locationProvider', function($locationProvider){
        //   $locationProvider.html5Mode({
        //     enabled: true,
        //     requireBase: false
        //     })
        //   }])
        app.controller('MainCtrl', function($scope,$http) {
            $scope.loadCountry = function(){
                $http({
                    method: 'GET',
                    url: '{{route('getCountries')}}'
                }).then(function successCallback(response) {
                    $scope.countries = response.data;
                    //$scope.country = $location.search().country;
                }, function errorCallback(response) {

                });
            }
            $scope.loadState = function(){
                $http({
                    method: 'POST',
                    data: { 'id' : $scope.country },
                    url: '{{route('getZonesbyId')}}'
                }).then(function successCallback(response) {
                    $scope.states = response.data;
                    //$scope.state = $location.search().zone;
                }, function errorCallback(response) {

                });
            }

            $scope.loadCommunity = function(){
                $http({
                    method: 'POST',
                    data: { 'id' : $scope.country },
                    url: 'getCommunitybyId'
                }).then(function successCallback(response) {
                    $scope.communities = response.data;
                    //$scope.state = $location.search().zone;
                }, function errorCallback(response) {

                });
            }
        });
    </script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>Manage Users</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Users</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" ng-app="location">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary" >

                        <!-- /.box-header -->

                        <div class="box-body" ng-controller="MainCtrl">
                            {!!  Form::open(['method' => 'GET','action' => 'Admin\AdminUsersController@index' ]) !!}
                            <div class="col-xs-3 form-group" ng-init="loadCountry();loadState()">
                                <select class="form-control" name="country" id="country"  ng-model="country" ng-change="loadState()">
                                    <option value=''>Select Country</option>
                                    <option value="<% country.id %>" data-ng-repeat="country in countries"><% country.name %></option>
                                </select>
                            </div>
                            <div class="col-xs-3 form-group">
                                <select id="state" name="zone" ng-model="state" class="form-control">
                                    <option value=''>Select Zone</option>
                                    <option value="<% state.id %>" data-ng-repeat="state in states"><% state.name %></option>
                                </select>
                            </div>

                            <div class="col-xs-3 form-group">
                                {!! Form::text('name',null,['class'=>'form-control','placeholder' => 'Name']) !!}
                            </div>
                            <div class="col-xs-3 form-group mininise">
                                {!! Form::text('email',null,['class'=>'form-control','placeholder' => 'Email']) !!}
                            </div>
                            <div class="col-xs-3 form-group mininise">
                                <select  name="status" class="form-control">
                                    <option value=''>Status</option>
                                    <option value="1" @if(app('request')->input('status') == 1) selected @endif>Active</option>
                                    <option value="2" @if(app('request')->input('status') == 2) selected @endif>Inactive</option>
                                </select>
                            </div>
                            <div class="col-xs-3 form-group mininise">
                                <select  name="role" class="form-control">
                                    <option value=''>Select Role</option>
                                    @foreach ($allRoles as $key => $val)
                                    <option value="{{$key}}" @if(app('request')->input('role') == $key) selected @endif>{{$val}}</option>    
                                    @endforeach
                                    
                                </select>
                            </div>
                            <div class="col-xs-3 form-group mininise">
                                <select  name="associations" class="form-control">
                                    <option value=''>Select Associations</option>
                                    @foreach ($userGroups as $key => $val)
                                    <option value="{{$key}}" @if(app('request')->input('associations') == $key) selected @endif>{{$val}}</option>    
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-xs-3 form-group">
                                {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                                {!! Form::reset('Reset', ['class' => 'btn btn-default reset-form']) !!}
                                <button type="button" class="btn btn-info dropdown-toggle" id="more_filter"><span class="caret"></span></button>
                                <button type="button" class="btn btn-info dropdown-toggle" id="showless"><span class="caret"></span></button>
                            </div> 

                            {!! Form::close() !!}


                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" onclick="window.location='{{ route("users.create") }}'">+ User</button>
                                </div>
                            </div>
                            @if(Session::has('success_msg'))
                                <div class="col-md-12">
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-check"></i> {{session('success_msg')}}</h4>
                                    </div>
                                </div>
                            @endif
                            @if(Session::has('fail_msg'))
                                <div class="col-md-12">
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</h4>
                                    </div>
                                </div>
                            @endif
                            <table id="rolesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Sl.no</th>
                                    <th>Action</th>
                                    <th>Contact Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Association</th>
                                    <th>Mobile</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                    <th>Realopedia Created Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($users)
                                    @php($ctr = 1)
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$ctr}}</td>
                                            <td>
                                                <span><a href="{{route('users.edit',$user->id)}}" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i>Edit</a></span>
                                                <span><a href="{{route('users.show',$user->id)}}" data-toggle="tooltip" title="View"><i class="fa fa-fw fa-eye"></i>View</a></span>
                                                <span style="float: left">
                                                        <form action="{{route('users.destroy',$user->id) }}" method="post" id="deleteForm{{$user->id}}">
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            {!! csrf_field() !!}
                                                            <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$user->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-fw  fa-trash"></i>Delete</a>
                                                        </form>
                                                </span>

                                                @if($user->is_active)
                                                    <span><a href="{{route('users.statusChange',$user->id)}}" data-toggle="tooltip" title="Disable"><i class="fa fa-fw fa-check"></i>Active</a></span>
                                                @else
                                                    <span><a href="{{route('users.statusChange',$user->id)}}" data-toggle="tooltip" title="Enable"><i class="fa fa-fw fa-times"></i>Inactive</a></span>
                                                @endif
                                            </td>
                                            <td>{{$user->contact_name}} </td>
                                            <td>{{$user->email}} </td>
                                            <td>{{$user->username}} </td>
                                            <td>{{$user->role->title}} </td>
                                            <td>{{$user->association->title}} </td>
                                            <td>{{$user->mobile}} </td>
                                            <td>{{$user->created_at->format('Y-m-d')}}</td>
                                            <td>{{$user->updated_at->format('Y-m-d')}}</td>
                                            <td>{{$user->realopedia_created_at->format('Y-m-d')}}</td>
                                        </tr>
                                        @php($ctr++)
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th style="width:7%">Action</th>
                                    <th>Contact Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Association</th>
                                    <th>Mobile</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                    <th>Realopedia Created Date</th>
                                </tr>
                                </tfoot>
                            </table> 
                            @include('admin/partials/pagination',['results'=>$users])
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

@stop 