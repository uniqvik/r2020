@extends('layouts.admin')
@section('content')
    <script>
        var app = angular.module('location', [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });
        app.controller('MainCtrl', function($scope,$http) {
            $scope.loadCountry = function(){
                $http({
                    method: 'GET',
                    url: '{{url('admin/location/getCountries')}}'
                }).then(function successCallback(response) {
                    $scope.countries = response.data;
                    @if(isset($user->country))
                        $scope.country = '{{$user->country}}';
                    @endif
                }, function errorCallback(response) {
                }); 
            }

            $scope.loadState = function(){

                        @if(isset($user->country))
                var con = '{{$user->country}}';
                @endif
                if($scope.country){con = $scope.country;}
                $http({
                    method: 'POST',
                    data: { 'id' : con },
                    url: '{{url('admin/location/getZonesbyId')}}'
                }).then(function successCallback(response) {
                    $scope.states = response.data;
                    @if(isset($user->zone))
                        $scope.state = '{{$user->zone}}';
                    @endif
                }, function errorCallback(response) {

                });
            }

            $scope.loadCity = function(){

                        @if(isset($user->zone))
                var con = '{{$user->zone}}';
                @endif
                if($scope.states){con = $scope.state;}
                $http({
                    method: 'POST',
                    data: { 'id' : con },
                    url: '{{url('admin/location/getCitiesbyId')}}'
                }).then(function successCallback(response) {
                    $scope.cities = response.data;
                    @if(isset($user->city))
                        $scope.city = '{{$user->city}}';
                    @endif
                }, function errorCallback(response) {

                });
            }
        });
    </script>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>Manage Users</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('users.index')}}">Users</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" ng-app="location">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">

                        </div>
                        <!-- /.box-header -->
                        @php
                            //dd($user);
                        @endphp
                        <div class="box-body viewform" ng-controller="MainCtrl">

                            <div class="form-group">
                                <label for="name">Name :</label>
                                <span>{{$user->contact_name}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">Company :</label>
                                <span>{{$user->company_name}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">License#</label>
                                <span>{{$user->license_number}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">License expiry</label>
                                @if($user->license_expiry)
                                <span>{{date('m/d/Y', strtotime($user->license_expiry))}}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Email :</label>
                                <span>{{$user->email}}</span>

                            </div>
                            <div class="form-group ">
                                <label for="name">Alternate Email</label>
                                <span>{{$user->email_alt}}</span>

                            </div>
                            <div class="form-group">
                                <label for="name">Username :</label>
                                <span>{{$user->username}}</span>

                            </div>

                            <div class="form-group">
                                <label for="name">Role :</label>
                                <span>{{$user->role->title}}</span>

                            </div>
                            @if($user->parent_id > 0)
                            <div class="form-group">
                                <label for="name">Parent Role :</label>
                                <span>{{$user->parentRole->contact_name}}</span>

                            </div>
                            @endif
                            <div class="form-group">
                                <label for="name">Association :</label>
                                <span>{{$user->association->title}}</span>

                            </div>
                            <div class="form-group">
                                <label for="name">Mobile :</label>
                                <span>{{$user->mobile}}</span>

                            </div>
                            <div class="form-group">
                                <label for="name">Telephone :</label>
                                <span>{{$user->telephone}}</span>

                            </div>
                            <div class="form-group">
                                <label for="name">Country :</label>
                                <span>{{$user->countryDet->name}}</span>

                            </div>
                            <div class="form-group">
                                <label for="name">Zone :</label>
                                <span>{{$user->state->name}}</span>

                            </div>
                            @if($user->city)
                                <div class="form-group">
                                    <label for="name">City :</label>
                                    <span>{{$user->cityDet->name}}</span>

                                </div>
                            @endif
                            <div class="form-group">
                                <label for="name">Address :</label>
                                <span>{{$user->userDetail->address}}</span>

                            </div>
                            <div class="form-group">
                                <label for="name">Branch Address :</label>
                                <span>{{$user->userDetail->address_alt}}</span>

                            </div>
                            <div class="form-group">
                                <label for="name">Gender :</label>
                                <span>{{$user->gender}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">Occupation :</label>
                                <span>{{$user->occupation}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">Language :</label>
                                <span>{{$user->language}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">Specialities :</label>
                                <span>{{$user->specialities}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">Website :</label>
                                <span>{{$user->website}}</span>
                            </div>
                            <div class="form-group">
                                <label for="name">Profile :</label>
                                <span>{{$user->userDetail->about_me}}</span>
                            </div>

                            <div class="form-group">
                                <label for="name">License upload :</label>
                                <span> @isset($user->userDetail->license_upload)
                                                     
                                                 
                                    <a  href="{{ url('/') }}{{$user->userDetail->license_upload}}" data-fancybox style="margin:10px 0">
                                        <img src="{{ url('/') }}{{$user->userDetail->license_upload}}" width="100px">
                                    </a>
                                    @endisset</span>
                            </div>

                            <div class="form-group">
                                <label for="name">User image upload :</label>
                                <span>@if($user->user_image)
                                    <a  href="{{ url('/') }}{{$user->user_image}}" data-fancybox style="margin:10px 0">
                                        <img src="{{ url('/') }}{{$user->user_image}}" width="100px">
                                    </a>
                                    @endif</span>
                            </div>
                            <div class="form-group">
                                <label for="name">User Banner upload :</label>
                                <span>@isset($user->userDetail->userdetail_banner)
                                    <a  href="{{ url('/') }}{{$user->userDetail->userdetail_banner}}" data-fancybox style="margin:10px 0">
                                        <img src="{{ url('/') }}{{$user->userDetail->userdetail_banner}}" width="100px">
                                    </a>
                                    @endisset</span>
                            </div>
                            </form>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
            });
        });
    </script>
@stop 