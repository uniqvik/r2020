@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Package
                <small>Corporate Affiliate</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="javascript://">Packages Management</a></li>
                <li class="active">Corporate Affiliate</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary" style="padding:20px;float: left;">
                <div class=""><label style="float: left;line-height: 2;">Select Product:</label></div>
                <div class="col-md-3"> 
                    <select name="package" class="form-control" onchange="switch_package(this);">
                    @foreach($products as $product)
                        <option value="{{$product->id}}" @if($product->id == $inclusions->id)selected=selected @endif>{{$product->productName}}</option>
                        @endforeach
                    </select>
                </div>
                @if(Session::has('success_msg'))
                                <div class="col-md-12" style="padding: 0;margin-top: 20px;">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> {{session('success_msg')}}</h4>
                                </div>
                                </div>
                @endif
                <form action="{{route('updatepackage')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="redirect" value="corporateaffiliatepackage">
                   <table class="table table-bordered table-striped" style="margin-top:20px;float: left;">
                        <thead>
                            <tr>
                                    <th>Order</th>
                            <th style="width:80%">Inclusion</th>
                            <th>{{$inclusions->productName}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inclusions->inclusions as $inc)
                            @php
                             // dump($inc->toArray());
                            @endphp

                            <tr>
                                    <td><input type="number" name="order[{{$inc->pivot->id}}]" value="{{$inc->pivot->order}}" class="form-control" min="0"></td>
                                    <td style="width:80%;">{{$inc->name}}</td>
                                    <td>
                                        @if($inc->pivot->value == 'No' || $inc->pivot->value == 'Yes')
                                            <div class="form-group">
                                            <div class="radio">
                                                <label style="margin-right:5px">
                                                    <input type="radio" value="Yes" name="value[{{$inc->pivot->id}}]" @if($inc->pivot->value == 'Yes') checked @endif>Yes
                                                </label>
                                                <label>
                                                    <input type="radio" value="No" name="value[{{$inc->pivot->id}}]"  @if($inc->pivot->value == 'No') checked @endif>No
                                                </label>
                                            </div>
                                            </div>
                                        @else
                                            <select name="value[{{$inc->pivot->id}}]" class="from-control">
                                            @for( $i=0; $i<=500; $i++)
                                                <option value="{{$i}}" @if($inc->pivot->value == $i) selected @endif>{{$i}}</option>
                                            @endfor
                                            <option value="-1"  @if($inc->pivot->value == '-1') selected @endif>Unlimited</option>
                                            </select>
                                        @endif
                                        
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2"><input class="btn btn-primary" type="submit" value="Save"> </td>
                            </tr>
                        </tbody>
                   </table>
                </form>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        function switch_package(arg)
        {
            var url = '{{route('corporateaffiliatepackage')}}';
            url += '?&package='+arg.value;
            location.href = encodeURI(url); 
        }
    </script>
@stop