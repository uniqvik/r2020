@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Not Found

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Not Found</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="error-page">

                                <h2 class="headline text-info"> 404</h2>

                                <div class="error-content">

                                    <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

                                    <p>

                                        We could not find the page you were looking for.

                                        Meanwhile, you may <a href="{{url('admin/dashboard')}}">return to dashboard</a> or try using the search form.

                                    </p>

                                </div>

                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>


@stop