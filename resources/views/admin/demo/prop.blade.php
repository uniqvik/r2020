@extends('layouts.demo')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Property <small></small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/demodashboard')}}">Home</a></li>

                            <li class="breadcrumb-item active"><a href="">Property</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-examples">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="ag-grid-btns d-flex justify-content-between flex-wrap mb-1">
                                    <div class="dropdown sort-dropdown mb-1 mb-sm-0">
                                        <button class="btn btn-white filter-btn dropdown-toggle border text-dark" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            1 - 20 of 500
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton6">
                                            <a class="dropdown-item" href="#">20</a>
                                            <a class="dropdown-item" href="#">50</a>
                                            <a class="dropdown-item" href="#">100</a>
                                            <a class="dropdown-item" href="#">150</a>
                                        </div>
                                    </div>
                                    <div class="ag-btns d-flex flex-wrap">
                                        <input type="text" class="ag-grid-filter form-control w-50 mr-1 mb-1 mb-sm-0" id="filter-text-box" placeholder="Search...." />
                                        <div class="btn-export">
                                            <button class="btn btn-primary ag-grid-export-btn">
                                                Export as CSV
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="myGrid2" class="aggrid ag-theme-material"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script>

    $(document).ready(function() {
      /*** COLUMN DEFINE ***/
      var columnDefs = [
        {
          headerName: "Property Name",
          field: "firstname",
          editable: true,
          sortable: true,
          filter: true,
          width: 200,
          filter: true,
          checkboxSelection: true,
          headerCheckboxSelectionFilteredOnly: true,
          headerCheckboxSelection: true,pinned: "left"
        },
        {
          headerName: "Reference",
          field: "lastname",
          editable: true,
          sortable: true,
          filter: true,
          width: 175
        },
        {
          headerName: "Company",
          field: "company",
          editable: true,
          sortable: true,
          filter: true,
          width: 250
        },
        {
          headerName: "City",
          field: "city",
          editable: true,
          sortable: true,
          filter: true,
          width: 125
        },
        {
          headerName: "Country",
          field: "country",
          editable: true,
          sortable: true,
          filter: true,
          width: 150
        },
        {
          headerName: "State",
          field: "state",
          editable: true,
          sortable: true,
          filter: true,
          width: 125
        },
        {
          headerName: "Zip",
          field: "zip",
          editable: true,
          sortable: true,
          filter: true,
          width: 125
        },
        {
          headerName: "Email",
          field: "email",
          editable: true,
          sortable: true,
          filter: true,
          width: 260,

        },
        {
          headerName: "Followers",
          field: "followers",
          editable: true,
          sortable: true,
          filter: true,
          width: 150
        }
      ];

      /*** GRID OPTIONS ***/
      var gridOptions = {
        columnDefs: columnDefs,
        rowSelection: "multiple",
        floatingFilter: true,
        filter: true,
        pagination: true,
        paginationPageSize: 20,
        pivotPanelShow: "always",
        colResizeDefault: "shift",
        animateRows: true,
        resizable: true
      };

      /*** DEFINED TABLE VARIABLE ***/
      var gridTable = document.getElementById("myGrid2");

      /*** GET TABLE DATA FROM URL ***/

      agGrid
        .simpleHttpRequest({ url: "{{asset('adminassets/data/ag-grid-data.json')}}" })
        .then(function(data) {
          gridOptions.api.setRowData(data);
        });

      /*** FILTER TABLE ***/
      function updateSearchQuery(val) {
        gridOptions.api.setQuickFilter(val);
      }

      $(".ag-grid-filter").on("keyup", function() {
        updateSearchQuery($(this).val());
      });

      /*** CHANGE DATA PER PAGE ***/
      function changePageSize(value) {
        gridOptions.api.paginationSetPageSize(Number(value));
      }

      $(".sort-dropdown .dropdown-item").on("click", function() {
        var $this = $(this);
        changePageSize($this.text());
        $(".filter-btn").text("1 - " + $this.text() + " of 500");
      });

      /*** EXPORT AS CSV BTN ***/
      $(".ag-grid-export-btn").on("click", function(params) {
        gridOptions.api.exportDataAsCsv();
      });

      /*** INIT TABLE ***/
      new agGrid.Grid(gridTable, gridOptions);

      /*** SET OR REMOVE EMAIL AS PINNED DEPENDING ON DEVICE SIZE ***/

      if ($(window).width() < 768) {
        gridOptions.columnApi.setColumnPinned("email", null);
      } else {
        gridOptions.columnApi.setColumnPinned("email", "left");
      }
      $(window).on("resize", function() {
        if ($(window).width() < 768) {
          gridOptions.columnApi.setColumnPinned("email", null);
        } else {
          gridOptions.columnApi.setColumnPinned("email", "left");
        }
      });
    });

    </script>


@endsection

