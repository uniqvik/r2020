@extends('layouts.demo')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0"> My profile <small>Edit </small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/demodashboard')}}">Home</a></li>

                            <li class="breadcrumb-item active"><a href="">My profile </a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">

                                <form method="POST" action="" name="rolesForm" id="rolesForm" class="form form-vertical" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="IyCnTxsU20zcKLgwLCtWRaldsnCXZcPzgfK9yVlR">  <input type="hidden" name="_method" value="PUT">
                                                                            <div class="form-body">
                                            <div class="row">

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="text" name="Email" class="form-control" required="" value="billy@kwdubai.ae">
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Full Name</label>
                                                        <input type="text" name="Email" class="form-control" required="" value="Billy Rautenbach">
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Phone number</label>
                                                        <input type="text" name="Email" class="form-control" required="" value="0585179098">
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Secondary phone number</label>
                                                        <input type="text" name="Email" class="form-control" required="" value="">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Email Language Preference*</label>
                                                        <select name="" id="" class="form-control">
                                                            <option value="">English</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                    <a href="" class="btn btn-default waves-effect waves-light">Cancel</a></div>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
