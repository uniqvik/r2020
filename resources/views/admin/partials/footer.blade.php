<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.1.0
    </div>
    <strong>Copyright  2018 <a href="https://realopedia.com">Realopedia</a>.</strong> All rights
    reserved.
</footer>