<!-- sidebar: style can be found in sidebar.less -->
<aside class="main-sidebar">
<section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
        <li class="active">
            <a href="{{url('/admin/dashboard')}}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        @can('accessPermission','user,role,association')
        <li class="treeview">
            <a href="#">
                <i class="fa  fa-user"></i>
                <span>User Management</span>
                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                @can('accessPermission','role')
                <li><a href="{{route('roles.index')}}"><i class="fa fa-circle-o"></i> Roles</a></li>
                @endcan
                @can('accessPermission','association')
                <li><a href="{{route('associations.index')}}"><i class="fa fa-circle-o"></i> Associations/Groups</a></li>
                @endcan
                @can('accessPermission','user')
                <li><a href="{{route('users.index')}}"><i class="fa fa-circle-o"></i> Users</a></li>
                @endcan
            </ul>
        </li>
        @endcan
        @can('accessPermission','location')
        <li class="treeview">
            <a href="#">
                <i class="fa  fa-map-marker"></i>
                <span>Location Management</span>
                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{route('country.index')}}"><i class="fa fa-circle-o"></i> Country</a></li>
                <li><a href="{{route('zone.index')}}"><i class="fa fa-circle-o"></i> Zone</a></li>
                <li><a href="{{route('city.index')}}"><i class="fa fa-circle-o"></i> City</a></li>
                <li><a href="{{route('community.index')}}"><i class="fa fa-circle-o"></i> Community</a></li>
                <li><a href="{{route('subcommunity.index')}}"><i class="fa fa-circle-o"></i> Subcommunity</a></li>
            </ul>
        </li>
        @endcan
        @can('accessPermission',['product','inclusion'])
        <li class="treeview">
            <a href="#">
                <i class="fa fa-suitcase"></i>
                <span>Products Management</span>
                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>

            <ul class="treeview-menu">
                @can('accessPermission','product')
                <li><a href="{{route('products.index')}}"><i class="fa fa-circle-o"></i>Products</a></li>
                @endcan
                @can('accessPermission','inclusion')
                <li><a href="{{route('inclusions.index')}}"><i class="fa fa-circle-o"></i>Inclusions</a></li>
                @endcan
                {{--<li><a href="{{route('productIncludes.index')}}"><i class="fa fa-circle-o"></i> Product Includes</a></li>--}}
            </ul>
        </li>
        @endcan
        @can('accessPermission','package')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-server"></i>
                    <span>Packages Management</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('agentpackage')}}"><i class="fa fa-circle-o"></i> Agent Packages</a></li>
                    <li><a href="{{route('companypackage')}}"><i class="fa fa-circle-o"></i> Company Packages</a></li>
                    <li><a href="{{route('developerpackage')}}"><i class="fa fa-circle-o"></i> Developer Packages</a></li>
                    <li><a href="{{route('corporateaffiliatepackage')}}"><i class="fa fa-circle-o"></i> Corporate Affiliate Packages</a></li>
                </ul>
            </li>
        @endcan
        @can('accessPermission',['email'])
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-envelope-o"></i>
                    <span>Email Management</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        <li><a href="{{route('campaign.index')}}"><i class="fa fa-circle-o"></i>Campaigns</a></li>
                        <li><a href="{{route('template.index')}}"><i class="fa fa-circle-o"></i>Email Templates</a></li>
                        <li><a href="{{route('inclusions.index')}}"><i class="fa fa-circle-o"></i>Schedules</a></li>
                    {{--<li><a href="{{route('productIncludes.index')}}"><i class="fa fa-circle-o"></i> Product Includes</a></li>--}}
                </ul>
            </li>
        @endcan
        @can('accessPermission',['cms'])
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-television"></i>
                    <span>CMS Management</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        @can('accessPermission',['cms_category'])
                        <li><a href="{{route('cmscategories.index')}}"><i class="fa fa-circle-o"></i>CMS Category</a></li>
                        @endcan
                        <li><a href="{{route('cms.index')}}"><i class="fa fa-circle-o"></i>CMS Listing</a></li>
                        
                </ul>
            </li>
        @endcan
        @can('accessPermission',['seo'])
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-television"></i>
                    <span>SEO Management</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        
                        <li><a href="{{route('seo.index')}}"><i class="fa fa-circle-o"></i>SEO</a></li>
                        
                        
                </ul>
            </li>
        @endcan
        @can('accessPermission',['propertyfeild'])
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-television"></i>
                    <span>Property Options</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        
                        <li><a href="{{route('propertyfeild.index')}}"><i class="fa fa-circle-o"></i>Property Feilds</a></li>
                        <li><a href="{{route('propertyfeildlist.index')}}"><i class="fa fa-circle-o"></i>Property Feild List</a></li>
                        
                </ul>
            </li>
        @endcan
        @can('accessPermission',['property'])
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-building"></i>
                    <span>Property</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('property.index')}}"><i class="fa fa-circle-o"></i>Property</a></li>
                </ul>
            </li>
        @endcan
        @can('accessPermission',['project'])
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-building"></i>
                    <span>Project</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('project.index')}}"><i class="fa fa-circle-o"></i>Project</a></li>
                </ul>
            </li>
        @endcan

        @can('accessPermission',['setting'])
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-gear"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('setting.index')}}"><i class="fa fa-circle-o"></i>Keys And Code</a></li>
                </ul>
            </li>
        @endcan
    </ul>
</section>
<!-- /.sidebar -->
</aside>