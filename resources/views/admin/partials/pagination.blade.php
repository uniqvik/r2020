<style>
    .pagination {
	justify-content: center;
}
</style>
<div id="page1-content" class="border-grey border-lighten-2 p-1 mb-1" style="background: #fff;text-align: center;margin-top: 10px;">Showing {{($results->currentpage()-1)*$results->perpage()+1}} to
    {{($results->currentpage()-1)*$results->perpage()+$results->count()}}
    of  {{$results->total()}} entries</div>

    <div class="text-center">
        {{ $results->render() }}
    </div>

