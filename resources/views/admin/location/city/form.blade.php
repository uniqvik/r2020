@extends('layouts.admin')
@section('content')
<script>
    var app = angular.module('location', [], function($interpolateProvider) {
      $interpolateProvider.startSymbol('<%');
      $interpolateProvider.endSymbol('%>');
    });
    app.controller('MainCtrl', function($scope,$http) {
      $scope.loadCountry = function(){
        $http({
            method: 'GET',
            url: '{{url('admin/location/getCountries')}}'
        }).then(function successCallback(response) {
            $scope.countries = response.data;
            @if(isset($city->id))
             $scope.country = '{{$city->country}}';
            @endif
        }, function errorCallback(response) {
        });
      }

      $scope.loadState = function(){

        @if(isset($city->country))
        var con = '{{$city->country}}';
        @endif
        if($scope.country){con = $scope.country;}
        $http({
          method: 'POST',
          data: { 'id' : con },
          url: '{{url('admin/location/getZonesbyId')}}'
        }).then(function successCallback(response) {
            $scope.states = response.data;
            @if(isset($city->id))
             $scope.state = '{{$city->zone}}';
            @endif
        }, function errorCallback(response) {

        });
      }
    });
    </script>
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">   Locations
                        <small>Manage City</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('city.index')}}">City</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" ng-app="location">
        <section id="basic-horizontal-layouts" >
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body" ng-controller="MainCtrl">
                                @if(isset($city->id)) {{--edit form--}}
                                <form method="POST"
                                action="{{route('city.update',$city->id)}}"
                                name="cityform"
                                id="cityform"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else
                                <form method="POST"
                                            action="{{route('city.store')}}"
                                            name="cityform"
                                            id="cityform"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{$city->name}}" class="form-control" data-validation="required">

                                </div>

                                <div class="form-group" ng-init="loadCountry();loadState()">
                                    <label for="country">Country</label>
                                    <select data-validation="required" class="form-control" name="country" id="country"  ng-model="country" ng-change="loadState()"  ng-model-options="{updateOn: 'onload default'}">
                                        <option value=''>Select</option>
                                        <option value="<% country.id %>" data-ng-repeat="country in countries"><% country.name %></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <label for="zone">Zone</label>
                                    <select data-validation="required" id="state" name="zone" ng-model="state" class="form-control">
                                            <option value=''>Select</option>
                                            <option value="<% state.id %>" data-ng-repeat="state in states"><% state.name %></option>
                                        </select>


                                    </div>
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="latitude" value="{{$city->latitude}}" class="form-control" data-validation="required">
                                </div>
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="longitude" value="{{$city->longitude}}" class="form-control" data-validation="required">
                                </div>
                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('city.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
