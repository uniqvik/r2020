@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Locations
                <small>Manage Sub Communities</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('subcommunity.index')}}">Sub Community</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Name :</label><span>{{$subcommunity->name}}</span>
                    </div>
                   
                    <div class="form-group">
                        <label>Country :</label><span>{{$subcommunity->countryDet->name}}</span>
                    </div>
                    <div class="form-group">
                        <label>Community :</label><span>{{$subcommunity->community->name}}</span>
                    </div>
                    <div class="form-group">
                        <label>Latitude :</label><span>{{$subcommunity->latitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Longitude :</label><span>{{$subcommunity->longitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Description :</label><span>{{$subcommunity->description}}</span>
                    </div>
                   
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop