@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Location
                        <small>Manage Subcommunity</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('subcommunity.index')}}">Subcommunity</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('adminassets/js/getzone.js') }}"></script>
    <div class="content-body">
        <div class="card-content">
            <div class="data-list-view-header">
                <div class="card filter" ng-app="location">
                    <div class="card-header">
                        <h4 class="card-title">Filters</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                                <li><a href="{{route('subcommunity.index')}}"><i class="feather icon-rotate-cw users-data-filter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="users-list-filter" ng-controller="MainCtrl">
                                <form method="GET" action="{{route('subcommunity.index')}}">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Country</label>
                                            <fieldset class="form-group" ng-init="loadCountry();">

                                                    <select class="form-control" name="country" id="country"  ng-model="country" ng-change="loadCommunity()">
                                                    <option value=''>Select Country</option>
                                                    <option value="<% country.id %>" data-ng-repeat="country in countries"><% country.name %></option>
                                                    </select>

                                            </fieldset>

                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Community</label>
                                            <fieldset class="form-group">

                                                <select id="state" name="community" ng-model="community" class="form-control">
                                                    <option value=''>Select Community</option>
                                                    <option value="<% community.id %>" data-ng-repeat="community in communities"><% community.name %></option>
                                                </select>

                                        </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Name</label>
                                            <fieldset class="form-group">
                                               <input type="text" class="form-control" value="{{app('request')->input('name')}}" name="name">
                                            </fieldset>
                                        </div>
                                        {{-- <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Status</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" id="users-list-status">
                                                    <option value="">All</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Blocked">Blocked</option>
                                                    <option value="deactivated">Deactivated</option>
                                                </select>
                                            </fieldset>
                                        </div> --}}
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role"></label>
                                        <button class="btn btn-primary search" type="submit"><i class="feather icon-search"></i> Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 noapd">
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route("subcommunity.create") }}"><i class="feather icon-plus"></i> Subcommunity</a>
                    </div>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p><i class="icon fa fa-check"></i> {{session('success_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
                    @if(Session::has('fail_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-danger alert-dismissible">

                            <p><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
            <div class="table-responsive mt-1">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table class="table table-hover-animation mb-0">
                    <thead>
                        <tr>
                            <th style="width:2%">Sl.no</th>
                            <th >Action</th>
                            <th>Name</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Country</th>
                            <th>Community</th>
                            <th>Created Date</th>
                            <th>Modified Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($subcommunities)
                            @if($subcommunities->currentpage() == 1)
                                @php($ctr = 1)
                            @else
                                @php($ctr = ($subcommunities->currentpage()-1)*$subcommunities->perpage()+1)
                            @endif
                            @if($subcommunities->count() > 0)
                                @foreach($subcommunities as $subcommunity)
                                    <tr>
                                        <td >{{$ctr}}</td>
                                        <td>
                                            <span style="float: left"><a href="{{route('subcommunity.edit',$subcommunity->id)}}" data-toggle="tooltip" title="Edit"><i class="feather icon-edit"></i></a></span>

                                            <span style="float: left">
                                        <form action="{{route('subcommunity.destroy',$subcommunity->id) }}" method="post" id="deleteForm{{$subcommunity->id}}">
                                            <input type="hidden" name="_method" value="DELETE">
                                            {!! csrf_field() !!}
                                            <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$subcommunity->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                <i class="feather icon-trash"></i></a>
                                        </form>
                                        </span>
                                            @if($subcommunity->is_active)
                                                <span><a href="{{route('subcommunity.statusChange',$subcommunity->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                        <span style="color: yellowgreen"><i class="feather icon-check"></i></span></a></span>
                                            @else
                                                <span><a href="{{route('subcommunity.statusChange',$subcommunity->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                        <span style="color: red"><i class="feather icon-x"></i></span></a></span>
                                            @endif
                                        </td>
                                        <td>{{$subcommunity->name}} </td>
                                        <td>{{$subcommunity->latitude}} </td>
                                        <td>{{$subcommunity->longitude}} </td>
                                        <td>{{$subcommunity->countryDet->name}} </td>
                                        <td>{{$subcommunity->Community->name}} </td>
                                        <td>{{$subcommunity->created_at->format('Y-m-d')}}</td>
                                        <td>{{$subcommunity->updated_at->format('Y-m-d')}}</td>
                                    </tr>
                                    @php($ctr++)
                                @endforeach
                            @else
                                <tr><td colspan="10" style="text-align: center">No records available</td></tr>
                            @endif
                        @endif
                        </tbody>
                </table>
                </div>
                @include('admin/partials/pagination',['results'=>$subcommunities])

            </div>
            </div>
        </div>
    </div>
</div>

@endsection
