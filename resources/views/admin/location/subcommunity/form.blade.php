@extends('layouts.admin')
@section('content')
<script>
    var app = angular.module('location', [], function($interpolateProvider) {
      $interpolateProvider.startSymbol('<%');
      $interpolateProvider.endSymbol('%>');
    });
    app.controller('MainCtrl', function($scope,$http) {
      $scope.loadCountry = function(){
        $http({
            method: 'GET',
            url: '{{url('admin/location/getCountries')}}'
        }).then(function successCallback(response) {
            $scope.countries = response.data;
            @if(isset($subcommunity->id))
             $scope.country = '{{$subcommunity->country}}';
            @endif
        }, function errorCallback(response) {
        });
      }

      $scope.loadCommunity = function(){
        @if(isset($subcommunity->id))
        var con = '{{$subcommunity->country}}';
        @endif
        if($scope.country){con = $scope.country;}
        $http({
          method: 'POST',
          data: { 'id' : con },
          url: '{{url('admin/location/getCommunitybyId')}}'
        }).then(function successCallback(response) {
            $scope.communities = response.data;
            @if(isset($subcommunity->id))
             $scope.community = '{{$subcommunity->community_id}}';
            @endif
        }, function errorCallback(response) {

        });
      }
    });
    </script>
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">   Locations
                        <small>Manage Sub Community</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('subcommunity.index')}}">Sub Community</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" ng-app="location">
        <section id="basic-horizontal-layouts" >
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body" ng-controller="MainCtrl">
                                @if(isset($subcommunity->id)) {{--edit form--}}
                                <form method="POST"
                                action="{{route('subcommunity.update',$subcommunity->id)}}"
                                name="cityform"
                                id="cityform"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else
                                <form method="POST"
                                            action="{{route('subcommunity.store')}}"
                                            name="cityform"
                                            id="cityform"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{$subcommunity->name}}" class="form-control" data-validation="required">

                                </div>

                                <div class="form-group" ng-init="loadCountry();loadCommunity()">
                                    <label for="country">Country</label>
                                    <select data-validation="required" class="form-control" name="country" id="country"  ng-model="country" ng-change="loadCommunity()"  ng-model-options="{updateOn: 'onload default'}">
                                        <option value=''>Select</option>
                                        <option value="<% country.id %>" data-ng-repeat="country in countries"><% country.name %></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <label for="zone">Community</label>
                                    <select data-validation="required" id="state" name="community_id" ng-model="community" class="form-control">
                                        <option value=''>Select Community</option>
                                        <option value="<% community.id %>" data-ng-repeat="community in communities"><% community.name %></option>
                                    </select>


                                    </div>
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="latitude" value="{{$subcommunity->latitude}}" class="form-control" data-validation="required">
                                </div>
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="longitude" value="{{$subcommunity->longitude}}" class="form-control" data-validation="required">
                                </div>
                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('subcommunity.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
