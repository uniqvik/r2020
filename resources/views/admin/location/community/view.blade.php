@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Locations
                <small>Manage Communities</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('community.index')}}">Community</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Name :</label><span>{{$community->name}}</span>
                    </div>
                   
                    <div class="form-group">
                        <label>Country :</label><span>{{$community->countryDet->name}}</span>
                    </div>
                    <div class="form-group">
                        <label>Zone :</label><span>{{$community->state->name}}</span>
                    </div>
                   
                    <div class="form-group">
                        <label>Latitude :</label><span>{{$community->latitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Longitude :</label><span>{{$community->longitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Description :</label><span>{{$community->description}}</span>
                    </div>
                   
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop