@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Locations
                <small>Manage Countries</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('country.index')}}">Country</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Name :</label><span>{{$country->name}}</span>
                    </div>
                    <div class="form-group">
                        <label>ISO Code 2 :</label><span>{{$country->iso_code_2}}</span>
                    </div>
                    <div class="form-group">
                        <label>ISO Code 3 :</label><span>{{$country->iso_code_3}}</span>
                    </div>
                    <div class="form-group">
                        <label>Currency Code :</label><span>{{$country->currency_code}}</span>
                    </div>
                    <div class="form-group">
                        <label>Latitude :</label><span>{{$country->latitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Longitude :</label><span>{{$country->longitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Description :</label><span>{{$country->description}}</span>
                    </div>
                    <div class="form-group">
                        <label>Image :</label>
                        <span class="w100"><img src="{{ url('/') }}{{$country->image}}" style="width:100px"></span>
                    </div>
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop