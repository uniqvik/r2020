@extends('layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">  Locations
                        <small>Manage Countries</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('country.index')}}">Countries</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if(isset($country->id)) {{--edit form--}}
                                <form method="POST"
                                action="{{route('country.update',$country->id)}}"
                                name="associationsForm"
                                id="associationsForm"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else
                                <form method="POST"
                                            action="{{route('country.store')}}"
                                            name="associationsForm"
                                            id="associationsForm"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{$country->name}}" class="form-control" data-validation="required">

                                </div>
                                <div class="form-group">

                                        <label>ISO Code 2</label>
                                        <input type="text" name="iso_code_2" value="{{$country->iso_code_2}}" class="form-control" data-validation="required">
                                </div>
                                <div class="form-group">

                                    <label>ISO Code 3</label>
                                    <input type="text" name="iso_code_3" value="{{$country->iso_code_3}}" class="form-control" data-validation="required">
                            </div>
                            <div class="form-group">

                                <label>Currency Code</label>
                                <input type="text" name="currency_code" value="{{$country->currency_code}}" class="form-control" data-validation="required">
                        </div>
                        <div class="form-group">

                            <label>Latitude</label>
                            <input type="text" name="latitude" value="{{$country->latitude}}" class="form-control" data-validation="required">
                    </div>
                    <div class="form-group">

                        <label>Longitude</label>
                        <input type="text" name="longitude" value="{{$country->longitude}}" class="form-control" data-validation="required">
                </div>
                <div class="form-group">

                    <label>Image</label>
                    <input type="file" name="image" value="{{$country->longitude}}" class="form-control" data-validation="required">
            </div>
            @if(isset($country->image))
            <a href="{{ url('/') }}{{$country->image}}" target="_blank">View Image</a>
        @endif

                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('country.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
