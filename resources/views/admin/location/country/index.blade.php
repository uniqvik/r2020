@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Location
                        <small>Manage Countries</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('country.index')}}">Countries</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="card-content">
            <div class="data-list-view-header">
                <div class="card filter">
                    <div class="card-header">
                        <h4 class="card-title">Filters</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                                <li><a href="{{route('country.index')}}"><i class="feather icon-rotate-cw users-data-filter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="users-list-filter">
                                <form method="GET" action="{{route('country.index')}}">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Name</label>
                                            <fieldset class="form-group">
                                               <input type="text" class="form-control" value="{{app('request')->input('title')}}" name="title">
                                            </fieldset>
                                        </div>
                                        {{-- <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Status</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" id="users-list-status">
                                                    <option value="">All</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Blocked">Blocked</option>
                                                    <option value="deactivated">Deactivated</option>
                                                </select>
                                            </fieldset>
                                        </div> --}}
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role"></label>
                                        <button class="btn btn-primary search" type="submit"><i class="feather icon-search"></i> Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 noapd">
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route("country.create") }}"><i class="feather icon-plus"></i> Country</a>
                    </div>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p><i class="icon fa fa-check"></i> {{session('success_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
                    @if(Session::has('fail_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-danger alert-dismissible">

                            <p><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
            <div class="table-responsive mt-1">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table class="table table-hover-animation mb-0">
                    <thead>
                        <tr>
                            <th style="width:2%">Sl.no</th>
                            <th >Action</th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'name','order'=>$order]) }}">Name</a></th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'latitude','order'=>$order]) }}">Latitude</a></th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'longitude','order'=>$order]) }}">Longitude</a></th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'iso_code_2','order'=>$order]) }}">ISO code 2</a></th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'iso_code_3','order'=>$order]) }}">ISO code 3</a></th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'currency_code','order'=>$order]) }}">Currency code</a></th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'created_at','order'=>$order]) }}">Created Date</a></th>
                            <th><a href="{{ Request::fullUrlWithQuery(['sort' => 'updated_at','order'=>$order]) }}">Modified Date</a></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($countries)
                            @if($countries->currentpage() == 1)
                                @php($ctr = 1)
                            @else
                                @php($ctr = ($countries->currentpage()-1)*$countries->perpage()+1)

                            @endif
                            @if($countries->count() > 0)
                            @foreach($countries as $country)
                                <tr>
                                    <td >{{$ctr}}</td>
                                    <td>
                                        <span style="float: left"><a href="{{route('country.edit',$country->id)}}" data-toggle="tooltip" title="Edit"><i class="feather icon-edit"></i></a></span>


                                        <span style="float: left">
                                        <form action="{{route('country.destroy',$country->id) }}" method="post" id="deleteForm{{$country->id}}">
                                            <input type="hidden" name="_method" value="DELETE">
                                            {!! csrf_field() !!}
                                            <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$country->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                <i class="feather icon-trash"></i></a>

                                        </form>
                                        </span>

                                        @if($country->is_active)
                                            <span><a href="{{route('country.statusChange',$country->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                    <span style="color: yellowgreen"><i class="feather icon-check"></i></span></a></span>
                                        @else
                                            <span ><a href="{{route('country.statusChange',$country->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                    <span style="color: red"><i class="feather icon-x"></i></span></a></span>
                                        @endif

                                    </td>
                                    <td>{{$country->name}} </td>
                                    <td>{{$country->latitude}} </td>
                                    <td>{{$country->longitude}} </td>
                                    <td>{{$country->iso_code_2}} </td>
                                    <td>{{$country->iso_code_3}} </td>
                                    <td>{{$country->currency_code}} </td>
                                    <td>{{$country->created_at->format('Y-m-d')}}</td>
                                    <td>{{$country->updated_at->format('Y-m-d')}}</td>
                                </tr>
                                @php($ctr++)
                            @endforeach
                            @else
                                <tr><td colspan="10" style="text-align: center">No records available</td></tr>
                             @endif
                        @endif
                        </tbody>
                </table>
                </div>
                @include('admin/partials/pagination',['results'=>$countries])

            </div>
            </div>
        </div>
    </div>
</div>

@endsection
