@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Locations
                <small>Manage Zones</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('zone.index')}}">Zone</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Name :</label><span>{{$zone->name}}</span>
                    </div>
                   
                    <div class="form-group">
                        <label>ISO Code 3 :</label><span>{{$zone->code}}</span>
                    </div>
                    <div class="form-group">
                        <label>Country :</label><span>{{$zone->countryDet->name}} </span>
                    </div>
                    <div class="form-group">
                        <label>Latitude :</label><span>{{$zone->latitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Longitude :</label><span>{{$zone->longitude}}</span>
                    </div>
                    <div class="form-group">
                        <label>Description :</label><span>{{$zone->description}}</span>
                    </div>
                    <div class="form-group">
                        <label>Image :</label>
                        @if($zone->image)
                        <span class="w100"><img src="{{ url('/') }}{{$zone->image}}" style="width:100px"></span>
                        @endif
                    </div>
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop