@extends('layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">   Locations
                        <small>Manage Zones</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('zone.index')}}">Zone</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if(isset($zone->id)) {{--edit form--}}
                                <form method="POST"
                                action="{{route('zone.update',$zone->id)}}"
                                name="zoneForm"
                                id="zoneForm"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else
                                <form method="POST"
                                            action="{{route('zone.store')}}"
                                            name="zoneForm"
                                            id="zoneForm"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{$zone->name}}" class="form-control" data-validation="required">

                                </div>
                                <div class="form-group">
                                        <label>Code</label>
                                        <input type="text" name="code" value="{{$zone->code}}" class="form-control" data-validation="required">
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <select name="country" class="form-control" name="country">
                                        <option value="" @if($zone->country == '') selected @endif>Select Country</option>
                                        @foreach ($countries as $country)
                                        <option value="{{$country->id}}" @if($zone->country == $country->id) selected @endif>{{$country->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="latitude" value="{{$zone->latitude}}" class="form-control" data-validation="required">
                                </div>
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="longitude" value="{{$zone->longitude}}" class="form-control" data-validation="required">
                                </div>
                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('zone.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
