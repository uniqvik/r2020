@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Campaigns
                <small>Manage Email Campaigns</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('campaign.index')}}">Campaigns</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                           
                            <div class="form-group">
                                <label for="title">Title :</label>
                                {{$campaign->title}}
                            </div>
                            <div class="form-group">
                                <label for="title">Description :</label>
                                {{$campaign->description}}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
            });
        });
        $(function () {
            //CKEDITOR.replace('description');
        })
    </script>
@stop