@extends('layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">  Email  Template
                        <small>Manage Email Templates</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('template.index')}}">Email Template</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if(isset($template->id)) {{--edit form--}}

                                <form method="POST"
                                action="{{route('template.update',$template->id)}}"
                                name="associationsForm"
                                id="associationsForm"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else

                                <form method="POST"
                                            action="{{route('template.store')}}"
                                            name="associationsForm"
                                            id="associationsForm"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label for="name">Template Type</label>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label style="margin-right:5px">
                                                <input type="radio" value="mail_body" name="type" onclick="show_option('mail_body');">Mail Body
                                            </label>
                                            <label>
                                                <input type="radio" value="header" name="type" onclick="show_option('header');">Header
                                            </label>
                                            <label>
                                                <input type="radio" data-validation="type" value="footer" name="type" onclick="show_option('footer');">Footer
                                            </label>
                                        </div>
                                        </div>

                                </div>
                                <div class="form-group">

                                    <label for="name">Mail Subject*</label>
                                    <input class="form-control valid" value="{{ ($template->id ? $template->mailSubject: old('mailSubject') ) }}" data-validation="required" name="mailSubject" type="text" id="mailSubject">
                                </div>
                                <div class="form-group head_foot">
                                    <label for="name">Campaign*</label>
                                    <select name="campaign" class="form-control valid" data-validation="required" data-validation-depends-on="type" data-validation-depends-on-value="mail_body">
                                        <option value="">Select Campaign</option>
                                        @foreach ($campaigns as $campaign)
                                        <option value="{{$campaign->id}}" @if($template->campaign == $campaign->id) selected="selected" @endif>{{$campaign->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6 head_foot float-left" style="padding-left: 0">
                                    <label for="name">Mail From*</label>
                                    <input class="form-control valid" value="{{ ($template->id ? $template->mail_from: old('mail_from') ) }}" data-validation="required" name="mail_from" type="text" id="mail_from" data-validation-depends-on="type" data-validation-depends-on-value="mail_body">
                                </div>
                                <div class="form-group col-md-6 head_foot float-left" style="padding-right: 0">
                                    <label for="name">Reply To*</label>
                                    <input class="form-control valid" value="{{ ($template->id ? $template->replyTo: old('replyTo') ) }}" data-validation="required" name="replyTo" type="text" id="replyTo" data-validation-depends-on="type" data-validation-depends-on-value="mail_body">
                                </div>

                                @php
                                           if($template->id){$hdr = $template->header;}else{$hdr ='';}
                                           if($template->id){$ftr = $template->footer;}else{$ftr ='';}
                                        @endphp
                                <div class="form-group col-md-6 head_foot float-left" style="padding-left: 0">
                                    <label for="name">Header Template</label>
                                    <select name="header" class="form-control valid" data-validation="required" data-validation-depends-on="type" data-validation-depends-on-value="mail_body">
                                        <option value="">Select Header</option>
                                        @foreach ($header as $head)
                                        <option value="{{$head->id}}" @if($hdr == $head->id) selected="selected" @endif>{{$head->mailSubject}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6 head_foot float-left" style="padding-right: 0">
                                    <label for="name">Footer Template</label>
                                    <select name="footer" class="form-control valid" data-validation="required">
                                        <option value="">Select Footer</option>
                                        @foreach ($footer as $foot)
                                        <option value="{{$foot->id}}"  @if($ftr == $foot->id) selected="selected" @endif>{{$foot->mailSubject}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="w100"></div>
                                <div class="form-group">
                                    <label for="name">Mail Body</label>
                                    <textarea name="description" class="form-control valid" data-validation="required" cols="30" rows="10">{{$template->description}}</textarea>
                                </div>

                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('template.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@section('script')
@if(isset($template->id))
<script>jQuery( document ).ready(function() {jQuery('input:radio[name=type][value={{$template->type}}]').attr('checked', true);show_option('{{$template->type}}');});</script>
@else
<script>$( document ).ready(function() {$('input:radio[name=type][value=mail_body]').attr('checked', true);show_option('mail_body');});</script>
@endif
    <script>
        function show_option(opt){

           if(opt == 'mail_body'){
               $('.head_foot').show();
           }else{$('.head_foot').hide();}
       }
    </script>
@endsection
