@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Email Templates
                        <small>Manage Templates</small>
                        </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="">Email Templates</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="card-content">
            <div class="data-list-view-header">
                <div class="card filter">
                    <div class="card-header">
                        <h4 class="card-title">Filters</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                                <li><a href="{{route('template.index')}}"><i class="feather icon-rotate-cw users-data-filter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="users-list-filter">
                                <form method="GET" action="{{route('template.index')}}">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Email Type</label>
                                            <select name="type" class="form-control">
                                                <option value="">Type</option>
                                                <option value="mail_body"  @if(app('request')->input('type') == 'mail_body') selected @endif>Mail Body</option>
                                                <option value="header" @if(app('request')->input('type') == 'header') selected @endif>Header</option>
                                                <option value="footer" @if(app('request')->input('type') == 'footer') selected @endif>Footer</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Campaign</label>
                                                <select name="campaign" class="form-control">
                                                    <option value="">Campaign</option>
                                                    @foreach($campaigns as $campaign)
                                                    <option value="{{$campaign->id}}" @if(app('request')->input('campaign') == $campaign->id) selected @endif>{{$campaign->title}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Status</label>
                                        <select  name="status" class="form-control">

                                                <option value=''>Status</option>
                                                <option value="1" @if(app('request')->input('status') == 1) selected @endif>Active</option>
                                                <option value="2" @if(app('request')->input('status') == 2) selected @endif>Inactive</option>
                                            </select>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Email Subject</label>
                                            <fieldset class="form-group">
                                               <input type="text" class="form-control" value="{{app('request')->input('subject')}}" name="subject">
                                            </fieldset>
                                        </div>
                                        {{-- <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Status</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" id="users-list-status">
                                                    <option value="">All</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Blocked">Blocked</option>
                                                    <option value="deactivated">Deactivated</option>
                                                </select>
                                            </fieldset>
                                        </div> --}}
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role"></label>
                                        <button class="btn btn-primary search" type="submit"><i class="feather icon-search"></i> Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 noapd">
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route("template.create") }}"><i class="feather icon-plus"></i> Email Template</a>
                    </div>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p><i class="icon fa fa-check"></i> {{session('success_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
                    @if(Session::has('fail_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-danger alert-dismissible">

                            <p><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
            <div class="table-responsive mt-1">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table class="table table-hover-animation mb-0">

                    <thead>
                        <tr>
                            <th style="width:2%">Sl.no</th>
                            <th >Action</th>
                            <th>Email Subject</th>
                            <th>type</th>
                            <th>Campaign</th>
                            <th>Reply To</th>
                            <th>Mail From</th>
                            <th>Created Date</th>
                            <th>Modified Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($emailTemps)
                            @if($emailTemps->currentpage() == 1)
                                @php($ctr = 1)
                            @else
                                @php($ctr = ($emailTemps->currentpage()-1)*$emailTemps->perpage()+1)

                            @endif
                            @foreach($emailTemps as $emailTemp)
                                <tr>
                                    <td>{{$ctr}}</td>
                                    <td>
                                        <span style="float: left;margin-right:5px"><a href="{{route('template.edit',$emailTemp->id)}}" data-toggle="tooltip" title="Edit"><i class="feather icon-edit"></i></a></span>
                                        <span style="float: left;margin-right:5px"><a target="_blank" href="{{route('template.show',$emailTemp->id)}}" data-toggle="tooltip" title="View"><i class="feather icon-eye"></i></a></span>
                                        <span style="float: left;margin-right:5px">
                                        <form action="{{route('template.destroy',$emailTemp->id) }}" method="post" id="deleteForm{{$emailTemp->id}}">
                                            <input type="hidden" name="_method" value="DELETE">
                                            {!! csrf_field() !!}
                                            <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$emailTemp->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                <i class="feather icon-trash"></i></a>
                                        </form>
                                        </span>
                                        @if($emailTemp->is_active)
                                            <span><a href="{{route('template.statusChange',$emailTemp->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                    <span style="color: yellowgreen"><i class="feather icon-check"></i></span></a></span>
                                        @else
                                            <span><a href="{{route('template.statusChange',$emailTemp->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                   <span style="color: red"><i class="feather icon-x"></i></span></a></span>
                                        @endif
                                    </td>
                                    <td>{{$emailTemp->mailSubject}} </td>
                                    <td style="text-transform: capitalize;">{{ str_replace('_',' ', $emailTemp->type) }} </td>
                                    <td>@if($emailTemp->campaign) {{$emailTemp->campaigns->title}}   @endif</td>
                                    <td>{{$emailTemp->replyTo}} </td>
                                    <td>{{$emailTemp->mail_from}} </td>
                                    <td>{{$emailTemp->created_at->format('Y-m-d')}}</td>
                                    <td>{{$emailTemp->updated_at->format('Y-m-d')}}</td>
                                </tr>
                                @php($ctr++)
                            @endforeach
                        @endif
                        </tbody>
                </table>
                </div>
                @include('admin/partials/pagination',['results'=>$emailTemps])

            </div>
            </div>
        </div>
    </div>
</div>

@endsection

