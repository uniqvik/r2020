@extends('layouts.admin')
@section('content')
<script>
    var app = angular.module('location', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    // app.config(['$locationProvider', function($locationProvider){
    //   $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: false
    //     })
    //   }])
    app.controller('MainCtrl', function($scope,$http) {
        $scope.loadCountry = function(){
            $http({
                method: 'GET',
                url: '{{route('getCountries')}}'
            }).then(function successCallback(response) {
                $scope.countries = response.data;
                //$scope.country = $location.search().country;
            }, function errorCallback(response) {

            });
        }
        $scope.loadState = function(){
            $http({
                method: 'POST',
                data: { 'id' : $scope.country },
                url: '{{route('getZonesbyId')}}'
            }).then(function successCallback(response) {
                $scope.states = response.data;
                //$scope.state = $location.search().zone;
            }, function errorCallback(response) {

            });
        }

        $scope.loadCommunity = function(){
            $http({
                method: 'POST',
                data: { 'id' : $scope.country },
                url: 'getCommunitybyId'
            }).then(function successCallback(response) {
                $scope.communities = response.data;
                //$scope.state = $location.search().zone;
            }, function errorCallback(response) {

            });
        }
    });
</script>
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Users
                        <small>Manage Users</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('users.index')}}">Users</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="card-content">
            <div class="data-list-view-header">
                <div class="card filter" ng-app="location">
                    <div class="card-header" >
                        <h4 class="card-title">Filters</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                                <li><a href="{{route('roles.index')}}"><i class="feather icon-rotate-cw users-data-filter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show"  ng-controller="MainCtrl">
                        <div class="card-body">
                            <div class="users-list-filter">
                                <form method="GET" action="{{route('users.index')}}">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Country</label>
                                            <fieldset class="form-group">
                                                <div class=" form-group" ng-init="loadCountry();loadState()">
                                                    <select class="form-control" name="country" id="country"  ng-model="country" ng-change="loadState()">
                                                        <option value=''>Select Country</option>
                                                        <option value="<% country.id %>" data-ng-repeat="country in countries"><% country.name %></option>
                                                    </select>
                                                </div>
                                            </fieldset>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Zone</label>
                                            <fieldset class="form-group">
                                                <select id="state" name="zone" ng-model="state" class="form-control">
                                                    <option value=''>Select Zone</option>
                                                    <option value="<% state.id %>" data-ng-repeat="state in states"><% state.name %></option>
                                                </select>
                                            </fieldset>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Name</label>
                                            <fieldset class="form-group">
                                                <input type="text" value="{{app('request')->input('name')}}" class="form-control" name="name">
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Email</label>
                                            <fieldset class="form-group">
                                                <input type="text" value="{{app('request')->input('email')}}" class="form-control" name="email">
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Status</label>
                                            <select  name="status" class="form-control">
                                                <option value=''>Status</option>
                                                <option value="1" @if(app('request')->input('status') == 1) selected @endif>Active</option>
                                                <option value="2" @if(app('request')->input('status') == 2) selected @endif>Inactive</option>
                                            </select>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Role</label>
                                            <select  name="role" class="form-control">
                                                <option value=''>Select Role</option>
                                                @foreach ($allRoles as $key => $val)
                                                <option value="{{$key}}" @if(app('request')->input('role') == $key) selected @endif>{{$val}}</option>
                                                @endforeach

                                            </select>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Associations</label>
                                            <select  name="associations" class="form-control">
                                                <option value=''>Select Associations</option>
                                                @foreach ($userGroups as $key => $val)
                                                <option value="{{$key}}" @if(app('request')->input('associations') == $key) selected @endif>{{$val}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role"></label>
                                        <button class="btn btn-primary search" type="submit"><i class="feather icon-search"></i> Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 noapd">
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route("users.create") }}"><i class="feather icon-plus"></i> User</a>
                    </div>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p><i class="icon fa fa-check"></i> {{session('success_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
                    @if(Session::has('fail_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-danger alert-dismissible">

                            <p><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
            <div class="table-responsive mt-1">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table class="table table-hover-animation mb-0">
                    <thead>
                        <tr>
                            <th>Sl.no</th>
                            <th>Action</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Association</th>

                                    <th>Created Date</th>
                                    <th>Modified Date</th>


                        </tr>
                    </thead>
                    <tbody>
                        @if($users)
                            @php($ctr = 1)
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$ctr}}</td>
                                    <td>
                                        <span><a href="{{route('users.edit',$user->id)}}" data-toggle="tooltip" title="Edit"><i class="feather icon-edit"></i></a></span>
                                        <span><a href="{{route('users.show',$user->id)}}" data-toggle="tooltip" title="View"><i class="feather icon-eye"></i></a></span>
                                        <span style="float: left">
                                                <form action="{{route('users.destroy',$user->id) }}" method="post" id="deleteForm{{$user->id}}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {!! csrf_field() !!}
                                                    <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$user->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                        <i class="feather icon-trash"></i></a>
                                                </form>
                                        </span>

                                        @if($user->is_active)
                                            <span><a href="{{route('users.statusChange',$user->id)}}" data-toggle="tooltip" title="Disable"><i class="feather icon-check"></i></a></span>
                                        @else
                                            <span><a href="{{route('users.statusChange',$user->id)}}" data-toggle="tooltip" title="Enable"><i class="feather icon-x"></i></a></span>
                                        @endif
                                    </td>
                                    <td>{{$user->contact_name}} </td>
                                    <td>{{$user->email}} </td>
                                    <td>{{$user->username}} </td>
                                    <td>{{$user->role->title}} </td>
                                    <td>{{$user->association->title}} </td>

                                    <td>{{$user->created_at->format('Y-m-d')}}</td>
                                    <td>{{$user->updated_at->format('Y-m-d')}}</td>


                                </tr>
                                @php($ctr++)
                            @endforeach
                        @endif
                        </tbody>
                </table>
                </div>
                @include('admin/partials/pagination',['results'=>$users])

            </div>
            </div>
        </div>
    </div>
</div>

@endsection
