@extends('layouts.admin')
@section('content')
<script>
    var app = angular.module('location', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    app.controller('MainCtrl', function($scope,$http) {
        $scope.loadCountry = function(){
            $http({
                method: 'GET',
                url: '{{url('location/getCountries')}}'
            }).then(function successCallback(response) {
                $scope.countries = response.data;
                $scope.country = '{{ ($user->country ? $user->country: old('country') ) }}';
            }, function errorCallback(response) {
            });
        }

        $scope.loadState = function(){


            var con = '{{ ($user->country ? $user->country: old('country') ) }}';

            if($scope.country){con = $scope.country;}
            $http({
                method: 'POST',
                data: { 'id' : con },
                url: '{{url('location/getZonesbyId')}}'
            }).then(function successCallback(response) {
                $scope.states = response.data;

                $scope.state = '{{ ($user->zone ? $user->zone: old('zone') ) }}';

            }, function errorCallback(response) {

            });
        }

        $scope.loadCity = function(){


            var con = '{{ ($user->zone ? $user->zone: old('zone') ) }}';

            if($scope.states){con = $scope.state;}
            $http({
                method: 'POST',
                data: { 'id' : con },
                url: '{{url('location/getCitiesbyId')}}'
            }).then(function successCallback(response) {
                $scope.cities = response.data;
                $scope.city = '{{ ($user->city ? $user->city: old('city') ) }}';
            }, function errorCallback(response) {

            });
        }
        $scope.checkParent = function(){
            con = $scope.parent;
            $http({
                method: 'POST',
                data: { 'id' : con },
                url: '{{route('users.checkParentRole')}}'
            }).then(function successCallback(response) {
                $scope.parentroles = response.data;
                $scope.usrs = '{{ ($user->parent_id ? $user->parent_id: old('parent_id') ) }}';
            }, function errorCallback(response) {

            });
        }

        $scope.parent = '{{ ($user->role_id ? $user->role_id: old('role_id') ) }}';

    });
</script>
<div class="content-wrapper" >
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0"> Users
                        <small>Manage Users</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" ng-app="location">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="box-body" ng-controller="MainCtrl">
                                    @if(isset($user->id))
                                        <form  method="POST" action="{{route('users.update',$user->id)}}" name="userForm" enctype="multipart/form-data">
                                            {{method_field('put')}}
                                            @else
                                                <form method="POST" action="{{route('users.index')}}" name="userForm" enctype="multipart/form-data">
                                                    @endif
                                                    {{csrf_field()}}
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Name*</label>
                                                        <input class="form-control valid" value="{{ ($user->id ? $user->contact_name: old('contact_name') ) }}" data-validation="required" name="contact_name" type="text" id="contact_name" style="">
                                                        @if ($errors->has('contact_name'))
                                                            <span class="help-block form-error">{{$errors->first('contact_name')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Company</label>
                                                        <input class="form-control valid" value="{{ ($user->id ? $user->company_name: old('company_name') ) }}"  name="company_name" type="text" id="company_name" style="">
                                                    </div>
                                                    <div class="w100"></div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">License#</label>
                                                        <input class="form-control valid" value="{{ ($user->id ? $user->license_number: old('license_number') ) }}" name="license_number" type="text" id="license_number" style="">
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">License expiry</label>
                                                        <input class="form-control" id="license_expiry" value="{{ ($user->license_expiry ? date('m/d/Y', strtotime($user->license_expiry)): old('license_expiry') ) }}" name="license_expiry" type="text" id="license_expiry">
                                                    </div>
                                                    <div class="w100"></div>
                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('email') ? 'has-error' : '' )}}">
                                                        <label for="name">Email*</label>
                                                        <input class="form-control valid" data-validation="required" name="email" type="email" id="email" value="{{ ($user->id ? $user->email: old('email') ) }}" {{ ($user->email ? 'disabled' : '' )}}>

                                                        @if ($errors->has('email'))
                                                            <span class="help-block form-error">{{$errors->first('email')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('email_alt') ? 'has-error' : '' )}}">
                                                        <label for="name">Alternate Email</label>
                                                        <input class="form-control valid" name="email_alt" type="email" id="email_alt" value="{{ ($user->email_alt ? $user->email_alt: old('email_alt') ) }}">

                                                        @if ($errors->has('email_alt'))
                                                            <span class="help-block form-error">{{$errors->first('email_alt')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="w100"></div>
                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('username') ? 'has-error' : '' )}}">
                                                        <label for="name">Username*</label>
                                                        <input class="form-control valid" data-validation="required" name="username" type="text" id="username" value="{{ ($user->id ? $user->username: old('username') ) }}" {{ ($user->username ? 'disabled' : '' )}}>
                                                        @if ($errors->has('username'))
                                                            <span class="help-block form-error">{{$errors->first('username')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('password') ? 'has-error' : '' )}}">
                                                        <label for="name">Password*</label>
                                                        <input class="form-control valid" @if(!$user->id) data-validation="required" @endif name="password" type="password" id="password" style="">
                                                        @if ($errors->has('password'))
                                                            <span class="help-block form-error">{{$errors->first('password')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="w100"></div>

                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('role_id') ? 'has-error' : '' )}}">
                                                        <label for="name">Role*</label>
                                                        <select name="role_id" id="role_id" class="form-control valid" data-validation="required" ng-init="checkParent()"  ng-model="parent" ng-change="checkParent()">
                                                            <option value="">Select Role</option>

                                                            @foreach ($allRoles as $key => $val)
                                                                <option value="{{$key}}" @if($user->role_id == $key) selected="selected" @endif>{{$val}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('role_id'))
                                                            <span class="help-block form-error">{{$errors->first('role_id')}}</span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('group_flag') ? 'has-error' : '' )}}">
                                                        <label for="name">Associations*</label>
                                                        <select name="group_flag" id="group_flag" class="form-control valid" data-validation="required">
                                                            <option value="">Select Association</option>
                                                            @php
                                                             $group_flag =      ($user->group_flag ? $user->group_flag: old('group_flag') ) ;
                                                            @endphp
                                                            @foreach ($associations as $association)
                                                                <option value="{{$association->id}}" @if($group_flag == $association->id) selected="selected" @endif>{{$association->title}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('group_flag'))
                                                            <span class="help-block form-error">{{$errors->first('group_flag')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="w100"></div>
                                                    <div class="w100" ng-if='parentroles'>
                                                            <div class="form-group col-md-6 float-left" ng-if='parentroles'>
                                                                    <label for="name">Parent Role</label>
                                                                    <select name="parent_id" id="parent_id" class="form-control" ng-model="usrs" >
                                                                        <option value="">Select Parent Role</option>
                                                                        <option value="<% usrs.id %>" data-ng-repeat="usrs in parentroles"><% usrs.contact_name %></option>
                                                                    </select>
                                                                </div>
                                                    </div>

                                                    <div class="form-group col-md-6 float-left  {{ ($errors->has('mobile') ? 'has-error' : '' )}}">
                                                        <label for="name">Mobile</label>
                                                        <input class="form-control"  name="mobile" type="text" id="mobile" value="{{ ($user->id ? $user->mobile: old('mobile') ) }}">
                                                        @if ($errors->has('mobile'))
                                                            <span class="help-block form-error">{{$errors->first('mobile')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Telephone</label>
                                                        <input class="form-control"  name="telephone" type="text" id="telephone" value="{{ ($user->id ? $user->telephone: old('telephone') ) }}">

                                                    </div>
                                                    <div class="w100"></div>
                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('country') ? 'has-error' : '' )}}" ng-init="loadCountry();loadState();loadCity()">
                                                        <label for="name">Country*</label>
                                                        <select name="country" id="country" class="form-control valid" data-validation="required" ng-model="country" ng-change="loadState()"  ng-model-options="{updateOn: 'onload default'}">
                                                            <option value="">Select Country</option>
                                                            <option value="<% country.id %>" data-ng-repeat="country in countries"><% country.name %></option>
                                                        </select>
                                                        @if ($errors->has('country'))
                                                            <span class="help-block form-error">{{$errors->first('country')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6 float-left {{ ($errors->has('zone') ? 'has-error' : '' )}}">
                                                        <label for="name">Zone*</label>
                                                        <select name="zone" id="zone" class="form-control valid" data-validation="required" ng-model="state" ng-change="loadCity()">
                                                            <option value="">Select Zone</option>
                                                            <option value="<% state.id %>" data-ng-repeat="state in states"><% state.name %></option>
                                                        </select>
                                                        @if ($errors->has('zone'))
                                                            <span class="help-block form-error">{{$errors->first('zone')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="w100"></div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">City</label>
                                                        <select name="city" id="city" class="form-control" ng-model="city">
                                                            <option value="">Select City</option>
                                                            <option value="<% city.id %>" data-ng-repeat="city in cities"><% city.name %></option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Gender</label>
                                                        <div class="form-group">
                                                            <div class="radio">
                                                                <label style="margin-right:5px">
                                                                    <input type="radio" value="Male" name="gender" checked="">Male
                                                                </label>
                                                                <label>
                                                                    <input type="radio" value="Female" name="gender">Female
                                                                </label>
                                                            </div>
                                                            </div>
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Address</label>
                                                        <textarea name="address" id="address" cols="30" rows="4"  class="form-control">{{ ($user->id ? $user->userDetail->address: old('address') ) }}</textarea>
                                                    </div>



                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Branch Address</label>
                                                        <textarea name="address_alt" id="address_alt" cols="30" rows="4"  class="form-control">{{ ($user->id ? $user->userDetail->address_alt: old('address_alt') ) }}</textarea>
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                            <label for="name">Profile</label>

                                                            <textarea name="about_me" id="about_me" cols="30" rows="4"  class="form-control">{{ ($user->id ? $user->userDetail->about_me: old('about_me') ) }}</textarea>
                                                        </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Occupation</label>
                                                        <input class="form-control valid" name="occupation" type="text" id="occupation" value="{{ ($user->occupation ? $user->occupation: old('occupation') ) }}">
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Language</label>
                                                        <input class="form-control valid" name="language" type="text" id="language" value="{{ ($user->language ? $user->language: old('language') ) }}">
                                                    </div>
                                                    <div class="w100"></div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Specialities</label>
                                                        <input class="form-control valid" name="specialities" type="text" id="specialities" value="{{ ($user->specialities ? $user->specialities: old('specialities') ) }}">
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name">Website</label>
                                                        <input class="form-control valid" name="website" type="text" id="website" value="{{ ($user->website ? $user->website: old('website') ) }}">
                                                    </div>




                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name" class="pull-left w100">License upload</label>
                                                        <div class="col-md-6"><input type="file" name="license_upload" id="license_upload"></div>
                                                        <div class="col-md-3">
                                                        @isset($user->userDetail->license_upload)
                                                        <a  href="{{ url('/') }}{{$user->userDetail->license_upload}}" data-fancybox style="margin:10px 0">
                                                            <img src="{{ url('/') }}{{$user->userDetail->license_upload}}" width="100px">
                                                        </a>
                                                        @endisset
                                                    </div>
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name" class="pull-left w100">User image upload</label>

                                                        <div class="col-md-6"> <input type="file" name="user_image"></div>
                                                        <div class="col-md-3">
                                                        @if($user->user_image)
                                                        <a  href="{{ url('/') }}{{$user->user_image}}" data-fancybox style="margin:10px 0"><img src="{{ url('/') }}{{$user->user_image}}" width="100px"></a>
                                                        @endif
                                                    </div>
                                                    </div>
                                                    <div class="form-group col-md-6 float-left">
                                                        <label for="name" class="pull-left w100">User Banner  upload</label>

                                                        <div class="col-md-6">
                                                        <input type="file" name="userdetail_banner"></div>
                                                        <div class="col-md-3">
                                                        @isset($user->userDetail->userdetail_banner)
                                                        <a  href="{{ url('/') }}{{$user->userDetail->userdetail_banner}}" data-fancybox style="margin:10px 0">
                                                            <img src="{{ url('/') }}{{$user->userDetail->userdetail_banner}}" width="100px">
                                                        </a>
                                                        @endisset
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-12 float-left">
                                                        <input class="btn btn-primary" type="submit" value="Save">
                                                        <a href="{{route('users.index')}}" class="btn btn-default">Cancel</a>
                                                    </div>

                                                </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
