@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Roles <small>Manage User roles</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('roles.index')}}">Roles</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="card-content">
            <div class="data-list-view-header">
                <div class="card filter">
                    <div class="card-header">
                        <h4 class="card-title">Filters</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                                <li><a href="{{route('roles.index')}}"><i class="feather icon-rotate-cw users-data-filter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="users-list-filter">
                                <form method="GET" action="{{route('roles.index')}}">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Role</label>
                                            <fieldset class="form-group">
                                               <input type="text" class="form-control" value="{{app('request')->input('title')}}" name="title">
                                            </fieldset>
                                        </div>
                                        {{-- <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Status</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" id="users-list-status">
                                                    <option value="">All</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Blocked">Blocked</option>
                                                    <option value="deactivated">Deactivated</option>
                                                </select>
                                            </fieldset>
                                        </div> --}}
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role"></label>
                                        <button class="btn btn-primary search" type="submit"><i class="feather icon-search"></i> Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 noapd">
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route("roles.create") }}"><i class="feather icon-plus"></i> Role</a>
                    </div>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p><i class="icon fa fa-check"></i> {{session('success_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
                    @if(Session::has('fail_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-danger alert-dismissible">

                            <p><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
            <div class="table-responsive mt-1">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table class="table table-hover-animation mb-0">
                    <thead>
                        <tr>
                            <th>Sl.no</th>

                            <th>Role</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Created Date</th>
                            <th>Modified Date</th><th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($roles)
                                    @if($roles->currentpage() == 1)
                                        @php($ctr = 1)
                                    @else
                                        @php($ctr = ($roles->currentpage()-1)*$roles->perpage()+1)

                                   @endif
                                   @foreach($roles as $role)
                                        <tr class="odd">
                                            <td >{{$ctr}}</td>

                                            <td>{{$role->title}} </td>
                                            <td>{{$role->type}} </td>
                                            <td>
                                                @if(isset($role->description))
                                                   {{strip_tags($role->description)}}
                                                @else
                                                   {{'-'}}
                                                @endif
                                            </td>
                                            <td>{{$role->created_at->format('Y-m-d')}}</td>
                                            <td>{{$role->updated_at->format('Y-m-d')}}</td>
                                            <td >

                                                <span style="float: left"><a href="{{route('roles.edit',$role->id)}}" data-toggle="tooltip" title="Edit"><i class="feather icon-edit"></i></a></span>
                                                <span style="float: left">
                                                <form action="{{route('roles.destroy',$role->id) }}" method="post" id="deleteForm{{$role->id}}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {!! csrf_field() !!}
                                                    <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$role->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                        <i class="feather icon-trash"></i></a>

                                                </form>
                                                </span>

                                                @if($role->is_active)
                                                  <span><a href="{{route('roles.statusChange',$role->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                          <span> <i class="feather icon-check" style="color: yellowgreen"></i></span></a></span>
                                                @else
                                                  <span><a href="{{route('roles.statusChange',$role->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                          <span ><i class="feather icon-x" style="color: red"></i></span></a></span>
                                                @endif

                                                <span>
                                                @can('accessPermission','permission')
                                                @if($role->type == 'RIA')
                                                    <a href="{{route('roles.riapermissions',$role->id)}}" data-toggle="tooltip" title="Permission"><i class="feather icon-settings"></i></a>
                                                @else
                                                    {{--<a href="{{route('roles.permissions',$role->id)}}" data-toggle="tooltip" title="Permission"><i class="fa fa-fw  fa-key"></i>Permission</a>--}}
                                                @endif
                                                @endcan
                                                </span>
                                            </td>
                                        </tr>
                                        @php($ctr++)
                                   @endforeach
                               @endif
                    </tbody>
                </table>
                </div>
                @include('admin/partials/pagination',['results'=>$roles])

            </div>
            </div>
        </div>
    </div>
</div>

@endsection
