@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Roles <small>Manage User roles</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('roles.create')}}">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if(isset($role->id)) {{--edit form--}}
                                <form method="POST"
                                    action="{{route('roles.update',$role->id)}}"
                                    name="rolesForm"
                                    id="rolesForm"
                                    class="form form-vertical"
                                    enctype="multipart/form-data">
                                    {{csrf_field()}}  {{ method_field('PUT') }}
                                    @else {{--create form--}}
                                    <form method="POST"
                                        action="{{route('roles.store')}}"
                                        name="rolesForm"
                                        id="rolesForm"
                                        class="form form-vertical"
                                        enctype="multipart/form-data">{{csrf_field()}}
                                        @endif
                                        <div class="form-body">
                                            <div class="row">

                                                <div class="col-12">


                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <input type="text"
                                                            name="title"
                                                            class="form-control"
                                                            required
                                                            value="{{$role->title}}">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Type</label>
                                                        <select name="type"
                                                            id="type"
                                                            class="form-control">
                                                            <option value="">Select</option>
                                                            <option value="RIA"
                                                                @if($role->type == 'RIA') selected="selected" @endif>
                                                                Realopedia
                                                                Internal Account</option>
                                                            <option value="website"
                                                                @if($role->type == 'website') selected="selected"
                                                                @endif>For Website
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea name="description"
                                                            id="description"
                                                            class="form-control"
                                                            cols="10"
                                                            rows="10">{{$role->description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <button type="submit"
                                                        class="btn btn-primary">Save</button>
                                                    <a href="{{route('roles.index')}}"
                                                        class="btn btn-default">Cancel</a></div>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
