@extends('layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0"> Permissions
                        <small>Manage User permissions</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a>Permissions</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive border rounded px-1 ">
                        <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2"><i
                                class="feather icon-lock mr-50 "></i>Manage Permissions for
                            <b>{{ucfirst($permission->role->title)}}</b></h6>


                        @php
                        $accessString = $permission->access;$access = explode(',',$accessString);
                        $listString = $permission->list;$list = explode(',',$listString);
                        $searchString = $permission->search;$search = explode(',',$searchString);
                        $viewString = $permission->view;$view = explode(',',$viewString);
                        $addString = $permission->add;$add = explode(',',$addString);
                        $editString = $permission->edit;$edit = explode(',',$editString);
                        $deleteString = $permission->delete;$delete = explode(',',$deleteString);
                        $statusString = $permission->status;$status = explode(',',$statusString);
                        $exportString = $permission->export;$export = explode(',',$exportString);
                        @endphp
                        <form name="riaPermission" style="padding-bottom: 20px;"
                            method="post"
                            action="{{route('roles.riapermissions.update',$permission->role_id)}}">
                            {{csrf_field()}}
                            <input type="hidden"
                                name="_method"
                                value="PUT" >
                            <table id="permissionTable"
                                class="table table-borderless">

                                {{csrf_field()}}
                                <thead>
                                    <tr>
                                        <th>Modules</th>
                                        <th>Access</th>
                                        <th>View</th>
                                        <th>Add</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        <th>Status</th>
                                        <th>Export</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        <th>Role</th>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox"
                                                    class="custom-control-input"
                                                    value="role"
                                                    id="role_access"
                                                    name="access[]"
                                                    {{in_array('role',$access) ? 'checked' : ''}}>

                                                <label class="custom-control-label"
                                                    for="role_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="role"
                                                    id="role_view"
                                                    name="view[]"
                                                    {{in_array('role',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="role_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="role"
                                                    id="role_add"
                                                    name="add[]"
                                                    {{in_array('role',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="role_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="role"
                                                    id="role_edit"
                                                    name="edit[]"
                                                    {{in_array('role',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="role_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="role"
                                                    id="role_delete"
                                                    name="delete[]"
                                                    {{in_array('role',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="role_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="role"
                                                    id="role_status"
                                                    name="status[]"
                                                    {{in_array('role',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="role_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="role"
                                                    id="role_export"
                                                    name="export[]"
                                                    {{in_array('role',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="role_export"></label>
                                        </td>

                                    </tr>

                                    <tr>
                                        <th>Associations</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="association"
                                                    id="association_access"
                                                    name="access[]"
                                                    {{in_array('association',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="association_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="association"
                                                    id="association_view"
                                                    name="view[]"
                                                    {{in_array('association',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="association_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="association"
                                                    id="association_add"
                                                    name="add[]"
                                                    {{in_array('association',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="association_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="association"
                                                    id="association_edit"
                                                    name="edit[]"
                                                    {{in_array('association',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="association_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="association"
                                                    id="association_delete"
                                                    name="delete[]"
                                                    {{in_array('association',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="association_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="association"
                                                    id="association_status"
                                                    name="status[]"
                                                    {{in_array('association',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="association_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="association"
                                                    id="association_export"
                                                    name="export[]"
                                                    {{in_array('association',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="association_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Users</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="user"
                                                    id="user_access"
                                                    name="access[]"
                                                    {{in_array('user',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="user_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="user"
                                                    id="user_view"
                                                    name="view[]"
                                                    {{in_array('user',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="user_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="user"
                                                    id="user_add"
                                                    name="add[]"
                                                    {{in_array('user',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="user_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="user"
                                                    id="user_edit"
                                                    name="edit[]"
                                                    {{in_array('user',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="user_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="user"
                                                    id="user_delete"
                                                    name="delete[]"
                                                    {{in_array('user',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="user_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="user"
                                                    id="user_status"
                                                    name="status[]"
                                                    {{in_array('user',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="user_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="user"
                                                    id="user_export"
                                                    name="export[]"
                                                    {{in_array('user',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="user_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Location</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="location"
                                                    id="location_access"
                                                    name="access[]"
                                                    {{in_array('location',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="location_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="location"
                                                    id="location_view"
                                                    name="view[]"
                                                    {{in_array('location',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="location_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="location"
                                                    id="location_add"
                                                    name="add[]"
                                                    {{in_array('location',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="location_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="location"
                                                    id="location_edit"
                                                    name="edit[]"
                                                    {{in_array('location',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="location_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="location"
                                                    id="location_delete"
                                                    name="delete[]"
                                                    {{in_array('location',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="location_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="location"
                                                    id="location_status"
                                                    name="status[]"
                                                    {{in_array('location',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="location_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="location"
                                                    id="location_export"
                                                    name="export[]"
                                                    {{in_array('location',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="location_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Product</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="product"
                                                    id="product_access"
                                                    name="access[]"
                                                    {{in_array('product',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="product_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="product"
                                                    id="product_view"
                                                    name="view[]"
                                                    {{in_array('product',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="product_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="product"
                                                    id="product_add"
                                                    name="add[]"
                                                    {{in_array('product',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="product_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="product"
                                                    id="product_edit"
                                                    name="edit[]"
                                                    {{in_array('product',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="product_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="product"
                                                    id="product_delete"
                                                    name="delete[]"
                                                    {{in_array('product',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="product_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="product"
                                                    id="product_status"
                                                    name="status[]"
                                                    {{in_array('product',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="product_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="product"
                                                    id="product_export"
                                                    name="export[]"
                                                    {{in_array('product',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="product_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Inclusions</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="inclusion"
                                                    id="inclusion_access"
                                                    name="access[]"
                                                    {{in_array('inclusion',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="inclusion_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="inclusion"
                                                    id="inclusion_view"
                                                    name="view[]"
                                                    {{in_array('inclusion',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="inclusion_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="inclusion"
                                                    id="inclusion_add"
                                                    name="add[]"
                                                    {{in_array('inclusion',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="inclusion_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="inclusion"
                                                    id="inclusion_edit"
                                                    name="edit[]"
                                                    {{in_array('inclusion',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="inclusion_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="inclusion"
                                                    id="inclusion_delete"
                                                    name="delete[]"
                                                    {{in_array('inclusion',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="inclusion_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="inclusion"
                                                    id="inclusion_status"
                                                    name="status[]"
                                                    {{in_array('inclusion',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="inclusion_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="inclusion"
                                                    id="inclusion_export"
                                                    name="export[]"
                                                    {{in_array('inclusion',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="inclusion_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Packages</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="package"
                                                    id="package_access"
                                                    name="access[]"
                                                    {{in_array('package',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="package_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="package"
                                                    id="package_view"
                                                    name="view[]"
                                                    {{in_array('package',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="package_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="package"
                                                    id="package_add"
                                                    name="add[]"
                                                    {{in_array('package',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="package_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="package"
                                                    id="package_edit"
                                                    name="edit[]"
                                                    {{in_array('package',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="package_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="package"
                                                    id="package_delete"
                                                    name="delete[]"
                                                    {{in_array('package',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="package_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="package"
                                                    id="package_status"
                                                    name="status[]"
                                                    {{in_array('package',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="package_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="package"
                                                    id="package_export"
                                                    name="export[]"
                                                    {{in_array('package',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="package_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Emails</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="email"
                                                    id="email_access"
                                                    name="access[]"
                                                    {{in_array('email',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="email_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="email"
                                                    id="email_view"
                                                    name="view[]"
                                                    {{in_array('email',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="email_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="email"
                                                    id="email_add"
                                                    name="add[]"
                                                    {{in_array('email',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="email_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="email"
                                                    id="email_edit"
                                                    name="edit[]"
                                                    {{in_array('email',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="email_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="email"
                                                    id="email_delete"
                                                    name="delete[]"
                                                    {{in_array('email',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="email_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="email"
                                                    id="email_status"
                                                    name="status[]"
                                                    {{in_array('email',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="email_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="email"
                                                    id="email_export"
                                                    name="export[]"
                                                    {{in_array('email',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="email_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>CMS</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms"
                                                    id="cms_access"
                                                    name="access[]"
                                                    {{in_array('cms',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms"
                                                    id="cms_view"
                                                    name="view[]"
                                                    {{in_array('cms',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms"
                                                    id="cms_add"
                                                    name="add[]"
                                                    {{in_array('cms',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms"
                                                    id="cms_edit"
                                                    name="edit[]"
                                                    {{in_array('cms',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms"
                                                    id="cms_delete"
                                                    name="delete[]"
                                                    {{in_array('cms',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms"
                                                    id="cms_status"
                                                    name="status[]"
                                                    {{in_array('cms',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms"
                                                    id="cms_export"
                                                    name="export[]"
                                                    {{in_array('cms',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_export"></label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th>CMS Category</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms_category"
                                                    id="cms_category_access"
                                                    name="access[]"
                                                    {{in_array('cms_category',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_category_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms_category"
                                                    id="cms_category_view"
                                                    name="view[]"
                                                    {{in_array('cms_category',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_category_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms_category"
                                                    id="cms_category_add"
                                                    name="add[]"
                                                    {{in_array('cms_category',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_category_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms_category"
                                                    id="cms_category_edit"
                                                    name="edit[]"
                                                    {{in_array('cms_category',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_category_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms_category"
                                                    id="cms_category_delete"
                                                    name="delete[]"
                                                    {{in_array('cms_category',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_category_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms_category"
                                                    id="cms_category_status"
                                                    name="status[]"
                                                    {{in_array('cms_category',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_category_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="cms_category"
                                                    id="cms_category_export"
                                                    name="export[]"
                                                    {{in_array('cms_category',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="cms_category_export"></label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th>SEO Management</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="seo"
                                                    id="seo_access"
                                                    name="access[]"
                                                    {{in_array('seo',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="seo_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="seo"
                                                    id="seo_view"
                                                    name="view[]"
                                                    {{in_array('seo',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="seo_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="seo"
                                                    id="seo_add"
                                                    name="add[]"
                                                    {{in_array('seo',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="seo_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="seo"
                                                    id="seo_edit"
                                                    name="edit[]"
                                                    {{in_array('seo',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="seo_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="seo"
                                                    id="seo_delete"
                                                    name="delete[]"
                                                    {{in_array('seo',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="seo_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="seo"
                                                    id="seo_status"
                                                    name="status[]"
                                                    {{in_array('seo',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="seo_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="seo"
                                                    id="seo_export"
                                                    name="export[]"
                                                    {{in_array('seo',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="seo_export"></label>
                                        </td>

                                    </tr>
                                    <th>Property Feild</th>
                                    <td>
                                        <div class="custom-control custom-checkbox"><input type="checkbox"
                                                class="custom-control-input"
                                                value="propertyfeild"
                                                id="propertyfeild_access"
                                                name="access[]"
                                                {{in_array('propertyfeild',$access) ? 'checked' : ''}}> <label
                                                class="custom-control-label"
                                                for="propertyfeild_access"></label>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox"><input type="checkbox"
                                                class="custom-control-input"
                                                value="propertyfeild"
                                                id="propertyfeild_view"
                                                name="view[]"
                                                {{in_array('propertyfeild',$view) ? 'checked' : ''}}> <label
                                                class="custom-control-label"
                                                for="propertyfeild_view"></label>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox"><input type="checkbox"
                                                class="custom-control-input"
                                                value="propertyfeild"
                                                id="propertyfeild_add"
                                                name="add[]"
                                                {{in_array('propertyfeild',$add) ? 'checked' : ''}}> <label
                                                class="custom-control-label"
                                                for="propertyfeild_add"></label>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox"><input type="checkbox"
                                                class="custom-control-input"
                                                value="propertyfeild"
                                                id="propertyfeild_edit"
                                                name="edit[]"
                                                {{in_array('propertyfeild',$edit) ? 'checked' : ''}}> <label
                                                class="custom-control-label"
                                                for="propertyfeild_edit"></label>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox"><input type="checkbox"
                                                class="custom-control-input"
                                                value="propertyfeild"
                                                id="propertyfeild_delete"
                                                name="delete[]"
                                                {{in_array('propertyfeild',$delete) ? 'checked' : ''}}> <label
                                                class="custom-control-label"
                                                for="propertyfeild_delete"></label>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox"><input type="checkbox"
                                                class="custom-control-input"
                                                value="propertyfeild"
                                                id="propertyfeild_status"
                                                name="status[]"
                                                {{in_array('propertyfeild',$status) ? 'checked' : ''}}> <label
                                                class="custom-control-label"
                                                for="propertyfeild_status"></label>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox"><input type="checkbox"
                                                class="custom-control-input"
                                                value="propertyfeild"
                                                id="propertyfeild_export"
                                                name="export[]"
                                                {{in_array('propertyfeild',$export) ? 'checked' : ''}}> <label
                                                class="custom-control-label"
                                                for="propertyfeild_export"></label>
                                    </td>
                                    </tr>
                                    <tr>
                                        <th>Property Feild List</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="propertyfeildlist"
                                                    id="propertyfeildlist_access"
                                                    name="access[]"
                                                    {{in_array('propertyfeildlist',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="propertyfeildlist_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="propertyfeildlist"
                                                    id="propertyfeildlist_view"
                                                    name="view[]"
                                                    {{in_array('propertyfeildlist',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="propertyfeildlist_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="propertyfeildlist"
                                                    id="propertyfeildlist_add"
                                                    name="add[]"
                                                    {{in_array('propertyfeildlist',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="propertyfeildlist_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="propertyfeildlist"
                                                    id="propertyfeildlist_edit"
                                                    name="edit[]"
                                                    {{in_array('propertyfeildlist',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="propertyfeildlist_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="propertyfeildlist"
                                                    id="propertyfeildlist_delete"
                                                    name="delete[]"
                                                    {{in_array('propertyfeildlist',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="propertyfeildlist_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="propertyfeildlist"
                                                    id="propertyfeildlist_status"
                                                    name="status[]"
                                                    {{in_array('propertyfeildlist',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="propertyfeildlist_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="propertyfeildlist"
                                                    id="propertyfeildlist_export"
                                                    name="export[]"
                                                    {{in_array('propertyfeildlist',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="propertyfeildlist_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Setting</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="setting"
                                                    id="setting_access"
                                                    name="access[]"
                                                    {{in_array('setting',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="setting_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="setting"
                                                    id="setting_view"
                                                    name="view[]"
                                                    {{in_array('setting',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="setting_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="setting"
                                                    id="setting_add"
                                                    name="add[]"
                                                    {{in_array('setting',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="setting_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="setting"
                                                    id="setting_edit"
                                                    name="edit[]"
                                                    {{in_array('setting',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="setting_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="setting"
                                                    id="setting_delete"
                                                    name="delete[]"
                                                    {{in_array('setting',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="setting_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="setting"
                                                    id="setting_status"
                                                    name="status[]"
                                                    {{in_array('setting',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="setting_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="setting"
                                                    id="setting_export"
                                                    name="export[]"
                                                    {{in_array('setting',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="setting_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Property</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="property"
                                                    id="property_access"
                                                    name="access[]"
                                                    {{in_array('property',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="property_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="property"
                                                    id="property_view"
                                                    name="view[]"
                                                    {{in_array('property',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="property_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="property"
                                                    id="property_add"
                                                    name="add[]"
                                                    {{in_array('property',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="property_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="property"
                                                    id="property_edit"
                                                    name="edit[]"
                                                    {{in_array('property',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="property_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="property"
                                                    id="property_delete"
                                                    name="delete[]"
                                                    {{in_array('property',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="property_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="property"
                                                    id="property_status"
                                                    name="status[]"
                                                    {{in_array('property',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="property_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="property"
                                                    id="property_export"
                                                    name="export[]"
                                                    {{in_array('property',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="property_export"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Project</th>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="project"
                                                    id="project_access"
                                                    name="access[]"
                                                    {{in_array('project',$access) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="project_access"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="project"
                                                    id="project_view"
                                                    name="view[]"
                                                    {{in_array('project',$view) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="project_view"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="project"
                                                    id="project_add"
                                                    name="add[]"
                                                    {{in_array('project',$add) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="project_add"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="project"
                                                    id="project_edit"
                                                    name="edit[]"
                                                    {{in_array('project',$edit) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="project_edit"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="project"
                                                    id="project_delete"
                                                    name="delete[]"
                                                    {{in_array('project',$delete) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="project_delete"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="project"
                                                    id="project_status"
                                                    name="status[]"
                                                    {{in_array('project',$status) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="project_status"></label>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input"
                                                    value="project"
                                                    id="project_export"
                                                    name="export[]"
                                                    {{in_array('project',$export) ? 'checked' : ''}}> <label
                                                    class="custom-control-label"
                                                    for="project_export"></label>
                                        </td>
                                    </tr>
                                    {{--<tr>
                                    <th>Login History</th>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                </tr>
                                <tr>
                                    <th>Location</th>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                </tr>
                                <tr>
                                    <th>Products</th>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                </tr>
                                <tr>
                                    <th>Product Inclusions</th>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                </tr>
                                <tr>
                                    <th>User Packages</th>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                    <td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label" for=""></label></td>
                                </tr>--}}

                                </tbody>

                            </table>
                            <input class="btn btn-primary"
                                value="Save"
                                type="submit">
                            <a href="{{route('roles.index')}}"
                                class="btn btn-default">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop
