@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                SEO
                <small>Manage SEO</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">SEO</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                                {!!  Form::open(['method' => 'GET','action' => 'Admin\AdminSeoManagement@index' ]) !!}
                                <div class="col-xs-3 form-group">
                                    {!! Form::text('page',null,['class'=>'form-control','placeholder' => 'Page']) !!}
                                </div>
                                <div class="col-xs-3 form-group">
                                    {!! Form::text('meta_keywords',null,['class'=>'form-control','placeholder' => 'Keyword']) !!}
                                </div>
                                <div class="col-xs-3 form-group">
                                    {!! Form::text('description',null,['class'=>'form-control','placeholder' => 'Description']) !!}
                                </div>
                                <div class="col-xs-3 form-group mininise">
                                 
                                <select  name="status" class="form-control">
                                    <option value=''>Status</option>
                                    <option value="1" @if(app('request')->input('status') == 1) selected @endif>Active</option>
                                    <option value="2" @if(app('request')->input('status') == 2) selected @endif>Inactive</option>
                                </select>
                                </div>
 
                                <div class="col-xs-3 form-group">
                                    {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                                    {!! Form::reset('Reset', ['class' => 'btn btn-default reset-form']) !!}
                                    <button type="button" class="btn btn-info dropdown-toggle" id="more_filter"><span class="caret"></span></button>
                                    <button type="button" class="btn btn-info dropdown-toggle" id="showless"><span class="caret"></span></button>
                                    </div>
                                {!! Form::close() !!}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" onclick="window.location='{{ route("seo.create") }}'">+ SEO</button>
                                </div>
                            </div>
                            @if(Session::has('success_msg'))
                                <div class="col-md-12">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> {{session('success_msg')}}</h4>
                                </div>
                                </div>
                            @endif
                            @if(Session::has('fail_msg'))
                                <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</h4>
                                </div>
                                </div>
                            @endif
                            <table id="countriesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th>Action</th>
                                    <th>Page</th>
                                    <th>Keyword</th>
                                    <th>Description</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($seos)
                                    @if($seos->currentpage() == 1)
                                        @php($ctr = 1)
                                    @else
                                        @php($ctr = ($seos->currentpage()-1)*$seos->perpage()+1)

                                    @endif
                                    @if($seos->count() > 0)
                                    @foreach($seos as $seo)
                                        <tr>
                                            <td >{{$ctr}}</td>
                                            <td>
                                                <span style="float: left"><a href="{{route('seo.edit',$seo->id)}}" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i>Edit</a></span>
                                                <span style="float: left"><a href="{{route('seo.show',$seo->id)}}" data-toggle="tooltip" title="View"><i class="fa fa-fw fa-eye"></i>View</a></span>
                                                <span style="float: left">
                                                <form action="{{route('seo.destroy',$seo->id) }}" method="post" id="deleteForm{{$seo->id}}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {!! csrf_field() !!}
                                                    <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$seo->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                        <i class="fa fa-fw  fa-trash"></i>Delete</a>

                                                </form>
                                                </span>

                                                @if($seo->is_active)
                                                    <span><a href="{{route('seo.statusChange',$seo->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                            <span style="color: yellowgreen"><i class="fa fa-fw fa-check"></i></span>Active</a></span>
                                                @else
                                                    <span ><a href="{{route('seo.statusChange',$seo->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                            <span style="color: red"><i class="fa fa-fw fa-times"></i></span>Inactive</a></span>
                                                @endif

                                            </td>
                                            <td>{{$seo->page}} </td>
                                            <td>{{$seo->meta_keywords}} </td>
                                            <td>{{$seo->meta_description}} </td>
                                            <td>{{$seo->created_at->format('Y-m-d')}}</td>
                                            <td>{{$seo->updated_at->format('Y-m-d')}}</td>
                                        </tr>
                                        @php($ctr++)
                                    @endforeach
                                    @else
                                        <tr><td colspan="10" style="text-align: center">No records available</td></tr>
                                     @endif
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th>Action</th>
                                    <th>Page</th>
                                    <th>Keyword</th>
                                    <th>Description</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </tfoot>
                            </table>

                            {{--pagination--}}

                            @include('admin/partials/pagination',['results'=>$seos])
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

@stop