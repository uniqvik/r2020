@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                SEO
                <small>Manage SEO</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('seo.index')}}">SEO</a></li>
                <li class="active">Add/Edit</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Page :</label><span>{{$seo->page}}</span>
                    </div>
                  
                    
                    <div class="form-group">
                        <label>Meta Keywords :</label><span>{{$seo->meta_keywords}}</span>
                    </div>
                    <div class="form-group">
                        <label>Meta Description :</label>
                        <span>{{$seo->meta_description}}</span>
                    </div>
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop