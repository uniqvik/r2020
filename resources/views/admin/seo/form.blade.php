@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                SEO
                <small>Manage SEO</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('seo.index')}}">SEO</a></li>
                <li class="active">Add/Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add/Edit SEO</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if(isset($seo->id)) {{--edit form--}}
                            {!! Form::model($seo,['method' => 'PATCH','action' => ['Admin\AdminSeoManagement@update',$seo->id],
                            'files'=>true,'name' => 'countryForm','id' => 'countryForm']) !!}
                            {{csrf_field()}}
                            @else {{--create form--}}
                            {!!  Form::open(['method' => 'POST','action' => 'Admin\AdminSeoManagement@store','name' => 'countryForm','id' => 'countryForm','files'=>true]) !!}
                            @endif
                            <div class="form-group">
                                {!! Form::label('page','Page') !!}
                                {!! Form::text('page',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                    {!! Form::label('type','Type') !!} 
                                    <div class="radio">
                                            <label style="margin-right:5px">
                                                <input type="radio" data-validation="required" value="static" name="type"  @if(isset($seo->type) == 'static')checked=""@endif> 
                                                <span style="color: #000000;">Static</span>
                                            </label>
                                            <label>
                                                <input type="radio" data-validation="required" value="dynamic" name="type" @if(isset($seo->type) == 'dynamic')checked=""@endif>
                                                <span style="color: #000000;">Dynamic</span>
                                            </label>
                                    </div>
                                
                            </div>
                            <div class="form-group" id="note" style="display:none;margin-bottom:10px">Dynamic Rule: country/state/city/name/address</div>
                            <div class="form-group">
                                {!! Form::label('meta_keywords','Meta Keywords') !!}
                                {!! Form::textarea('meta_keywords',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('meta_description','Meta Description') !!}
                                {!! Form::textarea('meta_description',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>
                            {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                            <a href="{{route('seo.index')}}" class="btn btn-default">Cancel</a>
                            {!! Form::close() !!}

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
               // modules : 'location,file'
            });
        });
        $(function () {
            //CKEDITOR.replace('description');
        })
        $('input[type=radio][name=type]').change(function() {
            if (this.value == 'static') {
                $('#note').hide();
            }
            else if (this.value == 'dynamic') {
                $('#note').show();
            }
        });
    </script>
@stop