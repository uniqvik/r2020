@extends('layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">  Associations/Groups
                        <small>Manage User Associations/Groups</small></h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('associations.index')}}">Roles</a></li>
                            <li class="breadcrumb-item active"><a href="">Add/Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-horizontal-layouts">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if(isset($association->id)) {{--edit form--}}
                                <form method="POST"
                                action="{{route('associations.update',$association->id)}}"
                                name="associationsForm"
                                id="associationsForm"
                                class="form form-vertical"
                                enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PUT') }}
                                @else
                                <form method="POST"
                                            action="{{route('associations.store')}}"
                                            name="associationsForm"
                                            id="associationsForm"
                                            class="form form-vertical"
                                            enctype="multipart/form-data">  {{csrf_field()}}
                                @endif
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" value="{{$association->title}}" class="form-control" data-validation="required">

                                </div>
                                <div class="form-group">

                                        <label>Code</label>
                                        <input type="text" name="code" value="{{$association->code}}" class="form-control" data-validation="required">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" id="description" cols="20" rows="5" class="form-control">{{$association->description}}</textarea>

                                </div>

                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('associations.index')}}" class="btn btn-default">Cancel</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
