@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting 
                <small>Manage Keys & Code</small>
            </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{route('setting.index')}}">Keys & Code</a></li>
                    <li class="active">View</li>
                </ol>
            </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Title :</label><span>{{$setting->title}}</span>
                    </div>
                  
                    <div class="form-group">
                            <label>Type :</label><span>{{$setting->type}}</span>
                        </div>
                    <div class="form-group">
                        <label>Content :</label><span>{{$setting->value}}</span>
                    </div>
                    
                   
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop