@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting 
                <small>Manage Keys & Code</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('setting.index')}}">Keys & Code</a></li>
                <li class="active">Add/Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add/Edit Keys & Code</h3>
                        </div>
                        <!-- /.box-header -->
                        
                        <div class="box-body">
                            @if(isset($setting->id)) {{--edit form--}}
                            {!! Form::model($setting,['method' => 'PATCH','action' => ['Admin\AdminSettings@update',$setting->id],
                            'files'=>true,'name' => 'countryForm','id' => 'countryForm']) !!}
                            {{csrf_field()}}
                            @else {{--create form--}}
                            {!!  Form::open(['method' => 'POST','action' => 'Admin\AdminSettings@store','name' => 'countryForm','id' => 'countryForm','files'=>true]) !!}
                            {{csrf_field()}}
                            @endif
                            
                            
                            <div class="form-group col-md-6"> 
                                    {!! Form::label('title','Title') !!}
                                    {!! Form::text('title',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name">Type</label>
                                <div class="form-group">
                                    <div class="radio">
                                        <label style="margin-right:5px">
                                            <input type="radio" value="code" name="type" data-validation="required" @if($setting->type == 'code')checked="" @endif>Code
                                        </label>
                                        <label>
                                            <input type="radio" value="key" name="type" data-validation="required" @if($setting->type == 'key')checked="" @endif>Key
                                        </label>
                                    </div>
                                    </div>
                            </div>
                            <div class="w100"></div>
                            <div class="form-group col-md-12">
                                    {!! Form::label('value','Key Or Code') !!}
                                    {!! Form::textarea('value',null,['class'=>'form-control','data-validation' => 'required']) !!}
                                </div>

                           
                            
                            <div class="form-group col-md-12">
                            {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                            <a href="{{route('setting.index')}}" class="btn btn-default">Cancel</a>
                            {!! Form::close() !!}
                        </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
               // modules : 'location,file'
            });
        });
        $(function () {
            //CKEDITOR.replace('description');
        })
        $("#add_category").click(function(){
            var cat = $('#cattitle').val();
            var CSRF_TOKEN = $('input[name="_token"]').val();
            if(cat == ''){return false;}
            $.ajax(
            {
                url: '{{route('storeCategoryFromAjax')}}',
                type: 'post',
                dataType: 'json',
                data: {_token: CSRF_TOKEN, title:cat},
                beforeSend: function() {$('#add_category').attr('disabled','disabled');},
                complete: function() {},
                success: function(json)
                {
                    $('#add_category').removeAttr('disabled');
                    console.log(json);
                    if(json['status'] == 'success'){
                        $('#cattitle').val('');
                        $("#category_id").append(new Option(json['title'], json['id']));
                        $("#category_id").val(json['id']);
                        $('#category_modal').modal('hide');

                    }
                }
            });
        });
    </script>
@stop