@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Manage Keys & Code</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                                {!!  Form::open(['method' => 'GET','action' => 'Admin\AdminSettings@index' ]) !!}
                                <div class="col-xs-3 form-group">
                                    {!! Form::text('title',null,['class'=>'form-control','placeholder' => 'Title']) !!}
                                </div>

                                <div class="col-xs-3 form-group">
                                    <select name="type" class="form-control">
                                        <option value="">Type</option>
                                        <option value="key" @if(app('request')->input('type') == 'key') selected @endif>Key</option>
                                        <option value="code" @if(app('request')->input('type') == 'code') selected @endif>Code</option>
                                    </select>
                                </div>
                                <div class="col-xs-3 form-group">
                                 
                                <select  name="status" class="form-control">
                                    <option value=''>Status</option>
                                    <option value="1" @if(app('request')->input('status') == 1) selected @endif>Active</option>
                                    <option value="2" @if(app('request')->input('status') == 2) selected @endif>Inactive</option>
                                </select>
                                </div>

                                {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                                {!! Form::reset('Reset', ['class' => 'btn btn-default reset-form']) !!}
                                {!! Form::close() !!}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" onclick="window.location='{{ route("setting.create") }}'">+ Keys & Code</button>
                                </div>
                            </div>
                            @if(Session::has('success_msg'))
                                <div class="col-md-12">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> {{session('success_msg')}}</h4>
                                </div>
                                </div>
                            @endif
                            @if(Session::has('fail_msg'))
                                <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</h4>
                                </div>
                                </div>
                            @endif
                            <table id="countriesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th>Action</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($settings)
                                    @if($settings->currentpage() == 1)
                                        @php($ctr = 1)
                                    @else
                                        @php($ctr = ($settings->currentpage()-1)*$categories->perpage()+1)

                                    @endif
                                    @if($settings->count() > 0)
                                    @foreach($settings as $setting)
                                        <tr>
                                            <td >{{$ctr}}</td>
                                            <td>
                                                <span style="float: left"><a href="{{route('setting.edit',$setting->id)}}" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i>Edit</a></span>
                                                <span style="float: left"><a href="{{route('setting.show',$setting->id)}}" data-toggle="tooltip" title="View"><i class="fa fa-fw fa-eye"></i>View</a></span>
                                                <span style="float: left">
                                                <form action="{{route('setting.destroy',$setting->id) }}" method="post" id="deleteForm{{$setting->id}}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {!! csrf_field() !!}
                                                    <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$setting->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                        <i class="fa fa-fw  fa-trash"></i>Delete</a>

                                                </form>
                                                </span>

                                                @if($setting->is_active)
                                                    <span><a href="{{route('setting.statusChange',$setting->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                            <span style="color: yellowgreen"><i class="fa fa-fw fa-check"></i></span>Active</a></span>
                                                @else
                                                    <span ><a href="{{route('setting.statusChange',$setting->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                            <span style="color: red"><i class="fa fa-fw fa-times"></i></span>Inactive</a></span>
                                                @endif

                                            </td>
                                            <td>{{$setting->title}} </td>
                                            <td>{{$setting->type}} </td>
                                            
                                            <td>{{$setting->created_at->format('Y-m-d')}}</td>
                                            <td>{{$setting->updated_at->format('Y-m-d')}}</td>
                                        </tr>
                                        @php($ctr++)
                                    @endforeach
                                    @else
                                        <tr><td colspan="10" style="text-align: center">No records available</td></tr>
                                     @endif
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th>Action</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </tfoot>
                            </table> 

                            {{--pagination--}}

                            @include('admin/partials/pagination',['results'=>$settings])
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

@stop