@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
                <h1>
                    CMS
                    <small>Manage CMS</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{route('cms.index')}}">CMS</a></li>
                    <li class="active">View</li>
                </ol>
            </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Title :</label><span>{{$cms->title}}</span>
                    </div>
                  
                    <div class="form-group">
                            <label>Category :</label><span>{{$cms->cmsCat->title}}</span>
                        </div>
                    <div class="form-group">
                        <label>Content :</label><span>{{$cms->content}}</span>
                    </div>
                    <div class="form-group">
                            <label>Summary :</label><span>{{$cms->summary}}</span>
                        </div>
                    <div class="form-group">
                        <label>Image :</label>
                        @if($cms->image)
                        <span class="w100"> 
                            <a href="{{ url('/') }}{{$cms->image}}" data-fancybox>
                                <img src="{{ url('/') }}{{$cms->image}}" style="width:100px">
                            </a>
                        </span>
                        @endif
                    </div>
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop