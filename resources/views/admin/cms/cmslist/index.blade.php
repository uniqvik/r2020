@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Cms
                <small>Manage Cms</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Cms</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                                {!!  Form::open(['method' => 'GET','action' => 'Admin\AdminCms@index' ]) !!}
                                <div class="col-xs-3 form-group">
                                    {!! Form::text('title',null,['class'=>'form-control','placeholder' => 'Title']) !!}
                                </div>

                                <div class="col-xs-3 form-group">
                                    <select name="category_id" class="form-control">
                                        <option value="">Category</option>
                                        @foreach ($categories as $cms)
                                        <option value="{{$cms->id}}" @if(app('request')->input('category_id') == $cms->id) selected @endif>{{$cms->title}}</option>    
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-3 form-group">
                                 
                                <select  name="status" class="form-control">
                                    <option value=''>Status</option>
                                    <option value="1" @if(app('request')->input('status') == 1) selected @endif>Active</option>
                                    <option value="2" @if(app('request')->input('status') == 2) selected @endif>Inactive</option>
                                </select>
                                </div>

                                {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                                {!! Form::reset('Reset', ['class' => 'btn btn-default reset-form']) !!}
                                {!! Form::close() !!}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" onclick="window.location='{{ route("cms.create") }}'">+ CMS List</button>
                                </div>
                            </div>
                            @if(Session::has('success_msg'))
                                <div class="col-md-12">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> {{session('success_msg')}}</h4>
                                </div>
                                </div>
                            @endif
                            @if(Session::has('fail_msg'))
                                <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</h4>
                                </div>
                                </div>
                            @endif
                            <table id="countriesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th>Action</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($cmslist)
                                    @if($cmslist->currentpage() == 1)
                                        @php($ctr = 1)
                                    @else
                                        @php($ctr = ($cmslist->currentpage()-1)*$categories->perpage()+1)

                                    @endif
                                    @if($cmslist->count() > 0)
                                    @foreach($cmslist as $cms)
                                        <tr>
                                            <td >{{$ctr}}</td>
                                            <td>
                                                <span style="float: left"><a href="{{route('cms.edit',$cms->id)}}" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i>Edit</a></span>
                                                <span style="float: left"><a href="{{route('cms.show',$cms->id)}}" data-toggle="tooltip" title="View"><i class="fa fa-fw fa-eye"></i>View</a></span>
                                                <span style="float: left">
                                                <form action="{{route('cms.destroy',$cms->id) }}" method="post" id="deleteForm{{$cms->id}}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {!! csrf_field() !!}
                                                    <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$cms->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                        <i class="fa fa-fw  fa-trash"></i>Delete</a>

                                                </form>
                                                </span>

                                                @if($cms->is_active)
                                                    <span><a href="{{route('cms.statusChange',$cms->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                            <span style="color: yellowgreen"><i class="fa fa-fw fa-check"></i></span>Active</a></span>
                                                @else
                                                    <span ><a href="{{route('cms.statusChange',$cms->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                            <span style="color: red"><i class="fa fa-fw fa-times"></i></span>Inactive</a></span>
                                                @endif

                                            </td>
                                            <td>{{$cms->title}} </td>
                                            <td>{{$cms->cmsCat->title}} </td>
                                            
                                            <td>{{$cms->created_at->format('Y-m-d')}}</td>
                                            <td>{{$cms->updated_at->format('Y-m-d')}}</td>
                                        </tr>
                                        @php($ctr++)
                                    @endforeach
                                    @else
                                        <tr><td colspan="10" style="text-align: center">No records available</td></tr>
                                     @endif
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th style="width:2%">Sl.no</th>
                                    <th>Action</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Created Date</th>
                                    <th>Modified Date</th>
                                </tr>
                                </tfoot>
                            </table>

                            {{--pagination--}}

                            @include('admin/partials/pagination',['results'=>$cmslist])
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

@stop