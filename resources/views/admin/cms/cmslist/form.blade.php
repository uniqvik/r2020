@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                CMS
                <small>Manage CMS</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('cms.index')}}">CMS</a></li>
                <li class="active">Add/Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add/Edit CMS</h3>
                        </div>
                        <!-- /.box-header -->
                        <div id="category_modal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Add CMS Category</h4>
                                    </div>
                                    <div class="modal-body">
                                      <form action="" method="POST" id="cmscategory">
                                          <div class="form-group">
                                                {!! Form::label('category','CMS Category Title') !!}
                                                <input type="text" id="cattitle" name="title" class="form-control valid" placeholder="Title" data-validation="required">
                                          </div>
                                      </form>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-primary" id="add_category">Add</button>
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                              
                                </div>
                              </div>
                        <div class="box-body">
                            @if(isset($cms->id)) {{--edit form--}}
                            {!! Form::model($cms,['method' => 'PATCH','action' => ['Admin\AdminCms@update',$cms->id],
                            'files'=>true,'name' => 'countryForm','id' => 'countryForm']) !!}
                            {{csrf_field()}}
                            @else {{--create form--}}
                            {!!  Form::open(['method' => 'POST','action' => 'Admin\AdminCms@store','name' => 'countryForm','id' => 'countryForm','files'=>true]) !!}
                            @endif
                            
                            <div class="form-group col-md-4">
                                    {!! Form::label('category_id','CMS Category') !!}
                                    @if(isset($cms->id))
                                        {!! Form::select('category_id', (['' => 'Select a Category'] + $categories),  $cms->category_id,['class' => 'form-control cms-cat','data-validation' => 'required']);!!}
                                    @else
                                        {!! Form::select('category_id', (['' => 'Select a Category'] + $categories),  null,['class' => 'form-control','data-validation' => 'required']);!!}
                                    @endif
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-md-2">
                                    <label style="height:20px;float:left;width:100%"></label>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#category_modal">+ CMS Category</button>
                            </div>
                            <div class="form-group col-md-6">
                                    {!! Form::label('title','Title') !!}
                                    {!! Form::text('title',null,['class'=>'form-control','data-validation' => 'required']) !!}
                                </div>
                            <div class="w100"></div>
                            <div class="form-group col-md-12">
                                    {!! Form::label('content','Content') !!}
                                    {!! Form::textarea('content',null,['class'=>'form-control','data-validation' => 'required']) !!}
                                </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('summary','Summary') !!}
                                {!! Form::textarea('summary',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>
                            <div class="form-group col-md-12">
                                    {!! Form::label('image','Image') !!}
                                    {!! Form::file('image',null,['class'=>'form-control','data-validation'=>'mime size',
                                    'data-validation-allowing'=>'jpg,png,gif,jpeg','data-validation-max-size'=>'512kb',
                                    'data-validation-error-msg-size'=>'You can not upload images larger than 512kb',
                                    'data-validation-error-msg-mime'=>'You can only upload images']) !!}

                                    @if(isset($cms->image))
                                        <a href="{{ url('/') }}{{$cms->image}}" data-fancybox>
                                            <img src="{{ url('/') }}{{$cms->image}}" style="max-width:200px;">
                                        </a>
                                    @endif
                            </div>
                            <div class="form-group col-md-12">
                            {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                            <a href="{{route('cms.index')}}" class="btn btn-default">Cancel</a>
                            {!! Form::close() !!}
                        </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
               // modules : 'location,file'
            });
        });
        $(function () {
            //CKEDITOR.replace('description');
        })
        $("#add_category").click(function(){
            var cat = $('#cattitle').val();
            var CSRF_TOKEN = $('input[name="_token"]').val();
            if(cat == ''){return false;}
            $.ajax(
            {
                url: '{{route('storeCategoryFromAjax')}}',
                type: 'post',
                dataType: 'json',
                data: {_token: CSRF_TOKEN, title:cat},
                beforeSend: function() {$('#add_category').attr('disabled','disabled');},
                complete: function() {},
                success: function(json)
                {
                    $('#add_category').removeAttr('disabled');
                    console.log(json);
                    if(json['status'] == 'success'){
                        $('#cattitle').val('');
                        $("#category_id").append(new Option(json['title'], json['id']));
                        $("#category_id").val(json['id']);
                        $('#category_modal').modal('hide');

                    }
                }
            });
        });
    </script>
@stop