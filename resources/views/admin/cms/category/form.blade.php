@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                CMS Categories
                <small>Manage CMS Categories</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('cmscategories.index')}}">CMS Categories</a></li>
                <li class="active">Add/Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add/Edit CMS Category</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if(isset($category->id)) {{--edit form--}}
                            {!! Form::model($category,['method' => 'PATCH','action' => ['Admin\AdminCmsCategories@update',$category->id],
                            'files'=>true,'name' => 'countryForm','id' => 'countryForm']) !!}
                            {{csrf_field()}}
                            @else {{--create form--}}
                            {!!  Form::open(['method' => 'POST','action' => 'Admin\AdminCmsCategories@store','name' => 'countryForm','id' => 'countryForm','files'=>true]) !!}
                            @endif
                            <div class="form-group">
                                {!! Form::label('title','Title') !!}
                                {!! Form::text('title',null,['class'=>'form-control','data-validation' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('description','Description') !!}
                                {!! Form::textarea('description',null,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                    {!! Form::label('image','Image') !!}
                                    {!! Form::file('image',null,['class'=>'form-control','data-validation'=>'mime size',
                                    'data-validation-allowing'=>'jpg,png,gif,jpeg','data-validation-max-size'=>'512kb',
                                    'data-validation-error-msg-size'=>'You can not upload images larger than 512kb',
                                    'data-validation-error-msg-mime'=>'You can only upload images']) !!}

                                    @if(isset($category->image))
                                        <a href="{{ url('/') }}{{$category->image}}" data-fancybox>
                                            <img src="{{ url('/') }}{{$category->image}}" style="max-width:200px;">
                                        </a>
                                    @endif
                            </div>
                            {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                            <a href="{{route('cmscategories.index')}}" class="btn btn-default">Cancel</a>
                            {!! Form::close() !!}

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
               // modules : 'location,file'
            });
        });
        $(function () {
            CKEDITOR.replace('description');
        })
    </script>
@stop