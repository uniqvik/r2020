@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                CMS Categories
                <small>Manage CMS Categories</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('cmscategories.index')}}">CMS Categories</a></li>
                <li class="active">View</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Title :</label><span>{{$category->title}}</span>
                    </div>
                  
                    
                    <div class="form-group">
                        <label>Description :</label><span>{{$category->description}}</span>
                    </div>
                    <div class="form-group">
                        <label>Image :</label>
                        @if($category->image)
                        <span class="w100"> 
                            <a href="{{ url('/') }}{{$category->image}}" data-fancybox>
                                <img src="{{ url('/') }}{{$category->image}}" style="width:100px">
                            </a>
                        </span>
                        @endif
                    </div>
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop