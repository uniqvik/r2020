@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Inclusions
                        <small>Manage Inclusions</small>
                        </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="">Inclusions</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="card-content">
            <div class="data-list-view-header">
                <div class="card filter">
                    <div class="card-header">
                        <h4 class="card-title">Filters</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                                <li><a href="{{route('inclusions.index')}}"><i class="feather icon-rotate-cw users-data-filter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="users-list-filter">
                                <form method="GET" action="{{route('inclusions.index')}}">
                                    <div class="row">

                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Email Subject</label>
                                            <fieldset class="form-group">
                                               <input type="text" class="form-control" value="{{app('request')->input('subject')}}" name="subject">
                                            </fieldset>
                                        </div>
                                        {{-- <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Status</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" id="users-list-status">
                                                    <option value="">All</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Blocked">Blocked</option>
                                                    <option value="deactivated">Deactivated</option>
                                                </select>
                                            </fieldset>
                                        </div> --}}
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role"></label>
                                        <button class="btn btn-primary search" type="submit"><i class="feather icon-search"></i> Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 noapd">
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route("inclusions.create") }}"><i class="feather icon-plus"></i> Email Template</a>
                    </div>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p><i class="icon fa fa-check"></i> {{session('success_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
                    @if(Session::has('fail_msg'))
                        <div class="col-md-12 noapd">
                        <div class="alert alert-danger alert-dismissible">

                            <p><i class="icon fa fa-ban"></i> {{session('fail_msg')}}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        </div>
                    @endif
            <div class="table-responsive mt-1">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table class="table table-hover-animation mb-0">

                    <thead>
                        <tr>
                            <th style="width:2%">Sl.no</th>
                            <th >Action</th>
                            <th>Inclusion Name</th>
                            <th>Slug</th>
                            <th>Created Date</th>
                            <th>Modified Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($inclusions)
                            @if($inclusions->currentpage() == 1)
                                @php($ctr = 1)
                            @else
                                @php($ctr = ($inclusions->currentpage()-1)*$inclusions->perpage()+1)

                            @endif
                            @foreach($inclusions as $inclusion)
                                <tr>
                                    <td >{{$ctr}}</td>
                                    <td>
                                        <span style="float: left"><a href="{{route('inclusions.edit',$inclusion->id)}}" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-edit"></i>Edit</a></span>
                                        <span style="float: left"><a href="{{route('inclusions.show',$inclusion->id)}}" data-toggle="tooltip" title="View"><i class="fa fa-fw fa-eye"></i>View</a></span>
                                        <span style="float: left">
                                        <form action="{{route('inclusions.destroy',$inclusion->id) }}" method="post" id="deleteForm{{$inclusion->id}}">
                                            <input type="hidden" name="_method" value="DELETE">
                                            {!! csrf_field() !!}
                                            <a href="javascript:void(0)" onclick="document.getElementById('deleteForm{{$inclusion->id}}').submit();" data-toggle="tooltip" title="Delete">
                                                <i class="fa fa-fw  fa-trash"></i>Delete</a>

                                        </form>
                                        </span>
                                        @if($inclusion->is_active)
                                            <span><a href="{{route('inclusions.statusChange',$inclusion->id)}}" data-toggle="tooltip" title="Click to Disable">
                                                    <span style="color: yellowgreen"><i class="fa fa-fw fa-check"></i></span>Active</a></span>
                                        @else
                                            <span><a href="{{route('inclusions.statusChange',$inclusion->id)}}" data-toggle="tooltip" title="Click to Enable">
                                                    <span style="color: red"><i class="fa fa-fw fa-times"></i></span>Inactive</a></span>
                                        @endif
                                    </td>
                                    <td>{{$inclusion->name}} </td>
                                    <td>{{$inclusion->slug}} </td>
                                    <td>{{$inclusion->created_at->format('Y-m-d')}}</td>
                                    <td>{{$inclusion->updated_at->format('Y-m-d')}}</td>

                                </tr>
                                @php($ctr++)
                            @endforeach
                        @endif
                        </tbody>
                </table>
                </div>
                @include('admin/partials/pagination',['results'=>$inclusions])

            </div>
            </div>
        </div>
    </div>
</div>

@endsection

