@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Inclusions
                <small>Manage Inclusions</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('inclusions.index')}}">Inclusions</a></li>
                <li class="active"><a href="{{route('inclusions.create')}}">Add/Edit</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add/Edit Inclusions</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if(isset($inclusion->id)) {{--edit form--}}
                            {!! Form::model($inclusion,['method' => 'PATCH','action' => ['Admin\AdminInclusionsController@update',$inclusion->id]
                            ,'name' => 'inclusionsForm','id' => 'inclusionsForm']) !!}
                            {{csrf_field()}}
                            @else {{--create form--}}
                            {!!  Form::open(['method' => 'POST','action' => 'Admin\AdminInclusionsController@store','name' => 'inclusionsForm','id' => 'inclusionsForm']) !!}
                            @endif

                             <div class="col-md-6">
                                <div class="form-group">
                                {!! Form::label('name','Inclusion Name') !!}
                                {!! Form::text('name',null,['class'=>'form-control','data-validation' => 'required']) !!}
                                </div>
                             </div>
                             <div class="col-md-6">
                                 <div class="form-group">
                                {!! Form::label('slug','Slug') !!}
                                {!! Form::text('slug',null,['class'=>'form-control','data-validation' => 'required']) !!}
                                </div>
                             </div>

                            {{--<div class="form-group" style="height: 300px;">
                                 {!! Form::label('productinclusions','Product Inclusions') !!}
                                <select class="selectpicker dropupAuto" name="productinclusions[]" data-live-search="true" multiple>
                                    @foreach ($productInclusions as $key => $valueArray)
                                        <optgroup label="{{$key}}">
                                            @foreach($valueArray as $prodId => $prodName)
                                                <option value="{{$prodId}}">{{$prodName}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>--}}
                           {{-- <div class="row">
                                <div class="col-md-12">{!! Form::label('productinclusions','Product Inclusions') !!}</div>
                                @foreach ($productInclusions as $key => $valueArray)
                                    <div class="col-md-6"><b>{{$key}}</b>
                                    @foreach($valueArray as $prodId => $prodName)
                                            <div class="form-group">
                                                {!! Form::checkbox("prodInclu", "$prodId", true)!!} {{$prodName}}
                                            </div>

                                    @endforeach
                                    </div>
                                @endforeach
                            </div>--}}





                                <div class="col-md-8">{!! Form::label('subscriptionProducts','Associated Subscription (Choose the subscription to which this Inclusion is associated)') !!}</div>
                                @foreach ($subscriptionProducts as $key => $valueArray)
                                        <div class="col-md-12"><b>{{$key}}</b></div>
                                        <div class="col-md-12"><div class="form-group">
                                    @foreach($valueArray as $prodId => $prodName)
                                        @php($true="")
                                        @if(in_array($prodId,$product_inclusion))
                                             @php($true = "true")
                                        @endif
                                             <span style="padding: 10px;">{!! Form::checkbox("prodInclu[]", "$prodId", "$true")!!} {{$prodName}}</span>
                                    @endforeach
                                        </div></div>
                                 @endforeach


                             {{--<div class="form-group">
                                 {!! Form::label('description','Description') !!}
                                {!! Form::textarea('description',null,['class'=>'form-control']) !!}
                            </div>--}}
                                <div class="form-group">
                                    {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                                    <a href="{{route('inclusions.index')}}" class="btn btn-default">Cancel</a>

                                </div>
                                {!! Form::close() !!}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $.validate({
                lang: 'en',
            });

        });

    </script>
@stop