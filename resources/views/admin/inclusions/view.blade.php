@extends('layouts.admin')
@section('content')
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Inclusions
                <small>Manage Inclusions</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('inclusions.index')}}">Inclusions</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <div class="box box-primary viewform" style="padding:20px;float: left;">
                    <div class="form-group">
                        <label>Inclusion Name :</label><span>{{$inclusion->name}}</span>
                    </div>
                    <div class="form-group">
                        <label>Slug :</label><span>{{$inclusion->slug}}</span>
                    </div>
                    <div class="form-group">
                        <label>Associated Subscription (Choose the subscription to which this Inclusion is associated) :</label>

                        @foreach ($subscriptionProducts as $key => $valueArray)
                                        <div class="col-md-12"><b>{{$key}}</b></div>
                                        <div class="col-md-12"><div class="form-group">
                                       
                                    @foreach($valueArray as $prodId => $prodName)
                                        @php($true="")
                                        @if(in_array($prodId,$product_inclusion))
                                             @php($true = "true")
                                             <span style="padding: 10px;">{{$prodName}}</span>
                                        @endif
                                             
                                    @endforeach
                                        </div></div>
                                 @endforeach
                    </div>
                    
                   
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop