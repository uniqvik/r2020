<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="#">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">Realopedia</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            {{-- <li class=" nav-item"><a href="index.html"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span><span class="badge badge badge-warning badge-pill float-right mr-2">2</span></a>
                <ul class="menu-content">
                    <li class="active"><a href="dashboard-analytics.html"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Analytics</span></a>
                    </li>
                    <li><a href="dashboard-ecommerce.html"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="eCommerce">eCommerce</span></a>
                    </li>
                </ul>
            </li>--}}

            @php
            $crut = \Request::route()->getName();
            $seg = request()->segment(2);
            $seg3 = request()->segment(3);
            @endphp
            <li class="nav-item @if($crut == 'admindashboard') active @endif"><a href="{{url('/dashboard')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Email">Dashboard</span></a>
            </li>
            @can('accessPermission','user,role,association')
            <li class="nav-item @if($seg == 'users' || $seg == 'roles' || $seg == 'associations') open @endif">
                <a href="{{route('users.index')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">User Management</span></a>
                <ul class="menu-content">
                    @can('accessPermission','role')
                    <li class="@if($seg == 'roles') active @endif">
                        <a href="{{route('roles.index')}}">
                        <i class="feather icon-circle"></i><span class="menu-item" >Roles</span>
                    </a></li>
                    @endcan
                    @can('accessPermission','association')
                    <li class="@if($seg == 'associations') active @endif"><a href="{{route('associations.index')}}"><i class="feather icon-circle"></i> Associations/Groups</a></li>
                    @endcan
                    @can('accessPermission','user')
                    <li class="@if($seg == 'users') active @endif"><a href="{{route('users.index')}}"><i class="feather icon-circle"></i> Users</a></li>
                    @endcan

                </ul>
            </li>
            @endcan
            @can('accessPermission','location')
            <li class="nav-item @if($seg == 'location') open @endif">

                <a href="{{route('users.index')}}"> <i class="feather icon-map-pin"></i><span class="menu-title" data-i18n="Location">Location</span></a>
                <ul class="menu-content">
                    <li class="@if($seg3 == 'country') active @endif"><a href="{{route('country.index')}}"><i class="feather icon-circle"></i> Country</a></li>
                    <li class="@if($seg3 == 'zone') active @endif"><a href="{{route('zone.index')}}"><i class="feather icon-circle"></i> Zone</a></li>
                    <li class="@if($seg3 == 'city') active @endif"><a href="{{route('city.index')}}"><i class="feather icon-circle"></i> City</a></li>
                    <li class="@if($seg3 == 'community') active @endif"><a href="{{route('community.index')}}"><i class="feather icon-circle"></i> Community</a></li>
                    <li class="@if($seg3 == 'subcommunity') active @endif"><a href="{{route('subcommunity.index')}}"><i class="feather icon-circle"></i> Subcommunity</a></li>
                </ul>
            </li>
            @endcan

            @can('accessPermission',['email'])
            <li class="nav-item @if($seg == 'email' || $seg == 'template') open @endif">

                    <a href="#">
                        <i class="feather icon-mail"></i><span class="menu-title" data-i18n="Email">Email Templates</span>
                    </a>
                    <ul class="menu-content">
                            <li class="@if($seg3 == 'campaign') active @endif"><a href="{{route('campaign.index')}}"><i class="feather icon-circle"></i>Campaigns</a></li>
                            <li class="@if($seg == 'template') active @endif"><a href="{{route('template.index')}}"><i class="feather icon-circle"></i>Email Templates</a></li>
                            {{-- <li class="@if($seg3 == 'inclusions') active @endif"><a href="{{route('inclusions.index')}}"><i class="feather icon-circle"></i>Schedules</a></li> --}}

                    </ul>
                </li>
            @endcan

            @can('accessPermission',['propertyfeild'])
            <li class="nav-item @if($seg == 'propertyfeild') open @endif">
                <a href="#">
                    <i class="fa fa-fw fa-television"></i>
                    <span>Property Options</span>

                </a>
                <ul class="menu-content">

                        <li class="@if($seg == 'propertyfeild') active @endif"><a href="{{route('propertyfeild.index')}}"><i class="feather icon-circle"></i>Property Feilds</a></li>
                        <li><a href="{{route('propertyfeildlist.index')}}"><i class="feather icon-circle"></i>Property Feild List</a></li>

                </ul>
            </li>
        @endcan

            @can('accessPermission',['setting'])
            <li class="nav-item @if($seg == 'setting') open @endif">
                <a href="#">
                    <i class="fa fa-fw fa-gear"></i>
                    <span>Settings</span>

                </a>
                {{-- <ul class="menu-content">
                    <li><a href="{{route('setting.index')}}"><i class="feather icon-circle"></i>Keys And Code</a></li>
                </ul> --}}
            </li>
        @endcan
        <li class="nav-item">
            <a href="#">
                <i class="fa fa-fw fa-gear"></i>
                <span>XML Feeds</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#">
                <i class="fa fa-fw fa-gear"></i>
                <span>Property</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#">
                <i class="fa fa-fw fa-gear"></i>
                <span>Profile</span>
            </a>
        </li>
        </ul>
    </div>
</div>
